# Photo Contest

**Presentation**

[Click](https://streamable.com/iac07f) for a video presentation of the user interface:
[![Click for a video presentation of the user interface](https://i.ibb.co/c8bhh60/thumb.png)](https://streamable.com/iac07f)

**How to set up and run**

**Database**

A MariaDB instance was used. You can find a sql script for creation of the database schema in the [database folder](https://gitlab.com/antchev/photo-contest/-/tree/master/database). It is essential to enable the [event scheduler](https://mariadb.com/docs/reference/es/system-variables/event_scheduler/#:~:text=The%20event_scheduler%20system%20variable%20can%20also%20be%20set%20dynamically%20at,next%20time%20the%20server%20restarts.) as there are events in the schema which depend on it.

**Server**

The server needs a .env file placed in the root of the [server folder](https://gitlab.com/antchev/photo-contest/-/tree/master/server) with the following format:

```
DB_USER=<db_user>
DB_PASS=<db_password>
DB_HOST=<db_host>
DB_PORT=<db_port>
DB_NAME=photocontest
REDIS_HOST=<redis_host>
REDIS_PORT=<redis_port>
HOST=test.mydomain.com
PORT=3000
CLIENT_HOST=test.mydomain.com
CLIENT_HOST_OUTSIDE=<outside_host_ip>
CLIENT_PORT=4000
ACCESS_PRIVATE_KEY=<access_token_sekret_key>
REFRESH_PRIVATE_KEY=<refresh_token_sekret_key>
REFRESH_COOKIE_KEY=rt
ACCESS_TOKEN_LIFETIME=900
REFRESH_TOKEN_LIFETIME=604800
PARTICIPATION_PATH=./public/uploads/participations/
NOTIFICATION_INTERVAL_SECONDS=60
STAGE=production
```

You need to connect a running Redis database instance.

Run **`npm install`** and then **`npm start`** from inside the [server folder](https://gitlab.com/antchev/photo-contest/-/tree/master/server). The server will start on `test.mydomain.com:3000`.

**Client**

The client needs a .env file placed in the root of the [server folder](https://gitlab.com/antchev/photo-contest/-/tree/master/client) with the following format:

```
HOST=test.mydomain.com
PORT=4000
REACT_APP_SERVER_HOST=test.mydomain.com
REACT_APP_SERVER_PORT=3000
REACT_APP_CLIENT_HOST=test.mydomain.com
REACT_APP_CLIENT_PORT=4000
```

Run **`npm install`** and then **`npm start`** from inside the [client folder](https://gitlab.com/antchev/photo-contest/-/tree/master/client). The client will be accessible from the browser at `test.mydomain.com:4000`.

**Hosts file**

You need to have an entry for `test.mydomain.com` pointing to `127.0.0.1` in the `hosts` file of your OS. This is essential for the httpOnly cookie used for refresh of the access token to be accepted by the browser, because it does not accept cookies coming from localhost.

Windows example: [How to Edit the Hosts File in Windows 10](https://www.groovypost.com/howto/edit-hosts-file-windows-10/)


