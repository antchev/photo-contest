/* eslint-disable no-undef */
export default {
  verbose: true,
  testEnvironment: 'jest-environment-node',
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
};
