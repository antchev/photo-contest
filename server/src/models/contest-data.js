import pool from './pool.js';

const createContest = async (
  title,
  category,
  cover,
  isInvitational,
  hasAdditionalJurors,
  phaseOnedate,
  phaseTwodate,
  userId,
) => {
  const sql = 'CALL create_contest(?, ?, ?, ?, ?, ?, ?, ?)';
  const contest = await pool.query(sql, [
    title,
    category,
    cover,
    isInvitational,
    hasAdditionalJurors,
    phaseOnedate,
    phaseTwodate,
    userId,
  ]);
  return contest?.[0]?.[0];
};

const getContestBy = async (columName, term, userId) => {
  const sql = 'CALL get_contest_by(?, ?, ? )';
  const result = await pool.query(sql, [columName, term, userId]);
  return result?.[0]?.[0];
};

const joinContest = async (contest_id, user_id) => {
  const sql = 'CALL join_contest(?, ?)';
  const result = await pool.query(sql, [contest_id, user_id]);
  return result?.[0]?.[0];
};

const addAdditionalJurors = async (contest_id, users) => {
  const sql = 'CALL add_additional_jurors(?, ?)';
  await pool.query(sql, [contest_id, users]);
};

const addContestInvites = async (contest_id, users) => {
  const sql = 'CALL add_contest_invites(?, ?)';
  await pool.query(sql, [contest_id, users]);
};

const getUsersByContest = async (contestId) => {
  const sql = 'CALL get_users_by_contest(?)';
  const result = await pool.query(sql, [contestId]);
  return result?.[0]?.[0];
};

const isContestInPhaseOne = async (contestId) => {
  const sql = 'SELECT is_contest_in_phase1(?) AS isPhaseOne';
  const result = await pool.query(sql, [contestId]);
  return result?.[0].isPhaseOne;
};

const isContestInPhaseTwo = async (contestId) => {
  const sql = 'SELECT is_contest_in_phase2(?) AS isPhaseTwo';
  const result = await pool.query(sql, [contestId]);
  return result?.[0].isPhaseTwo;
};

const isContestOver = async (contestId) => {
  const sql = 'SELECT is_contest_over(?) AS isOver';
  const result = await pool.query(sql, [contestId]);
  return result?.[0].isOver;
};

const isJuror = async (contestId, userId) => {
  const sql = 'SELECT is_juror(?, ?) AS isJuror';
  const result = await pool.query(sql, [contestId, userId]);
  return result?.[0].isJuror;
};

const getAllContests = async (page, perPage, sortBy, sortOrder, userId) => {
  const sql = 'CALL get_all_contests(?, ?, ?, ?, ?); SELECT @count;';
  const result = await pool.query(sql, [page, perPage, sortBy, sortOrder, userId]);
  const totalCount = result[2][0]['@count'];
  return {
    totalCount: totalCount,
    data: result?.[0],
    page: page,
    perPage: perPage,
  };
};

const scoreContest = async (contestId) => {
  const sql = 'CALL scoreContest(?, ?)';
  const result = await pool.query(sql, [contestId]);
  return result?.[0]?.[0];
};
const getContestsByUser = async (user_id) => {
  const sql = 'CALL get_contests_by_user(?)';
  const result = await pool.query(sql, [user_id]);
  return result?.[0];
};

export default {
  getContestsByUser,
  scoreContest,
  getAllContests,
  isJuror,
  getUsersByContest,
  isContestInPhaseOne,
  isContestInPhaseTwo,
  isContestOver,
  addContestInvites,
  addAdditionalJurors,
  joinContest,
  createContest,
  getContestBy,
};
