import redis from 'redis';
import dotenv from 'dotenv';

dotenv.config();
export const redisClient = redis.createClient({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST,
});
redisClient.on('connect', () => {
  console.log('Redis client connected');
});
redisClient.on('error', (error) => {
  console.log('Redis not connected', error);
});
export default redisClient;
