import pool from './pool.js';

const getNotificationsByUser = async (userId) => {
  const sql = 'CALL get_notifications_by_user( ? )';
  const result = await pool.query(sql, [userId]);
  return result?.[0];
};

const getNotificationsByUsers = async (usersIds) => {
  const sql = 'CALL get_notifications_by_users( ? )';
  const result = await pool.query(sql, [usersIds]);
  return result?.[0];
};
const markNotificationAsRead = async (notificationId) => {
  const sql = 'CALL mark_notification_as_read( ? )';
  const result = await pool.query(sql, [notificationId]);
  return result?.[0]?.[0];
};

const getAllNotifications = async (notificationId) => {
  const sql = 'CALL get_all_notifications()';
  const result = await pool.query(sql, [notificationId]);
  return result?.[0];
};

const sendOpenNotification = async (contest_id) => {
  const sql = 'CALL send_open_notification(?)';
  const result = await pool.query(sql, [contest_id]);
  return result?.[0];
};

export default {
  getNotificationsByUsers,
  sendOpenNotification,
  getNotificationsByUser,
  markNotificationAsRead,
  getAllNotifications,
};
