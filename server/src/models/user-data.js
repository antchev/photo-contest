import pool from './pool.js';

const createUser = async (username, hashedPassword, first_name, last_name) => {
  const sql = 'CALL create_user(?, ?, ?, ?)';
  const user = await pool.query(sql, [username, hashedPassword, first_name, last_name]);

  return user?.[0]?.[0];
};

const getUserBy = async (colum, value) => {
  const sql = 'CALL get_user_by(?, ?)';
  const user = await pool.query(sql, [colum, value]);
  return user?.[0]?.[0];
};

const getUserPassword = async (username) => {
  const sql = 'CALL get_user_password(?)';
  const result = await pool.query(sql, [username]);
  return result?.[0]?.[0]?.password;
};

const getAllUsers = async (page, perPage, sortBy, sortOrder) => {
  const sql = 'CALL get_all_users(?, ?, ?, ?); SELECT @count;';
  const result = await pool.query(sql, [page, perPage, sortBy, sortOrder]);
  const totalCount = result[2][0]['@count'];
  return {
    totalCount: totalCount,
    data: result?.[0],
    page: page,
    perPage: perPage,
  };
};
const hasJoined = async (contestId, userId) => {
  const sql = 'SELECT has_joined(?, ?) AS hasJoined';
  const result = await pool.query(sql, [contestId, userId]);
  return result?.[0].hasJoined;
};

export default {
  hasJoined,
  getAllUsers,
  createUser,
  getUserBy,
  getUserPassword,
};
