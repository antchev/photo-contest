import pool from './pool.js';

const getParticipation = async (participationId) => {
  const sql = 'CALL get_participation(?)';
  const result = await pool.query(sql, [participationId]);
  return result?.[0]?.[0];
};

const submitParticipation = async (participation_id, title, story, photo) => {
  const sql = 'CALL submit_participation(?, ?, ?, ?)';
  const result = await pool.query(sql, [participation_id, title, story, photo]);
  return result?.[0]?.[0];
};

const getParticipationsByContest = async (contestId) => {
  const sql = 'CALL get_participations_by_contest( ? )';
  const result = await pool.query(sql, [contestId]);
  return result?.[0];
};

const getParticipationsByUser = async (userId) => {
  const sql = 'CALL get_participations_by_user( ? )';
  const result = await pool.query(sql, [userId]);
  return result?.[0];
};
const getParticipationByUser = async (contestId, userId) => {
  const sql = 'CALL get_participation_by_user( ?, ? )';
  const result = await pool.query(sql, [contestId, userId]);
  return result?.[0]?.[0];
};

const gradeParticipation = async (participationId, jurorId, score, comment) => {
  const sql = 'CALL grade_participation( ?, ?, ?, ?)';
  const result = await pool.query(sql, [participationId, jurorId, score, comment]);
  return result?.[0]?.[0];
};

const getGrade = async (participationId, jurorId) => {
  const sql = 'CALL get_grade( ?, ? )';
  const result = await pool.query(sql, [participationId, jurorId]);
  return result?.[0]?.[0];
};

const getGradesByParticipation = async (participationId) => {
  const sql = 'CALL get_grades_by_participation( ? )';
  const result = await pool.query(sql, [participationId]);
  return result?.[0];
};
const hasGraded = async (participationId, userId) => {
  const sql = 'SELECT has_graded(?, ?) AS hasGraded';
  const result = await pool.query(sql, [participationId, userId]);
  return result?.[0].hasGraded;
};
const getRecentWinner = async () => {
  const sql = 'CALL get_recent_winners()';
  const result = await pool.query(sql);
  return result?.[0];
};

export default {
  getRecentWinner,
  getParticipationByUser,
  hasGraded,
  getGradesByParticipation,
  getGrade,
  gradeParticipation,
  getParticipationsByUser,
  getParticipationsByContest,
  getParticipation,
  submitParticipation,
};
