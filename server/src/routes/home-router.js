import express from 'express';
import { errorCatcher } from '../common/error-handler.js';
import homeController from '../controllers/home-controller.js';

const homeRouter = new express.Router();

homeRouter.get('/', errorCatcher(homeController.getRecentWinner)); // gets recent winners

export default homeRouter;
