import express from 'express';
import { errorCatcher } from '../common/error-handler.js';
import userController from '../controllers/user-controller.js';
import { validateByScheme, Schema } from '../common/validations.js';

const userRouter = new express.Router();

userRouter.get(
  '/',
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(validateByScheme(Schema.Pagination, 'query', false, 'users-')),
  errorCatcher(userController.getAllUsers),
); // get all users
userRouter.get('/:id', errorCatcher(validateByScheme(Schema.ID, 'params')), errorCatcher(userController.getUserById)); // get user by id
userRouter.get(
  '/:id/participations/',
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(userController.getParticipationsByUser),
); // get participation by user id

userRouter.get(
  '/:id/contests/',
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(userController.getContestsByUser),
); // get contests by user id
userRouter.get(
  '/:userId/contests/:id',
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(userController.getParticipationByUser),
); // get participation by contest id

export default userRouter;
