import express from 'express';
import { errorCatcher } from '../common/error-handler.js';
import authController from '../controllers/auth-controller.js';
import { validateByScheme, Schema } from '../common/validations.js';

const authRouter = new express.Router();

authRouter.post(
  '/registration',
  errorCatcher(validateByScheme(Schema.User, 'body', 'required')),
  errorCatcher(authController.registerUser),
); //register user
authRouter.post(
  '/',
  errorCatcher(validateByScheme(Schema.Session, 'body', 'required')),
  errorCatcher(authController.logInUserByUsername),
); //login user
authRouter.delete('/', authController.authenticate, errorCatcher(authController.logOut)); //logout user
authRouter.post('/token', errorCatcher(authController.refreshAccessToken)); //refresh access token
authRouter.delete(
  '/token/:id',
  authController.authenticate,
  authController.isOrganizer,
  errorCatcher(authController.deleteRefreshToken),
); //delete refresh token from Redis

export default authRouter;
