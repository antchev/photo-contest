import express from 'express';
import { errorCatcher } from '../common/error-handler.js';
import contestController from '../controllers/contest-controller.js';
import { validateByScheme, Schema } from '../common/validations.js';
import authController from '../controllers/auth-controller.js';
const contestRouter = new express.Router();

contestRouter.get(
  '/',
  errorCatcher(validateByScheme(Schema.Pagination, 'query', false, 'contests-')),
  errorCatcher(contestController.getAllContests),
); // get all contests
contestRouter.post(
  '/',
  authController.isOrganizer,
  errorCatcher(validateByScheme(Schema.Contest, 'body', 'required')),
  errorCatcher(validateByScheme(['users', 'jurors'], 'body')),
  errorCatcher(contestController.createContest),
); // create constest - organiser only
contestRouter.get(
  '/:id',
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(contestController.getContestById),
); // get constest by id
contestRouter.post(
  '/:id/jurors',
  authController.isOrganizer,
  errorCatcher(validateByScheme(['users'], 'body')),
  errorCatcher(contestController.addAdditionalJurors),
); // add more jurrors on contest as organiser
contestRouter.get(
  '/:contestId/participations/:id',
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(contestController.getParticipation),
); // get user by id
contestRouter.get(
  '/:id/isJuror',
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(contestController.isJuror),
); // isUserAJuror
contestRouter.put(
  '/:contestId/participations/:id',
  authController.isRegular,
  errorCatcher(validateByScheme(Schema.Participation, 'body', 'required')),
  errorCatcher(contestController.submitParticipation),
); // get participation
contestRouter.post(
  '/:id/participations/',
  authController.isRegular,
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(contestController.joinContest),
); // join constest - non jury only
contestRouter.get(
  '/:id/participations/',
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(contestController.getParticipationsByContest),
); // get user by id
contestRouter.post(
  '/:contestId/participations/:id/grade',
  errorCatcher(validateByScheme(Schema.Grade, 'body', 'required')),
  errorCatcher(contestController.gradeParticipation),
); //  grade participation
contestRouter.get(
  '/:contestId/participations/:id/grade',
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(contestController.getGradesByParticipation),
); // get user by id
contestRouter.get(
  '/:contestId/participations/:participationId/grade/:id',
  errorCatcher(validateByScheme(Schema.ID, 'params')),
  errorCatcher(contestController.getGrade),
); // get user by id

export default contestRouter;
