import { serviceErrors } from '../common/error-handler.js';

const getNotificationsByUser = (notificationData) => {
  return async (userId) => {
    const notification = await notificationData.getNotificationsByUser(userId);
    if (!notification) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data: notification };
    }
  };
};

const getNotificationsByUsers = (notificationData) => {
  return async (usersIds) => {
    const notification = await notificationData.getNotificationsByUsers(usersIds);
    if (!notification) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data: notification };
    }
  };
};

const markNotificationAsRead = (notificationData) => {
  return async (notificationId) => {
    const notification = await notificationData.markNotificationAsRead(notificationId);
    if (!notification) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data: notification };
    }
  };
};

const getAllNotifications = (notificationData) => {
  return async () => {
    const notification = await notificationData.getAllNotifications();
    if (!notification) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data: notification };
    }
  };
};

const sendOpenNotifications = (notificationData) => {
  return async (contestId) => {
    const notification = await notificationData.sendOpenNotifications(contestId);
    if (!notification) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data: notification };
    }
  };
};

export default {
  getNotificationsByUsers,
  sendOpenNotifications,
  getNotificationsByUser,
  getAllNotifications,
  markNotificationAsRead,
};
