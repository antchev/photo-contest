import { serviceErrors } from '../common/error-handler.js';

const getContestBy = (contestData) => {
  return async (columnName, term, userId) => {
    const data = await contestData.getContestBy(columnName, term, userId);
    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

const createContest = (contestData, notificationData) => {
  return async (
    title,
    category,
    cover,
    isInvitational,
    hasAdditionalJurors,
    phaseOneDate,
    phaseTwoDate,
    userId,
    users,
    jurors,
  ) => {
    if (users && jurors) {
      const usersArr = users.split(',');
      const jurorsArr = jurors.split(',');

      const duplicates = usersArr.filter((x) => jurorsArr.includes(x));
      if (duplicates.length) {
        return { error: serviceErrors.CAN_NOT_BE_BOTH_JUROR_AND_INVITED.code, data: null };
      }
    }

    const existingContest = await contestData.getContestBy('title', title, userId);
    if (existingContest) {
      return { error: serviceErrors.DUPLICATE_CONTEST_RECORD.code, data: null };
    }
    const contest = await contestData.createContest(
      title,
      category,
      cover,
      isInvitational,
      hasAdditionalJurors,
      phaseOneDate,
      phaseTwoDate,
      userId,
    );

    if (contest.isInvitational && users) {
      await contestData.addContestInvites(contest.id, users);
    }
    if (contest.hasAdditionalJurors && jurors) {
      await contestData.addAdditionalJurors(contest.id, jurors);
    }

    notificationData.sendOpenNotification(contest.id);

    return { error: null, data: contest };
  };
};

const getParticipation = (participationData) => {
  return async (participationId) => {
    const data = await participationData.getParticipation(participationId);
    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

const submitParticipation = (participationData, contestData) => {
  return async (participationId, title, story, photo, userId) => {
    const participation = await participationData.getParticipation(participationId);
    const isPhaseOne = await contestData.isContestInPhaseOne(participation.contestId);

    if (!isPhaseOne) {
      return { error: serviceErrors.PHASE_ONE_IS_OVER.code, data: null };
    }
    if (userId !== participation.userId) {
      return { error: serviceErrors.NOT_YOUR_PARTICIPATION.code, data: null };
    }
    if (participation.title) {
      return { error: serviceErrors.ALREADY_PARTICIPATING.code, data: null };
    }
    const data = await participationData.submitParticipation(participationId, title, story, photo);

    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

const addAdditionalJurors = (contestData) => {
  return async (contestId, users) => {
    const data = await contestData.addAdditionalJurors(contestId, users);

    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};
const addContestInvites = (contestData) => {
  return async (contestId, users) => {
    const data = await contestData.addContestInvites(contestId, users);

    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

const joinContest = (contestData, userData) => {
  return async (contestId, userId) => {
    const isPhaseOne = await contestData.isContestInPhaseOne(contestId);
    if (!isPhaseOne) {
      return { error: serviceErrors.PHASE_ONE_IS_OVER.code, data: null };
    }

    const contest = await contestData.getContestBy('id', contestId, userId);
    if (contest.isInvitational) {
      return { error: serviceErrors.NOT_INVITED.code, data: null };
    }

    const isJuror = await contestData.isJuror(contestId, userId);

    if (isJuror) {
      return { error: serviceErrors.A_JUROR.code, data: null };
    }

    const hasJoined = await userData.hasJoined(contestId, userId);
    if (hasJoined) {
      return { error: serviceErrors.ALREADY_JOINED.code, data: null };
    }

    const data = await contestData.joinContest(contestId, userId);
    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

const getUsersByContest = (contestData) => {
  return async (contestId) => {
    const data = await contestData.getUsersByContest(contestId);
    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

const getParticipationsByContest = (participationData) => {
  return async (contest_id) => {
    const data = await participationData.getParticipationsByContest(contest_id);

    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};
const isJuror = (contestData) => {
  return async (contestId, userId) => {
    const isJuror = await contestData.isJuror(contestId, userId);
    return { error: null, data: isJuror };
  };
};

const gradeParticipation = (participationData, contestData) => {
  return async (participationId, jurorId, score, comment) => {
    const participation = await participationData.getParticipation(participationId);

    const isPhaseTwo = await contestData.isContestInPhaseTwo(participation.contestId);

    if (!isPhaseTwo) {
      return { error: serviceErrors.NOT_IN_PHASE_TWO.code, data: null };
    }

    const isJuror = await contestData.isJuror(participation.contestId, jurorId);

    if (!isJuror) {
      return { error: serviceErrors.NOT_A_JUROR.code, data: null };
    }

    const hasGraded = await participationData.hasGraded(participationId, jurorId);
    if (hasGraded) {
      return { error: serviceErrors.ALREADY_GRADED.code, data: null };
    }

    const data = await participationData.gradeParticipation(participationId, jurorId, score, comment);
    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

const getGrade = (participationData) => {
  return async (participationId, jurorId) => {
    const data = await participationData.getGrade(participationId, jurorId);

    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

const getGradesByParticipation = (participationData) => {
  return async (participationId) => {
    const data = await participationData.getGradesByParticipation(participationId);

    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};
const hasGraded = (participationData) => {
  return async (participationId, jurorId) => {
    const hasGraded = await participationData.hasGraded(participationId, jurorId);
    return { error: null, data: hasGraded };
  };
};

const getAllContests = (contestData) => {
  return async (page, perPage, sortBy, sortOrder, userId) => {
    return await contestData.getAllContests(page, perPage, sortBy, sortOrder, userId);
  };
};

const scoreContest = (contestId) => {
  return async (contestData) => {
    const data = await contestData.getGradesByParticipation(contestId);

    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

const getRecentWinner = (participationData) => {
  return async () => {
    const data = await participationData.getRecentWinner();
    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

export default {
  getRecentWinner,
  isJuror,
  scoreContest,
  getAllContests,
  hasGraded,
  getGradesByParticipation,
  getGrade,
  gradeParticipation,
  getParticipationsByContest,
  getUsersByContest,
  addContestInvites,
  joinContest,
  addAdditionalJurors,
  submitParticipation,
  getContestBy,
  createContest,
  getParticipation,
};
