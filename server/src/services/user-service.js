import { serviceErrors } from '../common/error-handler.js';
import bcrypt from 'bcrypt';

const getUserBy = (usersData) => {
  return async (column, id) => {
    const user = await usersData.getUserBy(column, id);
    if (!user) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data: user };
    }
  };
};

const hasJoined = (usersData) => {
  return async (contestId, userId) => {
    const hasJoined = await usersData.hasJoined(contestId, userId);
    return { error: null, data: hasJoined };
  };
};

const createUser = (usersData) => {
  return async (username, password, firstName, lastName) => {
    const existingUserName = await usersData.getUserBy('username', username);
    if (existingUserName) {
      return {
        error: serviceErrors.DUPLICATE_USERNAME_RECORD.code,
        data: null,
      };
    }

    const hash = await bcrypt.hash(password, 10);
    const user = await usersData.createUser(username, hash, firstName, lastName);

    return { error: null, data: user };
  };
};

/*
username is not case sensitive
password is case sensitive
*/
const logInByUserName = (usersData) => {
  return async (username, password) => {
    const hashedPassword = await usersData.getUserPassword(username);
    if (!hashedPassword || !(await bcrypt.compare(password, hashedPassword))) {
      return { error: serviceErrors.INVALID_SIGNIN.code, data: null };
    }
    const user = await usersData.getUserBy('username', username);
    return {
      error: null,
      data: user,
    };
  };
};

const getAllUsers = (usersData) => {
  return async (page, perPage, sortBy, sortOrder) => {
    return await usersData.getAllUsers(page, perPage, sortBy, sortOrder);
  };
};

const getParticipationsByUser = (participationData) => {
  return async (userId) => {
    const data = await participationData.getParticipationsByUser(userId);
    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};

const getParticipationByUser = (participationData) => {
  return async (contestId, userId) => {
    const data = await participationData.getParticipationByUser(contestId, userId);
    return { error: null, data };
  };
};

const getContestsByUser = (contestData) => {
  return async (user_id) => {
    const data = await contestData.getContestsByUser(user_id);
    if (!data) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, data: null };
    } else {
      return { error: null, data };
    }
  };
};
export default {
  getParticipationByUser,
  getContestsByUser,
  getParticipationsByUser,
  getAllUsers,
  getUserBy,
  createUser,
  logInByUserName,
  hasJoined,
};
