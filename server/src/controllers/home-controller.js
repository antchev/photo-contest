import contestService from '../services/contest-service.js';
import participationData from '../models/participation-data.js';
import { serviceErrorStack } from '../common/error-handler.js';

const getRecentWinner = async (req, res) => {
  const { error, data } = await contestService.getRecentWinner(participationData)();
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

export default {
  getRecentWinner,
};
