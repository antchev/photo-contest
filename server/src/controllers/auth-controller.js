import { createAccessToken, createRefreshToken } from '../auth/token.js';
import { serviceErrorStack, serviceErrors } from '../common/error-handler.js';
import passport from 'passport';
import usersData from '../models/user-data.js';
import userService from '../services/user-service.js';
import redisClient from '../models/redis-client.js';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();
const authenticate = async (req, res, next) => {
  passport.authenticate('jwt', { session: false }, async (error, payload) => {
    if (error || !payload) {
      return res.status(401).json({ error: 'Unauthorized, invalid credentials.' });
    }
    req.user = payload;
    next();
  })(req, res, next);
};

const isOrganizer = (req, res, next) => {
  if (req.user && req.user.isOrganizer === 1) {
    next();
  } else {
    res.status(serviceErrors.RESOURCE_IS_FORBIDDEN_FOR_REGULAR.status).send({
      error: serviceErrors.RESOURCE_IS_FORBIDDEN_FOR_REGULAR.message,
    });
  }
};

const isRegular = (req, res, next) => {
  if (req.user && req.user.isOrganizer === 0) {
    next();
  } else {
    res.status(serviceErrors.RESOURCE_IS_FORBIDDEN_FOR_ORGANIZER.status).send({
      error: serviceErrors.RESOURCE_IS_FORBIDDEN_FOR_ORGANIZER.message,
    });
  }
};
const registerUser = async (req, res) => {
  const { username, password, firstName, lastName } = req.body;
  const data = await userService.createUser(usersData)(username, password, firstName, lastName);
  const user = data.data;
  const error = data.error;
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }

  res.status(201).send(user);
};

const logInUserByUsername = async (req, res) => {
  const { username, password } = req.body;

  const { error, data } = await userService.logInByUserName(usersData)(username, password);

  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  } else {
    createResponseData(data, req, res);
  }
};

const refreshAccessToken = async (req, res) => {
  const refreshToken = req.cookies[process.env.REFRESH_COOKIE_KEY];

  try {
    const verified = jwt.verify(refreshToken, process.env.REFRESH_PRIVATE_KEY);
    const { data } = await userService.getUserBy(usersData)('id', verified.sub);

    redisClient.get(data.id, (err, result) => {
      const storedRefreshToken = JSON.parse(result);
      if (refreshToken.localeCompare(storedRefreshToken) === 0) {
        createResponseData(data, req, res);
      } else {
        return res.clearCookie('rt').status(401).send({ error: 'Unauthorized, invalid credentials.' });
      }
    });
  } catch (err) {
    return res.clearCookie('rt').status(401).send({ error: 'Unauthorized, invalid credentials.' });
  }
};

const deleteRefreshToken = async (req, res) => {
  const userId = req.params.id;
  redisClient.del(userId, redisClient.print);
  res.clearCookie(process.env.REFRESH_COOKIE_KEY);
  res.status(200).send({ message: 'Refresh token removed' });
};

const logOut = async (req, res) => {
  const loggedUser = req.user;

  redisClient.del(loggedUser.id, redisClient.print);
  req.logout();
  res.clearCookie(process.env.REFRESH_COOKIE_KEY);
  res.status(200).send({ message: 'Logged out' });
};

const createResponseData = (user, req, res) => {
  const payload = {
    sub: user.id,
    username: user.username,
    firstName: user.firstName,
    lastName: user.lastName,
    createDate: user.createDate,
    isOrganizer: user.isOrganizer,
  };
  const accessToken = createAccessToken(payload);
  const refreshToken = createRefreshToken(payload);
  const sessionData = {
    data: user,
    token: accessToken,
  };

  storeTokenInRedis(sessionData.data.id, refreshToken);

  req.user = sessionData;
  res.cookie('rt', refreshToken, {
    httpOnly: true,
    maxAge: +process.env.REFRESH_TOKEN_LIFETIME * 1000,
    sameSite: 'Lax',
    secure: false,
  });
  res.status(200).send(sessionData);
};

const storeTokenInRedis = (id, token) => {
  redisClient.set(id, JSON.stringify(token), redisClient.print);
  redisClient.expire(id, +process.env.REFRESH_TOKEN_LIFETIME);
};

export default {
  registerUser,
  logInUserByUsername,
  refreshAccessToken,
  deleteRefreshToken,
  logOut,
  authenticate,
  isOrganizer,
  isRegular,
};
