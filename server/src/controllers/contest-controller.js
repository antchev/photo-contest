import contestService from '../services/contest-service.js';
import contestData from '../models/contest-data.js';
import participationData from '../models/participation-data.js';
import userData from '../models/user-data.js';
import notificationData from '../models/notification-data.js';

import { serviceErrorStack } from '../common/error-handler.js';
import { enumData } from '../common/common.js';

const createContest = async (req, res) => {
  const userId = req.user.id;
  const { title, category, isInvitational, hasAdditionalJurors, phaseOneDate, phaseTwoDate } = req.body;
  const cover = req.body.cover || null;
  const users = req.body.users || '';
  const jurors = req.body.jurors || '';

  const { error, data } = await contestService.createContest(contestData, notificationData)(
    title,
    category,
    cover,
    isInvitational,
    hasAdditionalJurors,
    phaseOneDate,
    phaseTwoDate,
    userId,
    users,
    jurors,
  );

  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

const isJuror = async (req, res) => {
  const contestId = req.params.id;
  const userId = req.user.id;
  const { error, data } = await contestService.isJuror(contestData)(contestId, userId);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

const getGrade = async (req, res) => {
  const participationId = +req.params.participationId;
  const jurorId = +req.params.id;
  const { data } = await contestService.getGrade(participationData)(participationId, jurorId);
  res.status(200).json(data);
};
const getGradesByParticipation = async (req, res) => {
  const participationId = +req.params.id;
  const { error, data } = await contestService.getGradesByParticipation(participationData)(participationId);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

const getParticipationsByContest = async (req, res) => {
  const contestId = +req.params.id;
  const { error, data } = await contestService.getParticipationsByContest(participationData)(contestId);

  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};
const getParticipation = async (req, res) => {
  const participationId = +req.params.id;
  const { error, data } = await contestService.getParticipation(participationData)(participationId);

  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};
const gradeParticipation = async (req, res) => {
  const { score, comment } = req.body;
  const jurorId = req.user.id;
  const participationId = +req.params.id;
  const { error, data } = await contestService.gradeParticipation(participationData, contestData)(
    participationId,
    jurorId,
    score,
    comment,
  );

  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

const getContestById = async (req, res) => {
  const contestId = +req.params.id;
  const userId = req.user.id;
  const { error, data } = await contestService.getContestBy(contestData)('id', contestId, userId);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};
const joinContest = async (req, res) => {
  const userId = req.user.id;
  const contestId = +req.params.id;
  const { error, data } = await contestService.joinContest(contestData, userData)(contestId, userId);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

const submitParticipation = async (req, res) => {
  const userId = req.user.id;
  const participationId = +req.params.id;
  const { title, story, photo } = req.body;
  const { error, data } = await contestService.submitParticipation(participationData, contestData)(
    participationId,
    title,
    story,
    photo,
    userId,
  );
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

const addAdditionalJurors = async (req, res) => {
  const contestId = +req.params.id;
  const { users } = req.body;

  const { error, data } = await contestService.addAdditionalJurors(contestData)(contestId, users);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

const getAllContests = async (req, res) => {
  const userId = +req.user.id;
  const page = +req.query.page || enumData.defaultPage;
  const perPage = +req.query.perPage || enumData.defaultPerPage;
  const sortBy = req.query.sortBy || enumData.defaultSortContestBy;
  const sortOrder = req.query.sortOrder || enumData.defaultSortOrderContestBy;

  const contests = await contestService.getAllContests(contestData)(page, perPage, sortBy, sortOrder, userId);
  res.status(200).json(contests);
};

export default {
  getGrade,
  getGradesByParticipation,
  getAllContests,
  gradeParticipation,
  getParticipationsByContest,
  addAdditionalJurors,
  submitParticipation,
  joinContest,
  getContestById,
  getParticipation,
  createContest,
  isJuror,
};
