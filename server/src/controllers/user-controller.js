import { enumData } from '../common/common.js';
import userService from '../services/user-service.js';
import userData from '../models/user-data.js';
import contestData from '../models/contest-data.js';
import participationData from '../models/participation-data.js';
import { serviceErrorStack } from '../common/error-handler.js';

const getUserById = async (req, res) => {
  const id = +req.params.id;
  const { error, data } = await userService.getUserBy(userData)('id', id);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

const getAllUsers = async (req, res) => {
  const page = +req.query.page || enumData.defaultPage;
  const perPage = +req.query.perPage || enumData.defaultPerPage;
  const sortBy = req.query.sortBy || enumData.defaultSortUserBy;
  const sortOrder = req.query.sortOrder || enumData.defaultSortOrderUserBy;

  const users = await userService.getAllUsers(userData)(page, perPage, sortBy, sortOrder);
  res.status(200).json(users);
};
const getParticipationsByUser = async (req, res) => {
  const userId = req.params.id;
  const { error, data } = await userService.getParticipationsByUser(participationData)(userId);

  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};
const getParticipationByUser = async (req, res) => {
  const contestId = req.params.id;
  const userId = req.user.id;
  const { error, data } = await userService.getParticipationByUser(participationData)(contestId, userId);

  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

const getContestsByUser = async (req, res) => {
  const userId = req.params.id;
  const { error, data } = await userService.getContestsByUser(contestData)(userId);

  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(data);
};

export default {
  getContestsByUser,
  getAllUsers,
  getUserById,
  getParticipationsByUser,
  getParticipationByUser,
};
