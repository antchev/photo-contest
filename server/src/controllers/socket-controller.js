const notificationsInMemory = { data: [], online: new Map() };
import notificationService from '../services/notification-service.js';
import notificationData from '../models/notification-data.js';
import { serviceErrorStack } from '../common/error-handler.js';
/**
 * Sends notifications of single user as an array.
 * @param {number} userId userId
 * @param {object} socket socket entity
 * @example
 * //returns void
 * getNotificationsByUser(1,socket) // get all notifications of user
 */
const getNotificationsByUser = async (userId, socket) => {
  if (notificationsInMemory.online.has(userId)) {
    const { error, data } = await notificationService.getNotificationsByUser(notificationData)(userId);
    if (serviceErrorStack.has(error)) {
      console.log({ error: serviceErrorStack.get(error).message });
    } else {
      socket.emit('notifications', data);
    }
  }
};
/**
 * Sends notifications to all logged in users as an array.
 * @param {object} io instance -
 * @example
 * //returns callback
 * notificationInterval(io)() // get all notifications of user
 */
const notificationInterval = (io) => async () => {
  if (notificationsInMemory.online.size) {
    const { error, data } = await notificationService.getNotificationsByUsers(notificationData)(
      [...notificationsInMemory.online.keys()].toString(),
    );
    if (serviceErrorStack.has(error)) {
      console.log({ error: serviceErrorStack.get(error).message });
    } else {
      notificationsInMemory.data = data;
    }
  }
  //get notifications only if there is at least one online user
  if (notificationsInMemory.online.size) {
    const data = notificationsInMemory.data;
    const notificationCache = new Map();
    //group notifications to users - userId:1 -> [notification:{}, notification:{},...]
    data.map((notification) => {
      if (notificationsInMemory.online.has(notification.userId)) {
        const current = notificationCache.get(notification.userId);
        if (!current) {
          notificationCache.set(notification.userId, [notification]);
        } else {
          current.push(notification);
        }
      }
    });
    //for each group send all notifications to the specific user
    notificationCache.forEach((notifications, userId) => {
      //io send private data to specific socket id
      io.to(notificationsInMemory.online.get(userId)).emit('notifications', notifications);
    });
  }
};
//upon server startup - setup the socket events and the notification interval
const connect = (io) => {
  //the notification itnerval is set to invoke each NOTIFICATION_INTERVAL_SECONDS.
  setInterval(notificationInterval(io), process.env.NOTIFICATION_INTERVAL_SECONDS * 1000);

  //uppon connection - each socket will listen for the events inside connection.
  io.on('connection', (socket) => {
    socket.on('login', (userId) => {
      if (userId) {
        socket.userId = userId; //set userId to socket id
        notificationsInMemory.online.set(socket.userId, socket.id); //set socket as online user - userId -> socket.id
        getNotificationsByUser(userId, socket); //send notificaitons of given socket
      }
    });
    socket.on('logout', () => {
      //destroy user related data with the socket
      notificationsInMemory.online.delete(socket.userId);
      delete socket.userId;
      socket.emit('destroy notifications');
    });
    //destroy user related data with the socket
    socket.on('mark as read', async (notificationId) => {
      const error = await notificationService.markNotificationAsRead(notificationData)(+notificationId).error;
      if (serviceErrorStack.has(error)) {
        socket.emit('error', {
          error: serviceErrorStack.get(error).message,
        });
      }
    });
    //disconnect socket and clear user id from online users
    socket.on('disconnect', () => notificationsInMemory.online.delete(socket.userId));
  });
};

export default { notificationInterval, connect };
