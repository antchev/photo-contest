import dotenv from 'dotenv'; //process.env.PASS
import jwt from 'jsonwebtoken';

dotenv.config();

export const createAccessToken = (payload) => {
  const token = jwt.sign(payload, process.env.ACCESS_PRIVATE_KEY, { expiresIn: +process.env.ACCESS_TOKEN_LIFETIME });
  return token;
};

export const createRefreshToken = (payload) => {
  const token = jwt.sign(payload, process.env.REFRESH_PRIVATE_KEY, { expiresIn: +process.env.REFRESH_TOKEN_LIFETIME });
  return token;
};

export const checkRefreshToken = (token) => {
  return jwt.verify(token, process.env.REFRESH_PRIVATE_KEY);
};
