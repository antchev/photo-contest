// /* eslint-disable no-undef */
// import userService from '../services/user-service.js';
// import userData from '../models/user-data.js';
// import { serviceErrors, serviceErrorStack } from '../common/error-handler.js';
// describe('Users Service Tests', () => {
//   it('Sanity check', () => {
//     expect(1).toEqual(1);
//   });

//   describe('getUserBy() tests', () => {
//     it('getUserBy() should return RECORD_NOT_FOUND error if the user is not found', async () => {
//       const id = 0;
//       const { error, data } = await userService.getUserBy(userData)('id', id);
//       expect(data).toEqual(null);
//       expect(serviceErrorStack.get(error).message).toEqual(serviceErrors.RECORD_NOT_FOUND.message);
//     });

//     it('getUserBy() should retun correct user and no error', async () => {
//       const id = 3;
//       const { error, data } = await userService.getUserBy(userData)('id', id);
//       expect(data.username).toEqual('gosho');
//       expect(error).toEqual(null);
//     });
//   });

//   describe('getAllUsers() tests', () => {
//     it('getAllUsers() should retun paginated user data', async () => {
//       const page = 1;
//       const perPage = 3;
//       const sortBy = 'username';
//       const sortOrder = 'desc';
//       const users = await userService.getAllUsers(userData)(page, perPage, sortBy, sortOrder);
//       expect(users.page).toEqual(page);
//       expect(users.perPage).toEqual(perPage);
//       expect(users.data.length).toEqual(perPage);
//     });
//   });
//   describe('createUser() tests', () => {
//     it('createUser() should retun return DUPLICATE_USERNAME_RECORD error if existing username is being used', async () => {
//       const username = 'anthony';
//       const password = 123456;
//       const firstName = 'Aaaaa';
//       const lastName = 'Bbbbb';
//       const { error, data } = await userService.createUser(userData)(username, password, firstName, lastName);
//       expect(serviceErrorStack.get(error).message).toEqual(serviceErrors.DUPLICATE_USERNAME_RECORD.message);
//     });
//   });

//   it('logInByUserName() should log the user if existing', async () => {
//     const user = await userService.logInByUserName(userData)('Anthony', 'anthony');
//     expect(user.data.lastName).toEqual('Tonev');
//   });
// });
