// /* eslint-disable no-undef */
// import contestService from '../services/contest-service.js';
// import contestData from '../models/contest-data.js';

// import { serviceErrors, serviceErrorStack } from '../common/error-handler.js';
// describe('Contest Service Tests', () => {
//   it('Sanity check', () => {
//     expect(1).toEqual(1);
//   });

//   describe('getContest() tests', () => {
//     it('getContestBy() should return RECORD_NOT_FOUND error if the contest is not found', async () => {
//       const id = 0;
//       const { error, data } = await contestService.getContestBy(contestData)('id', id);
//       expect(data).toEqual(null);
//       expect(serviceErrorStack.get(error).message).toEqual(serviceErrors.RECORD_NOT_FOUND.message);
//     });

//     it('getContestBy() should retun correct contest and no error', async () => {
//       const id = 1;
//       const { error, data } = await contestService.getContestBy(contestData)('id', id);
//       expect(data.title).toEqual('Scare me');
//       expect(error).toEqual(null);
//     });
//   });

//   it('createContest() should return error message if user is not organizer', async () => {
//     const body = {
//       title: 'Test event',
//       category: 'safgdfg fgfdgdfgdfgdf',
//       isInvitational: 1,
//       hasAdditionalJurors: 1,
//       phaseOneDate: 34,
//       phaseTwoDate: 12,
//       users: '8',
//       jurors: '4',
//     };
//     const userId = 1;
//     const { title, category, isInvitational, hasAdditionalJurors, phaseOneDate, phaseTwoDate, users, jurors } = body;

//     const { error, data } = await contestService.createContest(contestData)(
//       title,
//       category,
//       isInvitational,
//       hasAdditionalJurors,
//       phaseOneDate,
//       phaseTwoDate,
//       userId,
//       users,
//       jurors,
//     );
//     expect(data).toEqual(null);
//     expect(serviceErrorStack.get(error).message).toEqual(serviceErrors.DUPLICATE_CONTEST_RECORD.message);
//   });
// });
