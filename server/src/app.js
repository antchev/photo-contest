import express from 'express';
import * as socket from 'socket.io';
import cors from 'cors';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import helmet from 'helmet';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import homeRouter from './routes/home-router.js';
import { upload, resize } from './common/upload.js';
import path from 'path';
import { fileURLToPath } from 'url';
import { notFound, productionErrors, developmentErrors } from './common/error-handler.js';
import authController from './controllers/auth-controller.js';
import userRouter from './routes/user-router.js';
import authRouter from './routes/auth-router.js';
import contestRouter from './routes/contest-router.js';
import dotenv from 'dotenv'; //process.env.PASS
import socketController from './controllers/socket-controller.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

dotenv.config();

const app = express();
passport.use(jwtStrategy);

app.use(cookieParser());
app.use(
  cors({
    origin: [
      'http://localhost:4000',
      `http://${process.env.CLIENT_HOST}:${process.env.CLIENT_PORT}`,
      `http://${process.env.CLIENT_HOST_OUTSIDE}:${process.env.CLIENT_PORT}`,
    ],
    credentials: true,
  }),
);
app.use(helmet());
app.use(bodyParser.json());
app.use(passport.initialize());

const Server = app.listen(process.env.PORT, (err) => {
  if (err) {
    console.log('could not start');
  } else {
    console.log(`Listening on port ${process.env.PORT}`);
  }
});

app.use('/images/', express.static(path.resolve(__dirname + '/../public/uploads/')));
app.use('/home', homeRouter);
app.use('/session', authRouter);
app.use('/users', authController.authenticate, userRouter); //only for registered users
app.use('/contests', authController.authenticate, contestRouter); //only for registered users
app.post('/upload', authController.authenticate, upload.single('file'), resize); //only for registered users

const io = new socket.Server(Server);
socketController.connect(io);

if (process.env.STAGE === 'development') {
  app.use(developmentErrors);
} else if (process.env.STAGE === 'production') {
  app.use(productionErrors);
}

app.use(notFound);
