import { enumData, inputFieldsAliases } from './common.js';

/**
 * Validate the type of the value.
 * @param {any} val - The passed value to be checked.
 * @param {string} type - The type of the value.
 * @param {string} sub - The type of array elements.
 * @example
 * const func = (a,b,arr) => {
 * checkType( a,'integer');
 * checkType( b,'number');
 * checkType( arr,'array','integer');
 * }
 * func(1,2,[1,2,1.23,3]) // throws an error: 1.23 is not an integer.
 */
const checkType = (val, type, sub) => {
  const error = new Error(`${val} is not of type ${type}`);
  if (type === 'integer') {
    if (!Number.isInteger(val)) throw error;
  } else if (type === 'array') {
    if (!Array.isArray(val)) throw error;
    if (sub) val.every((item) => checkType(item, sub));
  } else if (typeof val !== type) throw error;
  return val;
};

const validateStringOfRange = (value, minLen, maxLen) => {
  return [
    (value) => value,
    (value) => typeof value === 'string',
    (value) => value.trim(),
    (value) => value.trim().length >= minLen,
    (value) => value.trim().length <= maxLen,
  ];
};

/**
 * Calls a every callback function on a given value 
 * @param {any} value - The passed value to be asserted.
 * @param {string} errorMsg - The error message thrown if atleast one fails
 * @param {callback} callback fn that tests what the value SHOULD be. The rest of the arguments are callbacks too
 * @example
 *  predicate(
        12,
        `ID should be A valid integer greater or equal to 1`,
        (id) => Number.isInteger(+id),
        (id) => +id >= enumData.minId
      );
 * // all validation functions have to test what the value SHOULD be
 */
const predicate = (value, errorMsg, ...callbacks) => {
  if (!callbacks.every((fn) => fn(value))) {
    throw new Error(errorMsg + ' instead received: ' + value);
  }
};

/**
 * Validate by given type.
 * @param {string} type - The type of validation.
 * @param {any} type - The passed value to be validated.
 * @example
 * validateParam('username',username);
 * // all validation functions have to test what the value SHOULD be
 */

const validateParam = (type, value, prefix = '') => {
  type = prefix + type;
  switch (type) {
    case 'id':
    case 'userId':
      predicate(
        value,
        `${type} should be A valid integer greater or equal to ${enumData.minId}`,
        (id) => Number.isInteger(+id),
        (id) => +id >= enumData.minId,
      );
      break;

    case 'username':
    case 'firstName':
    case 'lastName':
      predicate(
        value,
        `${inputFieldsAliases[type]} should be a string in the range between ${enumData.userNameMinLength} and ${enumData.userNameMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.userNameMinLength, enumData.userNameMaxLength),
      );
      break;

    case 'password':
      predicate(
        value,
        `Password should be a string in the range between ${enumData.passwordMinLength} and ${enumData.passwordMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.passwordMinLength, enumData.passwordMaxLength),
      );
      break;

    case 'title':
      predicate(
        value,
        `Title should be a string in the range between ${enumData.titleMinLength} and ${enumData.titleMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.titleMinLength, enumData.titleMaxLength),
      );
      break;
    case 'story':
      predicate(
        value,
        `Story should be a string in the range between ${enumData.storyMinLength} and ${enumData.storyMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.storyMinLength, enumData.storyMaxLength),
      );
      break;
    case 'photo':
      predicate(
        value,
        'Photo should be one of the following: .png, .jpg, .jpeg',
        (photo) => photo,
        (photo) => checkType(photo, 'string'),
      );
      break;
    case 'jurors':
    case 'users':
      !value ||
        predicate(
          value,
          type + ' should be a string of comma separated ids',
          (CSV) => checkType(CSV, 'string'),
          (CSV) => checkType(CSV.split(',').map(Number), 'array', 'integer'),
        );
      break;
    case 'category':
      predicate(
        value,
        `Category should be a string in the range between ${enumData.categoryMinLength} and ${enumData.categoryMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.categoryMinLength, enumData.categoryMaxLength),
      );
      break;

    case 'isInvitational':
    case 'hasAdditionalJurors':
      predicate(
        value,
        `${type} hould be a 0 or a 1`,
        (value) => Number.isInteger(+value),
        (value) => value === 0 || value === 1,
      );
      break;
    //put hardcoded values in enumData
    case 'phaseOneDate':
      predicate(
        value,
        `${type} should be between ${enumData.minPhaseOneDate} and ${enumData.maxPhaseOneDate} days`,
        (value) => Number.isInteger(+value),
        (value) => +value >= enumData.minPhaseOneDate,
        (value) => +value <= enumData.maxPhaseOneDate,
      );
      break;
    case 'phaseTwoDate':
      predicate(
        value,
        `${type} should be between ${enumData.minPhaseTwoDate} and ${enumData.maxPhaseTwoDate} hours`,
        (value) => Number.isInteger(+value),
        (value) => +value >= enumData.minPhaseTwoDate,
        (value) => +value <= enumData.maxPhaseTwoDate,
      );
      break;
    case 'score':
      predicate(
        value,
        `${type} should be between ${enumData.minRating} and ${enumData.maxRating}`,
        (value) => Number.isInteger(+value),
        (value) => +value >= enumData.minRating,
        (value) => +value <= enumData.maxRating,
      );
      break;
    case 'comment':
      predicate(
        value,
        `Comment should be a string in the range between ${enumData.commentMinLength} and ${enumData.commentMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.commentMinLength, enumData.commentMaxLength),
      );
      break;

    case 'page':
    case 'perPage':
      !value ||
        predicate(
          value,
          `Page should be A valid integer greater or equal to ${enumData.minPage}`,
          (page) => typeof +page === 'number',
          (page) => Number.isInteger(+page),
          (page) => +page >= enumData.minPage,
        );
      break;
    case 'users-sortBy':
      !value ||
        predicate(
          value,
          `Sorting users column should be one of '${[...enumData.sortByUserValues].join('\',\'')}'`,
          (sortBy) => typeof sortBy === 'string',
          (sortBy) => enumData.sortByUserValues.has(sortBy),
        );
      break;

    case 'contests-sortBy':
      !value ||
        predicate(
          value,
          `Sorting contests column should be one of '${[...enumData.sortByContestValues].join('\',\'')}'`,
          (sortBy) => typeof sortBy === 'string',
          (sortBy) => enumData.sortByContestValues.has(sortBy),
        );
      break;

    case 'sortOrder':
      !value ||
        predicate(
          value,
          'Sorting order should be "asc" or "desc"',
          (sortOrder) => typeof sortOrder === 'string',
          (sortOrder) => enumData.sortOrderValues.has(sortOrder),
        );
      break;

    default:
      break;
  }
};

const Schema = {
  ID: ['id'],
  User: ['username', 'password', 'firstName', 'lastName'],
  Session: ['username', 'password'],
  Pagination: ['page', 'perPage', 'sortBy', 'sortOrder'],
  Contest: ['title', 'category', 'isInvitational', 'hasAdditionalJurors', 'phaseOneDate', 'phaseTwoDate'],
  Participation: ['title', 'story', 'photo'],
  Grade: ['score', 'comment'],
};
/**
 * From a given scheme, validate all the values in given request parameter.
 * @param {array} params  - The scheme or an array of strings.
 * @param {string} section - defaults to body but can be params, query or other.
 * @param {boolean} mode - defaults to false, check if param is requered in section(body,params,query).
 * @param {callback} validatorFunction - defaults to validateParam, changes the validation Function used).
 * @example
 * validateByScheme(Schema.User,'body','required'); // requers and validates all user schema props in body.
 * validateByScheme(['id','username', 'password', 'firstName','lastName'],'query','required'); // requers and validates all user schema props in query.
 */

const validateByScheme = (params, section = 'body', mode = false, prefix = '', validatorFunction = validateParam) => {
  return async (req, res, next) => {
    await params.map((param) => {
      if (param in req[section]) {
        validatorFunction(param, req[section][param], prefix);
      } else {
        if (mode === 'required') {
          throw new Error(`${param} is required in ${section}!`);
        }
      }
    });
    return next();
  };
};

export { validateParam, validateByScheme, Schema };
