const enumData = {
  userNameMinLength: 3,
  userNameMaxLength: 45,
  passwordMinLength: 3,
  passwordMaxLength: 45,
  titleMinLength: 5,
  titleMaxLength: 200,
  storyMinLength: 10,
  storyMaxLength: 2000,
  commentMinLength: 10,
  commentMaxLength: 2000,
  categoryMinLength: 5,
  categoryMaxLength: 200,
  minId: 1,
  minPage: 1,
  minRating: 0,
  maxRating: 10,
  minDays: 1,
  defaultPage: 1,
  defaultPerPage: 1000000,
  minPhaseOneDate: 24,
  maxPhaseOneDate: 24 * 30,
  minPhaseTwoDate: 1,
  maxPhaseTwoDate: 24,
  sortOrderValues: new Set(['asc', 'desc']),
  defaultSortContestBy: 'createDate',
  defaultSortOrderContestBy: 'desc',
  sortByContestValues: new Set(['title', 'category', 'createDate', 'phaseOneDate', 'phaseTwoDate']),
  defaultSortUserBy: 'ranking',
  defaultSortOrderUserBy: 'asc',
  sortByUserValues: new Set([
    'username',
    'firstName',
    'lastName',
    'points',
    'level',
    'ranking',
    'isOrganizer',
    'createDate',
  ]),
  thumbnailImageThreshold: 400,
};

const inputFieldsAliases = {
  username: 'User name',
  firstName: 'First name',
  lastName: 'Last name',
};

/**
 * simple date formatter
 * @param {string} dateString - the date to be formatted
 * @example
 * //returns "Monday, November 30, 2020, 6:34 PM"
 * formatDate(new Date())
 */
const formatDate = (dateString) => {
  return new Date(dateString).toLocaleDateString('en-US', {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  });
};

export { enumData, formatDate, inputFieldsAliases };
