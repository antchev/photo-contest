/**
 * Catches any error thrown insidie the given callback
 * @param {callback} fn - The passed callback.
 * @example
 * //returns a function with req,req and next parameters
 * errorCatcher(getContestById)
 */

const errorCatcher = (fn) => {
  return (req, res, next) => {
    return fn(req, res, next).catch(next);
  };
};
// log unexisting endpoints and send
const notFound = (req, res, next) => {
  const error = new Error('Not Found');
  error.status = 404;
  const line = error.stack.split('\n')[1];
  error.stack = `Error: Not Found
  Link: ${req.url}
  ${line}`;
  next(error);
};
//an object with all expected errors and statuses
const serviceErrors = {
  /** Such a record does not exist (when it is expected to exist) */
  RECORD_NOT_FOUND: {
    code: 1,
    status: 404,
    message: 'not found!',
  },
  /** The requirements do not allow more than one of contest  resource */
  DUPLICATE_CONTEST_RECORD: {
    code: 2,
    status: 409,
    message: 'Contest title not available!',
  },
  /** The requirements do not allow more than one of username  resource */
  DUPLICATE_USERNAME_RECORD: {
    code: 3,
    status: 409,
    message: 'Name not available!',
  },
  /** The requirements do not allow such an operation */
  ALREADY_JOINED: {
    code: 4,
    status: 500,
    message: 'You have already joined the contest!',
  },
  /** The requirements do not allow such an operation */
  OPERATION_NOT_PERMITTED: {
    code: 5,
    status: 500,
    message: 'Not allowed!',
  },
  /** username/password mismatch */
  INVALID_SIGNIN: {
    code: 6,
    status: 500,
    message: 'Invalid username/password',
  },
  RECORD_NOT_AVAILABLE: {
    code: 7,
    status: 409,
    message: 'is not available!',
  },

  DELETE_NOT_PERMITTED: {
    code: 8,
    status: 403,
    message: 'You cannot delete!',
  },
  RESOURCE_IS_FORBIDDEN: {
    code: 9,
    status: 403,
    message: 'Action is forbidden',
  },
  RESOURCE_IS_FORBIDDEN_FOR_ORGANIZER: {
    code: 10,
    status: 403,
    message: 'You are an Organizer. You are not allowed to do this action.',
  },
  RESOURCE_IS_FORBIDDEN_FOR_REGULAR: {
    code: 11,
    status: 403,
    message: 'You are not an Organizer. You are not authorized for this action.',
  },
  RECORD_GONE: {
    code: 12,
    status: 410,
    message: 'Item no longer available!',
  },

  PHASE_ONE_IS_OVER: {
    code: 13,
    status: 403,
    message: 'You can not join. Contest phase one is already closed.',
  },

  ALREADY_PARTICIPATING: {
    code: 14,
    status: 403,
    message: 'You have already submitted a photo for this contest.',
  },
  NOT_YOUR_PARTICIPATION: {
    code: 15,
    status: 403,
    message: 'You are trying to participate as someone else.',
  },
  NOT_A_JUROR: {
    code: 16,
    status: 403,
    message: 'You are not a juror of this contest.',
  },
  NOT_IN_PHASE_TWO: {
    code: 17,
    status: 403,
    message: 'Contest is not in grading phase.',
  },

  ALREADY_GRADED: {
    code: 18,
    status: 403,
    message: 'You have already graded this photo.',
  },

  NOT_INVITED: {
    code: 19,
    status: 403,
    message: 'This contest is invitational but you are not invited.',
  },
  A_JUROR: {
    code: 20,
    status: 403,
    message: 'You can not join a contest which you are a juror to.',
  },
  CAN_NOT_BE_BOTH_JUROR_AND_INVITED: {
    code: 21,
    status: 403,
    message: 'You can not invite the same user for a juror and a participant for the same contest.',
  },
};
//converts the error object into a map with a unique code id as key
const serviceErrorStack = Object.values(serviceErrors).reduce((acc, item) => {
  acc.set(item.code, { status: item.status, message: item.message });
  return acc;
}, new Map());

//sends errors with error message only - next is used from express internally
const productionErrors = (err, req, res, next) => {
  res.status(err.status || 500).send({ error: err.message });
};
//sends errors with error message and call stack - next is used from express internally
const developmentErrors = (err, req, res, next) => {
  err.stack = err.stack || '';
  const errorDetails = {
    error: err.message,
    status: err.status,
    stackHighlighted: err.stack.split('\n').reduce((acc, item, index) => {
      acc[index] = item.trim();
      return acc;
    }, {}),
  };
  res.status(err.status || 500).send(errorDetails);
};

export { errorCatcher, notFound, serviceErrors, productionErrors, developmentErrors, serviceErrorStack };
