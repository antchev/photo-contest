import { extname, resolve } from 'path';
import { enumData } from './common.js';

import multer from 'multer';
import sharp from 'sharp';
import Calipers from 'calipers';

const sizeOf = Calipers('png', 'jpeg');

/*Local storage multer settings */
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, process.env.PARTICIPATION_PATH + 'org/');
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});
//image upload restrictions - only jpeg,jpg,png and gif are allowed.
const checkFileType = (file, cb) => {
  const filetypes = /jpeg|jpg|png|gif/;
  const ext = filetypes.test(extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);
  try {
    if (mimetype && ext) {
      return cb(null, true);
    } else {
      throw new Error('Images Only');
    }
  } catch (err) {
    console.log(err.message);
  }
};
//upload image settings 10mb limit of an image
const upload = multer({
  storage: storage,
  limits: { fileSize: 10000000 },
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
});

/*
Resizing the image and saving it as a separate thumbnail.

If image width is bigger than a specific threshold
the image is resized to the given threshold
proportionaly will set the height 
so the image will retain its original ratio

Thumbnails are saved in /public/participations/thumbs/
Original images are saved in /public/participations/org/
*/
const resize = async (req, res) => {
  try {
    const { filename: image } = req.file;
    const imageData = await sizeOf.measure(req.file.path);
    const dims = {
      width: imageData.pages[0].width,
      height: imageData.pages[0].height,
    };
    if (dims.width > enumData.thumbnailImageThreshold) {
      const coef = dims.width / enumData.thumbnailImageThreshold;
      dims.width /= coef;
      dims.height /= coef;
    }
    await sharp(req.file.path, { failOnError: false })
      .resize(Math.floor(dims.width), Math.floor(dims.height))
      .toFormat('jpeg')
      .jpeg({
        quality: 100,
        chromaSubsampling: '4:4:4',
        force: true,
      })
      .withMetadata()
      .toFile(resolve(req.file.destination, '../thumbs/', image.replace(/\.(jpeg|png|PNG|jpg)$/, '.jpg')));

    return res.json({
      thumb_url: process.env.PARTICIPATION_PATH + 'thumbs/' + image.replace(/\.(jpeg|png|PNG|jpg)$/, '.jpg'),
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error!');
  }
};

export { upload, resize };
