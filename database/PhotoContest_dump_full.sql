CREATE DATABASE  IF NOT EXISTS `photocontest` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `photocontest`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 93.123.78.206    Database: photocontest
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.5-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contests`
--

DROP TABLE IF EXISTS `contests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `is_invitational` tinyint(1) NOT NULL DEFAULT 0,
  `has_additional_jurors` tinyint(1) NOT NULL DEFAULT 0,
  `is_graded` tinyint(1) NOT NULL DEFAULT 0,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `phase1_date` datetime NOT NULL,
  `phase2_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`),
  KEY `fk_contests_users1_idx` (`user_id`),
  CONSTRAINT `fk_contests_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contests`
--

LOCK TABLES `contests` WRITE;
/*!40000 ALTER TABLE `contests` DISABLE KEYS */;
INSERT INTO `contests` VALUES (1,'Golden Autumn','Autumn','cover_3.jpg',0,0,0,'2020-11-29 13:36:38','2020-12-03 13:36:38','2020-12-04 00:36:38',4),(2,'Friendly Insects','Insects','cover_4.jpg',1,0,1,'2020-11-29 13:40:56','2020-11-29 19:13:56','2020-11-29 20:13:56',4),(3,'Colors','Abstract Liquid','cover_7.jpg',1,0,1,'2020-11-29 13:42:54','2020-11-29 19:14:54','2020-11-29 20:14:54',4),(4,'Critters','Animals','cover_15.jpg',0,0,0,'2020-11-29 13:45:35','2020-12-11 13:45:35','2020-12-12 03:45:35',4),(5,'On Top of A Mountain','Winter','cover_18.jpg',0,0,0,'2020-11-29 13:48:24','2020-12-05 13:48:24','2020-12-06 00:48:24',4),(6,'Constellation','Astronomy','cover_19.jpg',0,0,0,'2020-11-29 13:50:53','2020-12-04 13:50:53','2020-12-04 22:50:53',4),(7,'Champions League emotions','Football','cover_2.jpg',0,0,0,'2020-11-29 13:51:36','2020-12-12 13:51:36','2020-12-13 14:51:36',2),(8,'Delicious treats','Foods','cover_6.jpg',0,0,0,'2020-11-29 13:53:28','2020-12-10 13:53:28','2020-12-11 13:53:28',2),(9,'Golden Morning','Sunrise','cover_8.jpg',0,0,0,'2020-11-29 13:58:17','2020-12-03 14:58:17','2020-12-03 22:58:17',4),(10,'Lights','Abstract','cover_0.jpg',0,0,0,'2020-11-29 13:59:45','2020-12-05 13:59:45','2020-12-05 23:59:45',4),(11,'Summer mountain','Nature','cover_17.jpg',1,0,1,'2020-11-29 14:00:10','2020-11-29 17:00:10','2020-11-29 17:47:10',2),(12,'Modern Buildings','Architecture','cover_13.jpg',0,0,0,'2020-11-29 14:02:37','2020-12-08 14:02:37','2020-12-08 20:02:37',4),(13,'Sunset over the sea','Sunset','cover_16.jpg',0,0,0,'2020-11-29 14:07:13','2020-12-06 14:07:13','2020-12-06 15:07:13',2),(14,'Waves power','Ocean','cover_9.jpg',1,0,0,'2020-11-29 14:12:52','2020-12-02 19:12:52','2020-12-03 19:12:52',2),(15,'Mystic Lakes','Lakes','cover_1.jpg',0,0,0,'2020-11-29 14:12:55','2020-12-08 14:12:55','2020-12-08 20:12:55',4),(16,'Bloom','Abstract Flowers','cover_12.jpg',1,0,0,'2020-11-29 11:06:00','2020-12-02 11:06:00','2020-12-03 11:06:00',4),(17,'Bouquet','Flowers','cover_5.jpg',0,0,0,'2020-11-29 14:17:57','2020-12-09 14:17:57','2020-12-09 20:17:57',4),(18,'Golden Hour','Sunset','cover_default.jpg',1,0,0,'2020-11-29 14:19:42','2020-12-02 20:19:42','2020-12-03 20:19:42',4),(19,'Blue rocks','Abstract nature','cover_10.jpg',0,0,0,'2020-11-29 14:21:09','2020-12-05 14:21:09','2020-12-06 00:21:09',4),(20,'World of mirrors','Reflection','cover_11.jpg',0,0,0,'2020-11-29 12:23:03','2020-12-03 12:23:03','2020-12-04 12:23:03',4),(21,'Sakura','Spring','cover_14.jpg',0,0,0,'2020-11-29 14:23:58','2020-12-03 14:23:58','2020-12-03 20:23:58',4),(22,'Dolphins','Sea Mamals','cover_16.jpg',0,1,1,'2020-11-30 10:56:37','2020-11-30 10:56:37','2020-11-30 19:56:37',1),(23,'Lilium','Nature','cover_1.jpg',1,1,0,'2020-12-02 10:39:45','2020-12-15 11:39:45','2020-12-16 11:49:45',1);
/*!40000 ALTER TABLE `contests` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`telerik`@`%`*/ /*!50003 TRIGGER `photocontest`.`contests_AFTER_INSERT` AFTER INSERT ON `contests` FOR EACH ROW
BEGIN
	CALL add_organizers_as_jurors(NEW.id);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `grades`
--

DROP TABLE IF EXISTS `grades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score` int(11) NOT NULL,
  `comment` text NOT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `participation_id` int(11) NOT NULL,
  `juror_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_grades_participations1_idx` (`participation_id`),
  CONSTRAINT `fk_grades_participations1` FOREIGN KEY (`participation_id`) REFERENCES `participations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grades`
--

LOCK TABLES `grades` WRITE;
/*!40000 ALTER TABLE `grades` DISABLE KEYS */;
INSERT INTO `grades` VALUES (1,5,'Contrast is a little off','2020-11-29 17:28:36',47,2),(2,7,'Magnificent creatures in ther natural habitat...','2020-11-29 17:29:05',47,1),(3,9,'Wonderful! The only thing is boats are a little bit off','2020-11-29 17:29:22',49,2),(4,4,'Colors are fine, but sky contrast is terrible','2020-11-29 17:30:16',50,2),(5,2,'Very dark - not whar we are looking for as a mood. We are looking for a more happy pictures.','2020-11-29 17:30:24',51,1),(6,0,'Mountain is great, but where is the summer? I see snow here','2020-11-29 17:30:48',51,2),(7,1,'Very foggy indeed. One star!','2020-11-29 17:31:19',54,1),(8,0,'Sky is terrible and I see spring blooms here, summer is not in the photo.','2020-11-29 17:31:31',54,2),(9,8,'Magnificent!','2020-11-29 17:31:41',49,1),(10,10,'I like everything about it!','2020-11-29 17:32:05',53,2),(11,9,'Excellent! But a little bit over saturated for my taste','2020-11-29 17:32:36',52,2),(12,5,'Great shot - I like the yellow dots.','2020-11-29 17:32:49',50,1),(13,8,'Amazing pallet of different summer colors.','2020-11-29 17:33:27',53,1),(14,9,'Rila lakes, ah Rila lakes.','2020-11-29 17:33:53',52,1),(15,7,'Carefull you can get hurt.','2020-11-29 17:34:09',55,1),(16,2,'Not so much of a summer but sill nice.','2020-11-29 17:34:27',147,1),(17,7,'This is a Divine intervention - when God comes down of heaven and gives  justice upon thee. ','2020-11-29 17:35:37',56,1),(18,8,'I like the clouds and the angle.','2020-11-29 17:37:21',47,4),(19,8,'Cristal lake.','2020-11-29 17:38:41',49,4),(20,5,'It is fine for a junkie shot.','2020-11-29 17:39:01',50,4),(21,0,'Are you sure this is summer?','2020-11-29 17:39:17',51,4),(22,3,'Nice shot but next time pick a clear day. Can\'t see a thing. ','2020-11-29 17:40:16',54,4),(23,10,'Amazing colors and nice mood.','2020-11-29 17:40:39',53,4),(24,6,'Very summerish and yellow.','2020-11-29 17:41:38',52,4),(25,6,'I like tho rocky foreground and the sun.','2020-11-29 17:42:32',55,4),(26,5,'Great summer vibes on the foreground. Great winter vibes on the background.','2020-11-29 17:43:05',147,4),(27,4,'Too religious for my taste.','2020-11-29 17:43:29',56,4),(28,10,'Great! Personal favorite','2020-11-29 17:43:35',55,2),(29,8,'Very nice, I like it','2020-11-29 17:44:03',147,2),(30,10,'It\'s like God is smiling over the valley!','2020-11-29 17:44:37',56,2),(31,6,'It is like a helicoter really haha','2020-11-29 19:42:26',1,1),(32,6,'This is a scary dude','2020-11-29 19:42:52',1,2),(33,6,'Looks like a goofy.','2020-11-29 19:43:05',2,1),(34,10,'Wonderful! I\'ll take this one as a home pet','2020-11-29 19:43:19',2,2),(35,4,'Blury and colors are not so great','2020-11-29 19:43:57',3,2),(36,5,'Lady lady bug - lady bug','2020-11-29 19:44:44',3,1),(37,5,'I like the colors alot.','2020-11-29 19:46:28',4,1),(38,10,'Beauty! Colors are gorgeous!','2020-11-29 19:46:55',13,2),(39,0,'Nice photo but this contest is for FRIENDLY bugs.','2020-11-29 19:47:12',9,1),(40,7,'Vengence of the dew.','2020-11-29 19:47:49',5,1),(41,3,'This is a hummingbird dude.','2020-11-29 19:48:22',6,1),(42,10,'How should we name him? Ringo, Paul, John, George...maybe Gosho?','2020-11-29 19:49:14',4,2),(43,4,'Arbeit, arbeit!','2020-11-29 19:50:07',7,1),(44,5,'Do you have animal planet too? Nice photo btw.','2020-11-29 19:50:35',8,1),(45,8,'Looks a little bit aggressive','2020-11-29 19:50:37',5,2),(46,4,'She totaly is hehe','2020-11-29 19:50:47',10,1),(47,4,'Very greedy indeed.','2020-11-29 19:51:11',11,1),(48,6,'How did you manage to get it so close in flight?','2020-11-29 19:51:13',6,2),(49,5,'Looks fake','2020-11-29 19:51:47',7,2),(50,0,'Ok - friendly insects only!','2020-11-29 19:51:48',14,1),(51,3,'Wow - distance please','2020-11-29 19:52:09',12,1),(52,0,'Too much CGI, my friend','2020-11-29 19:52:11',9,2),(53,10,'for (var i=1; i < 101; i++){\n    if (i % 15 == 0) console.log(\"FizzBuzz\");\n    else if (i % 3 == 0) console.log(\"Fizz\");\n    else if (i % 5 == 0) console.log(\"Buzz\");\n    else console.log(i);\n}','2020-11-29 19:52:34',13,1),(54,8,'Is this real life? Bugs are gonna take over us!','2020-11-29 19:52:36',8,2),(55,10,'Now this is beautiful and romantic','2020-11-29 19:52:55',10,2),(56,6,'Heary is this one.','2020-11-29 19:53:14',15,1),(57,8,'One more until retirement.','2020-11-29 19:53:39',17,1),(58,7,'A+ for macro, but a gross little fellow','2020-11-29 19:53:47',12,2),(59,8,'The rocky of lady bugs.','2020-11-29 19:54:00',16,1),(60,6,'Nice species','2020-11-29 19:54:24',15,2),(61,7,'Yes, being on top is the best!','2020-11-29 19:54:48',16,2),(62,4,'Fufufufufufufuufufufufufufufufuufuf','2020-11-29 20:01:03',1,4),(63,4,'Strong scralet colors.','2020-11-29 20:01:24',3,4),(64,7,'Hah what a cute little fella.','2020-11-29 20:01:43',4,4),(65,10,'Now this is what I call art!','2020-11-29 20:01:55',167,2),(66,8,'The dew warrior of the morning.','2020-11-29 20:02:11',5,4),(67,7,'Nice idea, but the execution could be better','2020-11-29 20:02:25',39,2),(68,0,'This is a Hummingbird man.','2020-11-29 20:02:28',6,4),(69,8,'Nice shot - the wings are seen like frozen in time.','2020-11-29 20:02:49',7,4),(70,7,'Good question, beats me...','2020-11-29 20:02:56',38,2),(71,0,'Can\'t you read the title - it says \"Friendly\" insects!','2020-11-29 20:03:16',9,4),(72,3,'Not particularly fond of it','2020-11-29 20:03:25',37,2),(73,9,'I like it very much','2020-11-29 20:03:44',36,2),(74,5,'This little guy is an anthena hoarder.','2020-11-29 20:04:02',8,4),(75,7,'Beautiful, but looks a little bit too fake','2020-11-29 20:04:19',35,2),(76,5,'\"No she didn\'t\"','2020-11-29 20:04:22',10,4),(77,10,'This one is great!','2020-11-29 20:04:38',34,2),(78,7,'Poor thing - consumed by greed.','2020-11-29 20:04:55',11,4),(79,7,'Good combination of colors','2020-11-29 20:05:02',33,2),(80,5,'You are invading my space!','2020-11-29 20:05:23',12,4),(81,10,'The colors - the face - I love everything about this picture. Amazing shot!','2020-11-29 20:05:53',13,4),(82,7,'Good idea, light could be better','2020-11-29 20:05:57',32,2),(83,0,'Scary picture.','2020-11-29 20:06:11',14,4),(84,1,'Work, work.','2020-11-29 20:06:27',17,4),(85,1,'Chubacca butterfly.','2020-11-29 20:06:44',15,4),(86,5,'On the top - again!','2020-11-29 20:07:00',16,4),(87,10,'A personal favorite of mine','2020-11-29 20:07:54',167,4),(88,0,'I don\'t think it\'s suitable','2020-11-29 20:08:29',32,4),(89,6,'Phycadelic liquid shroom.','2020-11-29 20:08:34',32,1),(90,7,'Looks nice, but is it real?','2020-11-29 20:09:12',35,4),(91,6,'It reminds me of the liquid clocks of Dali.','2020-11-29 20:09:18',33,1),(92,8,'Looks like beer to me, so bonus points on top','2020-11-29 20:09:37',36,4),(93,7,'Are these bubbes near a pack of Skittles? I want to eat some skittles now.','2020-11-29 20:10:01',34,1),(94,1,'Terrible, is this your best shot?','2020-11-29 20:10:08',37,4),(95,7,'Balance is everything!','2020-11-29 20:10:13',35,1),(96,6,'It looks like some craters on a strange planet.','2020-11-29 20:10:38',36,1),(97,7,'Well, I guess you have to always strive to be over but going under for a bit can be beneficial sometimes.','2020-11-29 20:10:52',38,4),(98,3,'Nice try. I almost like it.','2020-11-29 20:11:02',37,1),(99,0,'Not abstract at all. Where are the colors?','2020-11-29 20:12:02',38,1),(100,6,'Why does it give me french vibes?','2020-11-29 20:12:21',39,1),(101,10,'Superb. I love the colors. And the ripples. Amazing shot.','2020-11-29 20:12:47',167,1),(102,9,'I like it alot!','2020-11-30 12:22:33',168,41),(103,6,'They are friends indeed.','2020-11-30 12:26:34',169,41),(104,0,'Wrong category.','2020-11-30 12:26:50',170,41),(105,7,'Nice shot.','2020-11-30 12:27:12',171,41),(106,10,'Stunning picture!','2020-11-30 12:27:36',174,41),(107,8,'Haha so funny!','2020-11-30 12:28:01',172,41),(108,9,'Love is Blind!','2020-11-30 12:28:24',175,41),(109,6,'Whoops hahaha.','2020-11-30 12:28:36',173,41),(110,3,'Meh - not so good.','2020-11-30 12:40:03',178,41),(111,6,'This is getting old.','2020-11-30 12:40:17',177,41),(112,8,'Nice I like it!','2020-11-30 12:44:21',180,41),(113,6,'Reminds me of the movie - Free willy','2020-11-30 12:45:56',176,41),(114,10,'Amazing - simply amazing.','2020-11-30 12:46:58',174,1),(115,0,'Wrong category. Try banana category.','2020-11-30 12:47:24',170,1),(116,7,'Nice shot!','2020-11-30 12:51:31',175,1),(117,6,'Funny shot.','2020-11-30 12:51:48',173,1),(118,2,'........................................','2020-11-30 12:52:09',178,1),(119,4,'I\'ve seen more unsual couples than this.','2020-11-30 12:52:42',177,1),(120,7,'Nice picture - I like it.','2020-11-30 12:53:02',180,1),(121,3,'Wanna go scating with \"C\" not \"K\"','2020-11-30 12:53:35',176,1),(122,9,'Hey! How are you?','2020-11-30 12:54:03',168,1),(123,6,'Oh he is Jonny good fellou - Oh he is Johny good pal.','2020-11-30 12:54:46',169,1),(124,9,'Very nice shot!','2020-11-30 12:57:32',168,4),(125,8,'Another good one!','2020-11-30 12:57:49',169,4),(126,1,'You get only one star from me.','2020-11-30 12:58:21',170,4),(127,6,'Weeeeeeeeeeeeeeeeeeeeee','2020-11-30 12:58:31',171,4),(128,10,'Stunning shot!','2020-11-30 12:58:46',174,4),(129,9,'Ourch that hurt!','2020-11-30 12:59:09',172,4),(130,8,'Amazing shot I like it.','2020-11-30 12:59:23',175,4),(131,6,'Bubble farts.','2020-11-30 13:00:20',173,4),(132,2,'Needs more saturation.','2020-11-30 13:02:58',178,4),(133,4,'Love is in the air - or in the water in this case.','2020-11-30 13:03:16',177,4),(134,7,'A group of dolphins is called a \"school\" or a \"pod\". Male dolphins are called \"bulls\", females \"cows\" and young dolphins are called \"calves\".','2020-11-30 13:04:16',180,4),(135,6,'I like this one - good job.','2020-11-30 13:04:30',176,4),(136,10,'Hansom fellow','2020-11-30 14:40:11',168,2),(137,7,'LOL, nice one','2020-11-30 14:40:52',170,2),(138,4,'Can\'t say I like it','2020-11-30 14:41:13',174,2),(139,10,'Excellent!','2020-11-30 14:41:28',173,2),(140,7,'Funny dude','2020-11-30 14:54:51',172,2),(141,10,'Wow, magnificent!','2020-12-01 18:43:09',181,2),(154,9,'Great capture! You have a talent for macro.','2020-12-03 09:13:15',95,4),(155,10,'Reminds of the little prince! His little planet full of boabab trees. Great job.','2020-12-03 09:13:47',95,1),(156,0,'Nice photo, but the wrong category, mate.','2020-12-03 09:14:05',77,4),(157,0,'Wrong category. It is a plant but not a blooming one.','2020-12-03 09:15:09',77,1),(158,7,'I like it, you have captured it in a special moment.','2020-12-03 09:15:10',78,4),(159,8,'Looks like a hermid crab. Very abstract.','2020-12-03 09:15:45',78,1),(160,5,'I like the idea, but the execution could be a little better.','2020-12-03 09:16:23',91,4),(161,5,'At first I haven\'t saw the bee. Great detail.','2020-12-03 09:17:51',82,1),(162,1,'Looks too fake to me','2020-12-03 09:19:45',80,4),(163,4,'They grow so fast....','2020-12-03 09:20:07',79,1),(164,2,'I lke the idea, but everything else about it puts me off.','2020-12-03 09:20:23',89,4),(165,5,'Great macro!','2020-12-03 09:21:15',84,4),(166,2,'I\'m not particularly fond of it.','2020-12-03 09:21:40',81,4);
/*!40000 ALTER TABLE `grades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jurors`
--

DROP TABLE IF EXISTS `jurors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jurors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contest_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jurors_contests1_idx` (`contest_id`),
  KEY `fk_jurors_users1_idx` (`user_id`),
  CONSTRAINT `fk_jurors_contests1` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jurors_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jurors`
--

LOCK TABLES `jurors` WRITE;
/*!40000 ALTER TABLE `jurors` DISABLE KEYS */;
INSERT INTO `jurors` VALUES (1,1,1),(2,1,2),(3,1,4),(4,2,1),(5,2,2),(6,2,4),(7,3,1),(8,3,2),(9,3,4),(10,4,1),(11,4,2),(12,4,4),(13,5,1),(14,5,2),(15,5,4),(16,6,1),(17,6,2),(18,6,4),(19,7,1),(20,7,2),(21,7,4),(22,8,1),(23,8,2),(24,8,4),(25,9,1),(26,9,2),(27,9,4),(28,10,1),(29,10,2),(30,10,4),(31,11,1),(32,11,2),(33,11,4),(34,12,1),(35,12,2),(36,12,4),(37,13,1),(38,13,2),(39,13,4),(40,14,1),(41,14,2),(42,14,4),(43,15,1),(44,15,2),(45,15,4),(46,16,1),(47,16,2),(48,16,4),(49,17,1),(50,17,2),(51,17,4),(52,18,1),(53,18,2),(54,18,4),(55,19,1),(56,19,2),(57,19,4),(58,20,1),(59,20,2),(60,20,4),(61,21,1),(62,21,2),(63,21,4),(64,22,1),(65,22,2),(66,22,4),(67,22,41),(68,23,1),(69,23,2),(70,23,4),(71,23,41);
/*!40000 ALTER TABLE `jurors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `levels`
--

DROP TABLE IF EXISTS `levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `required_points` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `levels`
--

LOCK TABLES `levels` WRITE;
/*!40000 ALTER TABLE `levels` DISABLE KEYS */;
INSERT INTO `levels` VALUES (1,'Junkie',0),(2,'Enthusiast',51),(3,'Master',151),(4,'Wise and Benevolent Photo Dictator',1001);
/*!40000 ALTER TABLE `levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `type` varchar(45) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notifications_users1_idx` (`user_id`),
  KEY `fk_notifications_contests1_idx` (`contest_id`),
  CONSTRAINT `fk_notifications_contests1` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=790 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',1,3,1),(2,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',1,19,1),(3,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',1,20,1),(4,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',1,21,1),(5,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,22,1),(6,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,23,1),(7,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,24,1),(8,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,25,1),(9,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,26,1),(10,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,27,1),(11,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',1,28,1),(12,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,29,1),(13,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,30,1),(14,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,31,1),(15,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',1,32,1),(16,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,33,1),(17,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,34,1),(18,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',1,35,1),(19,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,36,1),(20,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,37,1),(21,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,38,1),(22,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,39,1),(23,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',1,41,1),(24,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,42,1),(25,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,43,1),(26,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,44,1),(27,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,45,1),(28,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',1,46,1),(29,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',1,47,1),(30,'The contest has just started, you can join until Dec 3rd, 13:36','open','2020-11-29 13:36:38',0,48,1),(32,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',1,3,2),(33,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',1,21,2),(34,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',0,22,2),(35,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',0,23,2),(36,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',0,25,2),(37,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',1,28,2),(38,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',0,29,2),(39,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',1,32,2),(40,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',0,34,2),(41,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',1,35,2),(42,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',0,36,2),(43,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',0,39,2),(44,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',1,41,2),(45,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',0,43,2),(46,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',0,45,2),(47,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',1,46,2),(48,'The contest has just started, you can join until Dec 9th, 13:40','open','2020-11-29 13:40:56',1,47,2),(63,'The contest has just started, you can join until Dec 4th, 13:42','open','2020-11-29 13:42:54',1,20,3),(64,'The contest has just started, you can join until Dec 4th, 13:42','open','2020-11-29 13:42:54',1,21,3),(65,'The contest has just started, you can join until Dec 4th, 13:42','open','2020-11-29 13:42:54',0,26,3),(66,'The contest has just started, you can join until Dec 4th, 13:42','open','2020-11-29 13:42:54',0,27,3),(67,'The contest has just started, you can join until Dec 4th, 13:42','open','2020-11-29 13:42:54',0,30,3),(68,'The contest has just started, you can join until Dec 4th, 13:42','open','2020-11-29 13:42:54',0,31,3),(69,'The contest has just started, you can join until Dec 4th, 13:42','open','2020-11-29 13:42:54',0,42,3),(70,'The contest has just started, you can join until Dec 4th, 13:42','open','2020-11-29 13:42:54',0,48,3),(78,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',1,3,4),(79,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',1,19,4),(80,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',1,20,4),(81,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',1,21,4),(82,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,22,4),(83,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,23,4),(84,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,24,4),(85,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,25,4),(86,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,26,4),(87,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,27,4),(88,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',1,28,4),(89,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,29,4),(90,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,30,4),(91,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,31,4),(92,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',1,32,4),(93,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,33,4),(94,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,34,4),(95,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',1,35,4),(96,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,36,4),(97,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,37,4),(98,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,38,4),(99,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,39,4),(100,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',1,41,4),(101,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,42,4),(102,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,43,4),(103,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,44,4),(104,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,45,4),(105,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',1,46,4),(106,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',1,47,4),(107,'The contest has just started, you can join until Dec 11th, 13:45','open','2020-11-29 13:45:36',0,48,4),(109,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',1,3,5),(110,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',1,19,5),(111,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',1,20,5),(112,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',1,21,5),(113,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,22,5),(114,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,23,5),(115,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,24,5),(116,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,25,5),(117,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,26,5),(118,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,27,5),(119,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',1,28,5),(120,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,29,5),(121,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,30,5),(122,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,31,5),(123,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',1,32,5),(124,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,33,5),(125,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,34,5),(126,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',1,35,5),(127,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,36,5),(128,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,37,5),(129,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,38,5),(130,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,39,5),(131,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',1,41,5),(132,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,42,5),(133,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,43,5),(134,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,44,5),(135,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,45,5),(136,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',1,46,5),(137,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',1,47,5),(138,'The contest has just started, you can join until Dec 5th, 13:48','open','2020-11-29 13:48:24',0,48,5),(140,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',1,3,6),(141,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',1,19,6),(142,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',1,20,6),(143,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',1,21,6),(144,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,22,6),(145,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,23,6),(146,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,24,6),(147,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,25,6),(148,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,26,6),(149,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,27,6),(150,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',1,28,6),(151,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,29,6),(152,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,30,6),(153,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,31,6),(154,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',1,32,6),(155,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,33,6),(156,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,34,6),(157,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',1,35,6),(158,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,36,6),(159,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,37,6),(160,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,38,6),(161,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,39,6),(162,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',1,41,6),(163,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,42,6),(164,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,43,6),(165,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,44,6),(166,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,45,6),(167,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',1,46,6),(168,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',1,47,6),(169,'The contest has just started, you can join until Dec 4th, 13:50','open','2020-11-29 13:50:53',0,48,6),(171,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',1,3,7),(172,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',1,19,7),(173,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',1,20,7),(174,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',1,21,7),(175,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,22,7),(176,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,23,7),(177,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,24,7),(178,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,25,7),(179,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,26,7),(180,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,27,7),(181,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',1,28,7),(182,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,29,7),(183,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,30,7),(184,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,31,7),(185,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',1,32,7),(186,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,33,7),(187,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,34,7),(188,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',1,35,7),(189,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,36,7),(190,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,37,7),(191,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,38,7),(192,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,39,7),(193,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',1,41,7),(194,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,42,7),(195,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,43,7),(196,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,44,7),(197,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,45,7),(198,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',1,46,7),(199,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',1,47,7),(200,'The contest has just started, you can join until Dec 2nd, 13:51','open','2020-11-29 13:51:36',0,48,7),(202,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,3,8),(203,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,19,8),(204,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,20,8),(205,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,21,8),(206,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,22,8),(207,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,23,8),(208,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,24,8),(209,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,25,8),(210,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,26,8),(211,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,27,8),(212,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,28,8),(213,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,29,8),(214,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,30,8),(215,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,31,8),(216,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,32,8),(217,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,33,8),(218,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,34,8),(219,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,35,8),(220,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,36,8),(221,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,37,8),(222,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,38,8),(223,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,39,8),(224,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,41,8),(225,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,42,8),(226,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,43,8),(227,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,44,8),(228,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,45,8),(229,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,46,8),(230,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',1,47,8),(231,'The contest has just started, you can join until Dec 2nd, 13:53','open','2020-11-29 13:53:28',0,48,8),(233,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,3,9),(234,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,19,9),(235,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,20,9),(236,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,21,9),(237,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,22,9),(238,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,23,9),(239,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,24,9),(240,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,25,9),(241,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,26,9),(242,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,27,9),(243,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,28,9),(244,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,29,9),(245,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,30,9),(246,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,31,9),(247,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,32,9),(248,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,33,9),(249,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,34,9),(250,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,35,9),(251,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,36,9),(252,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,37,9),(253,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,38,9),(254,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,39,9),(255,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,41,9),(256,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,42,9),(257,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,43,9),(258,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,44,9),(259,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,45,9),(260,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,46,9),(261,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',1,47,9),(262,'The contest has just started, you can join until Dec 3rd, 13:58','open','2020-11-29 13:58:18',0,48,9),(264,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,3,10),(265,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,19,10),(266,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,20,10),(267,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,21,10),(268,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,22,10),(269,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,23,10),(270,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,24,10),(271,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,25,10),(272,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,26,10),(273,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,27,10),(274,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,28,10),(275,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,29,10),(276,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,30,10),(277,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,31,10),(278,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,32,10),(279,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,33,10),(280,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,34,10),(281,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,35,10),(282,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,36,10),(283,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,37,10),(284,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,38,10),(285,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,39,10),(286,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,41,10),(287,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,42,10),(288,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,43,10),(289,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,44,10),(290,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,45,10),(291,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,46,10),(292,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',1,47,10),(293,'The contest has just started, you can join until Dec 5th, 13:59','open','2020-11-29 13:59:45',0,48,10),(295,'The contest has just started, you can join until Dec 13th, 14:00','open','2020-11-29 14:00:10',1,3,11),(296,'The contest has just started, you can join until Dec 13th, 14:00','open','2020-11-29 14:00:10',1,19,11),(297,'The contest has just started, you can join until Dec 13th, 14:00','open','2020-11-29 14:00:10',1,21,11),(298,'The contest has just started, you can join until Dec 13th, 14:00','open','2020-11-29 14:00:10',0,23,11),(299,'The contest has just started, you can join until Dec 13th, 14:00','open','2020-11-29 14:00:10',1,28,11),(300,'The contest has just started, you can join until Dec 13th, 14:00','open','2020-11-29 14:00:10',0,31,11),(301,'The contest has just started, you can join until Dec 13th, 14:00','open','2020-11-29 14:00:10',1,41,11),(302,'The contest has just started, you can join until Dec 13th, 14:00','open','2020-11-29 14:00:10',0,42,11),(303,'The contest has just started, you can join until Dec 13th, 14:00','open','2020-11-29 14:00:10',0,44,11),(304,'The contest has just started, you can join until Dec 13th, 14:00','open','2020-11-29 14:00:10',1,46,11),(310,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,3,12),(311,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,19,12),(312,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,20,12),(313,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,21,12),(314,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,22,12),(315,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,23,12),(316,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,24,12),(317,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,25,12),(318,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,26,12),(319,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,27,12),(320,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,28,12),(321,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,29,12),(322,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,30,12),(323,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,31,12),(324,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,32,12),(325,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,33,12),(326,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,34,12),(327,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,35,12),(328,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,36,12),(329,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,37,12),(330,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,38,12),(331,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,39,12),(332,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,41,12),(333,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,42,12),(334,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,43,12),(335,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,44,12),(336,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,45,12),(337,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,46,12),(338,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',1,47,12),(339,'The contest has just started, you can join until Dec 8th, 14:02','open','2020-11-29 14:02:37',0,48,12),(341,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,3,13),(342,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,19,13),(343,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,20,13),(344,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,21,13),(345,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,22,13),(346,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,23,13),(347,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,24,13),(348,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,25,13),(349,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,26,13),(350,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,27,13),(351,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,28,13),(352,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,29,13),(353,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,30,13),(354,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,31,13),(355,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,32,13),(356,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,33,13),(357,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,34,13),(358,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,35,13),(359,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,36,13),(360,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,37,13),(361,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,38,13),(362,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,39,13),(363,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,41,13),(364,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,42,13),(365,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,43,13),(366,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,44,13),(367,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,45,13),(368,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,46,13),(369,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',1,47,13),(370,'The contest has just started, you can join until Dec 6th, 14:07','open','2020-11-29 14:07:13',0,48,13),(372,'The contest has just started, you can join until Dec 6th, 14:12','open','2020-11-29 14:12:52',1,3,14),(373,'The contest has just started, you can join until Dec 6th, 14:12','open','2020-11-29 14:12:52',1,21,14),(374,'The contest has just started, you can join until Dec 6th, 14:12','open','2020-11-29 14:12:52',0,22,14),(375,'The contest has just started, you can join until Dec 6th, 14:12','open','2020-11-29 14:12:52',1,28,14),(376,'The contest has just started, you can join until Dec 6th, 14:12','open','2020-11-29 14:12:52',0,31,14),(377,'The contest has just started, you can join until Dec 6th, 14:12','open','2020-11-29 14:12:52',1,32,14),(378,'The contest has just started, you can join until Dec 6th, 14:12','open','2020-11-29 14:12:52',0,38,14),(379,'The contest has just started, you can join until Dec 6th, 14:12','open','2020-11-29 14:12:52',0,39,14),(380,'The contest has just started, you can join until Dec 6th, 14:12','open','2020-11-29 14:12:52',0,44,14),(387,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,3,15),(388,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,19,15),(389,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,20,15),(390,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,21,15),(391,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,22,15),(392,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,23,15),(393,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,24,15),(394,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,25,15),(395,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,26,15),(396,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,27,15),(397,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,28,15),(398,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,29,15),(399,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,30,15),(400,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,31,15),(401,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,32,15),(402,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,33,15),(403,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,34,15),(404,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,35,15),(405,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,36,15),(406,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,37,15),(407,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,38,15),(408,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,39,15),(409,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,41,15),(410,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,42,15),(411,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,43,15),(412,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,44,15),(413,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,45,15),(414,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,46,15),(415,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',1,47,15),(416,'The contest has just started, you can join until Dec 8th, 14:12','open','2020-11-29 14:12:55',0,48,15),(418,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',1,3,16),(419,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',1,19,16),(420,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',1,20,16),(421,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',1,21,16),(422,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',1,23,16),(423,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',1,25,16),(424,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',0,26,16),(425,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',0,27,16),(426,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',0,30,16),(427,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',1,32,16),(428,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',0,34,16),(429,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',1,35,16),(430,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',0,37,16),(431,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',0,39,16),(432,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',1,41,16),(433,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',0,42,16),(434,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',0,44,16),(435,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',0,45,16),(436,'The contest has just started, you can join until Dec 6th, 14:15','open','2020-11-29 14:15:13',1,47,16),(449,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,3,17),(450,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,19,17),(451,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,20,17),(452,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,21,17),(453,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,22,17),(454,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,23,17),(455,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,24,17),(456,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,25,17),(457,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,26,17),(458,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,27,17),(459,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,28,17),(460,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,29,17),(461,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,30,17),(462,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,31,17),(463,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,32,17),(464,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,33,17),(465,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,34,17),(466,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,35,17),(467,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,36,17),(468,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,37,17),(469,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,38,17),(470,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,39,17),(471,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,41,17),(472,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,42,17),(473,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,43,17),(474,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,44,17),(475,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,45,17),(476,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,46,17),(477,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',1,47,17),(478,'The contest has just started, you can join until Dec 9th, 14:17','open','2020-11-29 14:17:57',0,48,17),(480,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',1,3,18),(481,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',1,19,18),(482,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',1,21,18),(483,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',1,23,18),(484,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',1,25,18),(485,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,27,18),(486,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',1,28,18),(487,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,29,18),(488,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,30,18),(489,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,31,18),(490,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',1,32,18),(491,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,33,18),(492,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',1,35,18),(493,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,36,18),(494,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,37,18),(495,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,38,18),(496,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,39,18),(497,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',1,41,18),(498,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,42,18),(499,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,45,18),(500,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',1,46,18),(501,'The contest has just started, you can join until Dec 10th, 14:19','open','2020-11-29 14:19:42',0,48,18),(511,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,3,19),(512,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,19,19),(513,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,20,19),(514,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,21,19),(515,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,22,19),(516,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,23,19),(517,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,24,19),(518,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,25,19),(519,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,26,19),(520,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,27,19),(521,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,28,19),(522,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,29,19),(523,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,30,19),(524,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,31,19),(525,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,32,19),(526,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,33,19),(527,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,34,19),(528,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,35,19),(529,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,36,19),(530,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,37,19),(531,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,38,19),(532,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,39,19),(533,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,41,19),(534,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,42,19),(535,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,43,19),(536,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,44,19),(537,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,45,19),(538,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,46,19),(539,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',1,47,19),(540,'The contest has just started, you can join until Dec 1st, 14:21','open','2020-11-29 14:21:09',0,48,19),(542,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,3,20),(543,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,19,20),(544,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,20,20),(545,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,21,20),(546,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,22,20),(547,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,23,20),(548,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,24,20),(549,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,25,20),(550,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,26,20),(551,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,27,20),(552,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,28,20),(553,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,29,20),(554,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,30,20),(555,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,31,20),(556,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,32,20),(557,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,33,20),(558,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,34,20),(559,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,35,20),(560,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,36,20),(561,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,37,20),(562,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,38,20),(563,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,39,20),(564,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,41,20),(565,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,42,20),(566,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,43,20),(567,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,44,20),(568,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,45,20),(569,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,46,20),(570,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',1,47,20),(571,'The contest has just started, you can join until Dec 9th, 14:23','open','2020-11-29 14:23:03',0,48,20),(573,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,3,21),(574,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,19,21),(575,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,20,21),(576,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,21,21),(577,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,22,21),(578,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,23,21),(579,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,24,21),(580,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,25,21),(581,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,26,21),(582,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,27,21),(583,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,28,21),(584,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,29,21),(585,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,30,21),(586,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,31,21),(587,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,32,21),(588,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,33,21),(589,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,34,21),(590,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,35,21),(591,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,36,21),(592,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,37,21),(593,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,38,21),(594,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,39,21),(595,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,41,21),(596,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,42,21),(597,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,43,21),(598,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,44,21),(599,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,45,21),(600,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,46,21),(601,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',1,47,21),(602,'The contest has just started, you can join until Dec 3rd, 14:23','open','2020-11-29 14:23:58',0,48,21),(604,'The contest is now in grading phase till Sunday, 18:00','grade','2020-11-29 17:31:29',1,1,11),(605,'The contest is now in grading phase till Sunday, 18:00','grade','2020-11-29 17:31:29',1,2,11),(606,'The contest is now in grading phase till Sunday, 18:00','grade','2020-11-29 17:31:29',1,4,11),(607,'You have finished at 6th place with an average score of 6.67','scored','2020-11-29 17:51:42',1,3,11),(608,'You have finished at 2nd place with an average score of 8.33','scored','2020-11-29 17:51:42',1,21,11),(609,'You have finished at 8th place with an average score of 4.67','scored','2020-11-29 17:51:42',0,23,11),(610,'You have finished at 10th place with an average score of 0.67','scored','2020-11-29 17:51:42',1,28,11),(611,'You have finished at 3rd place with an average score of 8','scored','2020-11-29 17:51:42',0,31,11),(612,'You have finished at 1st place with an average score of 9.33','scored','2020-11-29 17:51:42',1,41,11),(613,'You have finished at 9th place with an average score of 1.33','scored','2020-11-29 17:51:42',0,42,11),(614,'You have finished at 4th place with an average score of 7.67','scored','2020-11-29 17:51:42',0,44,11),(615,'You have finished at 5th place with an average score of 7','scored','2020-11-29 17:51:42',1,46,11),(616,'You have finished at 7th place with an average score of 5','scored','2020-11-29 17:51:42',1,19,11),(622,'The contest is now in grading phase till Sunday, 20:40','grade','2020-11-29 19:41:29',1,1,2),(623,'The contest is now in grading phase till Sunday, 20:40','grade','2020-11-29 19:41:29',1,2,2),(624,'The contest is now in grading phase till Sunday, 20:40','grade','2020-11-29 19:41:29',1,4,2),(625,'The contest is now in grading phase till Sunday, 20:42','grade','2020-11-29 19:46:29',1,1,3),(626,'The contest is now in grading phase till Sunday, 20:42','grade','2020-11-29 19:46:29',1,2,3),(627,'The contest is now in grading phase till Sunday, 20:42','grade','2020-11-29 19:46:29',1,4,3),(628,'You have finished at 8th place with an average score of 5.33','scored','2020-11-29 20:16:42',0,3,2),(629,'You have finished at 5th place with an average score of 6.33','scored','2020-11-29 20:16:42',1,21,2),(630,'You have finished at 11th place with an average score of 4.33','scored','2020-11-29 20:16:42',0,22,2),(631,'You have finished at 3rd place with an average score of 7.33','scored','2020-11-29 20:16:42',0,23,2),(632,'You have finished at 2nd place with an average score of 7.67','scored','2020-11-29 20:16:42',0,25,2),(633,'You have finished at 13th place with an average score of 3','scored','2020-11-29 20:16:42',1,28,2),(634,'You have finished at 7th place with an average score of 5.67','scored','2020-11-29 20:16:42',0,29,2),(635,'You have finished at 6th place with an average score of 6','scored','2020-11-29 20:16:42',0,32,2),(636,'You have finished at 15th place with an average score of 0','scored','2020-11-29 20:16:42',0,34,2),(637,'You have finished at 5th place with an average score of 6.33','scored','2020-11-29 20:16:42',1,35,2),(638,'You have finished at 10th place with an average score of 4.67','scored','2020-11-29 20:16:42',0,36,2),(639,'You have finished at 9th place with an average score of 5','scored','2020-11-29 20:16:42',0,39,2),(640,'You have finished at 1st place with an average score of 10','scored','2020-11-29 20:16:42',1,41,2),(641,'You have finished at 14th place with an average score of 1','scored','2020-11-29 20:16:42',0,43,2),(642,'You have finished at 11th place with an average score of 4.33','scored','2020-11-29 20:16:42',0,45,2),(643,'You have finished at 4th place with an average score of 6.67','scored','2020-11-29 20:16:42',0,46,2),(644,'You have finished at 12th place with an average score of 4','scored','2020-11-29 20:16:42',1,47,2),(659,'You have finished at 7th place with an average score of 4.33','scored','2020-11-29 20:21:42',1,20,3),(660,'You have finished at 5th place with an average score of 5.33','scored','2020-11-29 20:21:42',1,21,3),(661,'You have finished at 4th place with an average score of 6.67','scored','2020-11-29 20:21:42',0,26,3),(662,'You have finished at 3rd place with an average score of 7','scored','2020-11-29 20:21:42',0,27,3),(663,'You have finished at 2nd place with an average score of 7.67','scored','2020-11-29 20:21:42',0,30,3),(664,'You have finished at 8th place with an average score of 2.33','scored','2020-11-29 20:21:42',0,31,3),(665,'You have finished at 6th place with an average score of 4.67','scored','2020-11-29 20:21:42',0,42,3),(666,'You have finished at 5th place with an average score of 5.33','scored','2020-11-29 20:21:42',0,48,3),(667,'You have finished at 1st place with an average score of 10','scored','2020-11-29 20:21:42',1,41,3),(674,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,3,22),(675,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',1,19,22),(676,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',1,20,22),(677,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',1,21,22),(678,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,22,22),(679,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',1,23,22),(680,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,24,22),(681,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,25,22),(682,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,26,22),(683,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,27,22),(684,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',1,28,22),(685,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,29,22),(686,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,30,22),(687,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,31,22),(688,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,32,22),(689,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,33,22),(690,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,34,22),(691,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',1,35,22),(692,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,36,22),(693,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,37,22),(694,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,38,22),(695,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,39,22),(696,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,42,22),(697,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,43,22),(698,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,44,22),(699,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,45,22),(700,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,46,22),(701,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',1,47,22),(702,'The contest has just started, you can join until Dec 1st, 10:56','open','2020-11-30 10:56:37',0,48,22),(705,'The contest is now in grading phase till Tuesday, 12:56','grade','2020-11-30 12:06:29',1,1,22),(706,'The contest is now in grading phase till Tuesday, 12:56','grade','2020-11-30 12:06:29',1,2,22),(707,'The contest is now in grading phase till Tuesday, 12:56','grade','2020-11-30 12:06:29',1,4,22),(708,'The contest is now in grading phase till Tuesday, 12:56','grade','2020-11-30 12:06:29',1,41,22),(712,'You have finished at 1st place with an average score of 9.25','scored','2020-11-30 14:56:42',1,20,22),(713,'You have finished at 6th place with an average score of 5.75','scored','2020-11-30 14:56:42',0,23,22),(714,'You have finished at 12th place with an average score of 2','scored','2020-11-30 14:56:42',0,35,22),(715,'You have finished at 7th place with an average score of 4.75','scored','2020-11-30 14:56:42',0,37,22),(716,'You have finished at 4th place with an average score of 6.75','scored','2020-11-30 14:56:42',0,39,22),(717,'You have finished at 3rd place with an average score of 7','scored','2020-11-30 14:56:42',0,43,22),(718,'You have finished at 2nd place with an average score of 8.5','scored','2020-11-30 14:56:42',1,47,22),(719,'You have finished at 4th place with an average score of 6.75','scored','2020-11-30 14:56:42',0,19,22),(720,'You have finished at 8th place with an average score of 4.5','scored','2020-11-30 14:56:42',0,22,22),(721,'You have finished at 9th place with an average score of 4.25','scored','2020-11-30 14:56:42',0,24,22),(722,'You have finished at 11th place with an average score of 2.5','scored','2020-11-30 14:56:42',0,25,22),(723,'You have finished at 10th place with an average score of 3','scored','2020-11-30 14:56:42',0,27,22),(724,'You have finished at 5th place with an average score of 6.25','scored','2020-11-30 14:56:42',0,29,22),(727,'Contest has finished.','scored','2020-11-30 19:48:23',1,1,22),(728,'Contest has finished.','scored','2020-11-30 19:48:23',1,2,22),(729,'Contest has finished.','scored','2020-11-30 19:48:23',1,4,22),(730,'Contest has finished.','scored','2020-11-30 19:48:23',1,41,22),(781,'The contest is now in grading phase till Thursday, 19:12','grade','2020-12-03 08:36:29',0,1,14),(782,'The contest is now in grading phase till Thursday, 19:12','grade','2020-12-02 19:12:29',0,2,14),(783,'The contest is now in grading phase till Thursday, 19:12','grade','2020-12-03 08:36:29',1,4,14),(784,'The contest is now in grading phase till Thursday, 20:19','grade','2020-12-03 08:36:29',0,1,18),(785,'The contest is now in grading phase till Thursday, 20:19','grade','2020-12-02 20:19:29',0,2,18),(786,'The contest is now in grading phase till Thursday, 20:19','grade','2020-12-03 08:36:29',1,4,18),(787,'The contest is now in grading phase till Sunday, 11:06','grade','2020-12-03 09:11:29',0,1,16),(788,'The contest is now in grading phase till Sunday, 11:06','grade','2020-12-03 09:11:29',0,2,16),(789,'The contest is now in grading phase till Sunday, 11:06','grade','2020-12-03 09:11:29',1,4,16);
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participations`
--

DROP TABLE IF EXISTS `participations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `participations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `story` text DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `score` float DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `contest_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_participations_contests1_idx` (`contest_id`),
  KEY `fk_participations_users1_idx` (`user_id`),
  CONSTRAINT `fk_participations_contests1` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_participations_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participations`
--

LOCK TABLES `participations` WRITE;
/*!40000 ALTER TABLE `participations` DISABLE KEYS */;
INSERT INTO `participations` VALUES (1,'Little Chopper','Search and Destroy. Roger, roger.','1606670586101_EKpJSMBWsAE8_PS.jpg',5.33,8,'2020-11-29 13:40:56',2,3),(2,'A little road not made of man','A little road not made of man,\nEnabled of the eye,\nAccessible to thill of bee,\nOr cart of butterfly.','1606659298747.jpeg',6.33,5,'2020-11-29 13:40:56',2,21),(3,'Little friend','These tiny loiterers on the barley\'s beard,\nAnd happy units of a numerous herd\nOf playfellows, the laughing Summer brings,\nMocking the sunshine on their glittering wings,\nHow merrily they creep, and run, and fly!\nNo kin they bear to labour\'s drudgery,\nSmoothing the velvet of the pale hedge-rose;','1606659338546_IMG_5341_sm.jpg',4.33,11,'2020-11-29 13:40:56',2,22),(4,'The fifth Beatle','The missing member of the popular group.','1606661157166_cover_1.jpg',7.33,3,'2020-11-29 13:40:56',2,23),(5,'The Fly\'s Revenge','\"So,\" said a fly, as he paused and thought\nHow he had just been brushed about,\n\"They think, no doubt, I am next to nought,\nPut into life but to be put out!','1606660222197_photography-ethics-freezing-insects-macro-4-900x600.jpg',7.67,2,'2020-11-29 13:40:56',2,25),(6,'Not a Hummingbird','This is an insect actually.','1606662378562_гълъбова-опашка.jpg',3,13,'2020-11-29 13:40:56',2,28),(7,'Working bee','Working bee in the middle of a hot day','1606661687620_How-to-Photograph-Insects-in-Flight-5-800x534.jpg',5.67,7,'2020-11-29 13:40:56',2,29),(8,'Anthena Bug','I\'ve got 50 channels. Wanna wach discovery? It\'s the 25th one.','1606670741608_unnamed.jpg',6,6,'2020-11-29 13:40:56',2,32),(9,'Last thing the cricket saw before eaten.','Last thing the cricket saw before eaten by a praying mantis of death!','1606670809364_insects-macro-photography-donald-jusa-3.jpg',0,15,'2020-11-29 13:40:56',2,34),(10,'Ladies share latest gosip.','\"Omg - Queen is totaly pregnant - she says she is not but then wears a sweather in may.\"','1606671102416_ladybugs-ladybirds-bugs-insects-144243.jpeg',6.33,5,'2020-11-29 13:40:56',2,35),(11,'Mine!','It\'s mine, mine, mine. I\'m a greedy bee and this flower bud is all mine! I\'m so rich!','1606671263543_pexels-photo-2047420.jpeg',4.67,10,'2020-11-29 13:40:56',2,36),(12,'Crawling to victory','A centipede crawling to its next challenge.','1606670749139_john-hallmen-insect-photography-9.jpg',5,9,'2020-11-29 13:40:56',2,39),(13,'Fizz Buzz','Fizz Buzz Fizz Buzzzzz','1606658530704_insect-photo.png',10,1,'2020-11-29 13:40:56',2,41),(14,'Hi there','How are you doing today?','1606670781741_2006_BHC09.jpg',1,14,'2020-11-29 13:40:56',2,43),(15,'Beauty','I found this gorgeous one on a hike last Sunday. ','1606670865149_news_preview_mob_image__preview_1320.jpg',4.33,11,'2020-11-29 13:40:56',2,45),(16,'On the top','It\'s sweet getting to the top but there is only one way forward from here.','1606670922410_photo-1526773357673-2d4e8116d497.jpg',6.67,4,'2020-11-29 13:40:56',2,46),(17,'Doing the job','A little bee worker doing it\'s job on a random day.','1606670969246_insect-photography.jpg',4,12,'2020-11-29 13:40:56',2,47),(32,'Liquid Shroom','A mushroom of colorful water.','1606659527905_005056c0000112467cd322.jpeg',4.33,7,'2020-11-29 13:42:54',3,20),(33,'The tide in the river','The tide in the river,\nThe tide in the river,\nThe tide in the river runs deep.\nI saw a shiver\nPass over the river\nAs the tide turned in its sleep.','1606658027156_nguy-n-kim-thi-n-adobestock-258981263-abstract-colorful-liquid-water-splash-and-bubbles-background-macro-photography.jpg',5.33,5,'2020-11-29 13:42:54',3,21),(34,'Bubbles','All the colors captured by these little bubbling beauty','1606671311813_6423870a7707da61df04e0c2c7526a2e.jpg',6.67,4,'2020-11-29 13:42:54',3,26),(35,'The Brook','I come from haunts of coot and hern,\nI make a sudden sally\nAnd sparkle out among the fern,\nTo bicker down a valley.','1606660832152_I-take-Abstract-Macro-pictures-using-Oil-and-Water-57e510c627f36__880.jpg',7,3,'2020-11-29 13:42:54',3,27),(36,'Water is','Water is pure,\nWater is natural,\nWater is healthy,\nWater can help all\n\nWater is simple,\nWater is free,\nWater can help the lives,\nThe lives of you and me','1606661824952_oil-and-water-photography-5-1.jpg',7.67,2,'2020-11-29 13:42:54',3,30),(37,'Sparkling water','Sparkling water brings life','1606662065605_oil-abstract-photography-06.jpg',2.33,8,'2020-11-29 13:42:54',3,31),(38,'Over or under','Are you over the surface? Or are you under? You never know...','1606671364677_70eb8a575bd6570b7b7e5dc30f5a1052.jpg',4.67,6,'2020-11-29 13:42:54',3,42),(39,'Aquarium','Aquarium water colors','1606671403997_Aqurium-water-color.jpg',5.33,5,'2020-11-29 13:42:54',3,48),(47,'Horses Playground','The eldest son bestrides him,\nAnd the pretty daughter rides him,\nAnd I meet him oft o’ mornings on the Course;\nAnd there kindles in my bosom\nAn emotion chill and gruesome\nAs I canter past the Undertaker’s Horse …','1606663235335_pexels-photo-4268141.jpeg',6.67,6,'2020-11-29 14:00:10',11,3),(49,'A Mountain Gateway','I Know a vale where I would go one day,\nWhen June comes back and all the world once more\nIs glad with summer. Deep in shade it lies\nA mighty cleft between the bosoming hills,\nA cool dim gateway to the mountains\' heart.','1606658702585_555355.jpg',8.33,2,'2020-11-29 14:00:10',11,21),(50,'Yellow dots','Where horses love to roam.','1606660633175_IMG-8258.jpg',4.67,8,'2020-11-29 14:00:10',11,23),(51,'Brown summer skies','Somewhere in Canada.','1606661984848_pexels-photo-3029762.jpeg',0.67,10,'2020-11-29 14:00:10',11,28),(52,'Rila Lakes','Rila Lakes on a beautiful day','1606662269486_seven-rila-lakes.jpg',8,3,'2020-11-29 14:00:10',11,31),(53,'Escape Mountains','Escape here and never return.','1606657401907_wp4427970.jpg',9.33,1,'2020-11-29 14:00:10',11,41),(54,'Foggy Summer Mountain','The fog is subtle an yet so thick.','1606663440671_pexels-photo-5029303.jpeg',1.33,9,'2020-11-29 14:00:10',11,42),(55,'Colorado summer','Sunrise over the Engineer mountain in Colorado','1606663409878_20190728-Sunrise-on-Engineer-Mountain.jpg',7.67,4,'2020-11-29 14:00:10',11,44),(56,'Countryside','Countryside view of the valley','1606663157057_bigstock-Countryside-view-of-the-valley-87681455-862x575.jpg',7,5,'2020-11-29 14:00:10',11,46),(62,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:12:52',14,3),(63,'Break, Break, Break','Break, break, break,\nOn thy cold gray stones, O sea!\nAnd I would that my tongue could utter\nThe thoughts that arise in me.','1606658276909_97380f786352291c74c37c8c37e332a5.jpg',NULL,NULL,'2020-11-29 14:12:52',14,21),(64,'Dance with the waves','Dance with the waves,\nmove with the sea.\nLet the rhytm of the water\nset your soul free.','1606659275099_ocean-waves-water-light-warren-keelan-fb.jpg',NULL,NULL,'2020-11-29 14:12:52',14,22),(65,'Great wave','Great Wave of Kanigawa photo recreation.','1606662562948_da59eab0bfe7ffec18639456f7f9c911.jpg',NULL,NULL,'2020-11-29 14:12:52',14,28),(66,'Ocean Mightier Than the Land','Ocean, mightier than the land,\nWilful, turbulent, and wild,\nWill you love a little child\nAnd kiss her hand?','1606662131807_wave-photography-maelstrom-luke-shadbolt-fb.png',NULL,NULL,'2020-11-29 14:12:52',14,31),(67,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:12:52',14,32),(68,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:12:52',14,38),(69,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:12:52',14,39),(70,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:12:52',14,44),(77,'Blooming red flower','I took this shot long ago. Biggest flower I\'ve ever seen.','1606977647625.jpeg',NULL,NULL,'2020-11-29 14:15:13',16,3),(78,'The First Dandelion','Simple and fresh and fair from winter\'s close emerging,\nAs if no artifice of fashion, business, politics, had ever been,\nForth from its sunny nook of shelter\'d grass—innocent, golden, calm as the dawn,\nThe spring\'s first dandelion shows its trustful face.','1606662935120_blueflowers.jpeg',NULL,NULL,'2020-11-29 14:15:13',16,19),(79,'Grow up','A Flower journey.','1606659701200_spring.jpg',NULL,NULL,'2020-11-29 14:15:13',16,20),(80,'Beauty in nature','The flower is\nthe stem’s cry of beauty\nto the universe.','1606658344325.jpg',NULL,NULL,'2020-11-29 14:15:13',16,21),(81,'Pink fire','The flikering flames of the pink flower.','1606661292392_photo-1519073213287-1dda8e41e6cf.jpg',NULL,NULL,'2020-11-29 14:15:13',16,23),(82,'The Soul of the Sunflower','The warm sun kissed the earth\nTo consecrate thy birth,\nAnd from his close embrace\nThy radiant face\nSprang into sight,\nA blossoming delight.','1606660116626_flower-photography.jpg',NULL,NULL,'2020-11-29 14:15:13',16,25),(83,'Blond flowers','Some of them are natural.','1606978029247.jpg',NULL,NULL,'2020-11-29 14:15:13',16,26),(84,'I\'m a Pirate','I\'m a pirate in the grass—\nHear ye people as ye pass;\nI\'m a pirate bad and bold,\nTaking dandelion gold—\nAll my hands and ships can hold.','1606660906373_Photo-by-Paul-Talbot1.jpg',NULL,NULL,'2020-11-29 14:15:13',16,27),(85,'Little Dandelion','Happy little Dandelion\nLights up the meads,\nSwings on her slender foot,\nTelleth her beads,\nLists to the robin\'s note\nPoured from above;\nWise little Dandelion\nAsks not for love.','1606661888660_dandelion-stephen-jenkins.jpg',NULL,NULL,'2020-11-29 14:15:13',16,30),(86,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:15:13',16,32),(87,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:15:13',16,34),(88,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:15:13',16,35),(89,'Flower snail','Flowers crawing from their shell.','1606977834313.jpg',NULL,NULL,'2020-11-29 14:15:13',16,37),(90,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:15:13',16,39),(91,'Donkey Thorn Blooming','Beautiful flower with an ugly name.','1606654614247_maxresdefault.jpg',NULL,NULL,'2020-11-29 14:15:13',16,41),(92,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:15:13',16,42),(93,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:15:13',16,44),(94,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:15:13',16,45),(95,'Willpower','Never give up - no matter the odds.','1606656400023_closeup-of-dandelion-2.jpg',NULL,NULL,'2020-11-29 14:15:12',16,47),(108,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,3),(109,'The Sea of Sunset','This is the land the sunset washes,\nThese are the banks of the Yellow Sea;\nWhere it rose, or whither it rushes,\nThese are the western mystery!','1606656531885_wp-2016-03-golden-hour-featured.jpg',NULL,NULL,'2020-11-29 14:19:42',18,19),(110,'The Juggler of Day','The sun set late; and left along the west\nA belt of furious ruby, o\'er which snows\nOf clouds unrolled; each cloud a mighty breast\nBlooming with almond-rose.','1606658619222_9773da3751e235f5dec4961cba6f7d99.jpg',NULL,NULL,'2020-11-29 14:19:42',18,21),(111,'Reaching light','The golden lights reach through the forrest.','1606661521935_reach.jpg',NULL,NULL,'2020-11-29 14:19:42',18,23),(112,'A Walk at Sunset','When insect wings are glistening in the beam\nOf the low sun, and mountain-tops are bright,\nOh, let me, by the crystal valley-stream,\nWander amid the mild and mellow light;\nAnd while the redbreast pipes his evening lay,\nGive me one lonely hour to hymn the setting day.','1606660284750_beautiful-cropland-dawn-1237119-900x599.jpg',NULL,NULL,'2020-11-29 14:19:42',18,25),(113,'Oh, sunset','Oh sunset, you fade, silent as always,\nA trickle of fear touches my heart,\nA sliver of doubt is all that I need,\nTo tear this beauty apart.','1606660995769_seven-rila-lakes-how-to-get-to-1200x675.jpeg',NULL,NULL,'2020-11-29 14:19:42',18,27),(114,'Golden Wheat','Bread or beer - both good for you.','1606662114196_pexels-photo-1600139.jpeg',NULL,NULL,'2020-11-29 14:19:42',18,28),(115,'Sunset down the river','A man strolling down the river at the end of the day.','1606661585857.jpg',NULL,NULL,'2020-11-29 14:19:42',18,29),(116,'Sunset over the ocean','Oh sunset, you smile, wink, just play,\nDeciding it\'s time to make haste,\nSo gone is the promise of comfort and love,\nAll hopes, all dreams; a silly waste.','1606662003433_pexels-photo-189349.jpeg',NULL,NULL,'2020-11-29 14:19:42',18,30),(117,'Sunset through the wave','Sunset through the wave at the end of a long day','1606662232109_pexels-photo-1210273.jpeg',NULL,NULL,'2020-11-29 14:19:42',18,31),(118,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,32),(119,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,33),(120,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,35),(121,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,36),(122,'Bottle without a message','A message in a bottle\nThrown out to sea\nHoping my soulmate will find it\nAnd come searching for me\n\nA message in a bottle\nFilled with many hopes and desires\nPraying that one day\nThey all will transpire\n\nI walk the beaches at night\nMy mind wandering aimlessly\nWondering if the one I love\nWill sweep me off my feet gracefully\n\nAll alone knowing that\nWords will never be spoken\nBecause our love is separated\nBy a tremendous ocean\n\nA message in a bottle\nLeft this very shore\nTossed out to sea\nSo that love will be lost no more\n\nThoughts of you\nHow can it be?\nWhen I have no picture of you\nPainted in my mind visually\n\nWill he think I’m crazy\nOr just crazy in love\nOr maybe he’ll think I’m heaven-sent\nLike the angel Gabriel from above\n\nToo many days have gone by\nI long to feel your embrace\nWrap my arms around you\nAnd forever share the same space\n\nWithout you\nI feel an emptiness in my soul\nMe plus you\nWill make this broken heart whole\n\nThis letter has traveled the raging waves\nTossing and turning\nGod will bring us together\nBecause your love is what I’ve been yearning\n\nI dropped a tear in the ocean\nAnd this here is true\nThe day I find it\nWill be the day I stop missing you\n\nA message in a bottle\nThrown out to sea\nHoping my soulmate will find it\nAnd come searching for me','1606662836780_pexels-photo-462030.jpeg',NULL,NULL,'2020-11-29 14:19:42',18,37),(123,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,38),(124,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,39),(125,'The Sunset, Woven of Soft Lights','The sunset, woven of soft lights\nAnd tender colors, lingers late,\nAs looking back on all day\'s dreary plights,\nCompassionate;','1606655484070_1200px-Sunset_2007-1.jpg',NULL,NULL,'2020-11-29 14:19:42',18,41),(126,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,42),(127,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,45),(128,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,46),(129,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:19:42',18,48),(139,NULL,NULL,NULL,NULL,NULL,'2020-11-29 14:28:03',21,3),(140,'Pink river','japan is a very nice place','1606932258028.jpg',NULL,NULL,'2020-11-29 14:33:11',21,41),(141,'Halkata','Lokated in Sliven, Bulgaria. I was impressed by it. Halkata means \"Ring\" in Bulgarian speak. I have no idea why they call it like that. ','1606654441118_1dd7a4d178fd8e9e7f618c14eba9d823.jpg',NULL,NULL,'2020-11-29 14:47:13',19,41),(142,'Narcissus lake','Narcissus was excessively proud of his own handsomeness, rejecting others\' advances because he thought only someone as beautiful as himself should pursue him, causing some to take their own lives to prove their devotion to his striking beauty. In most versions of his legend, he did finally meet someone he thought was good enough for him – himself, by way of his reflection in a pool of water. Usually, he is said to have wasted away longing to be with his mirror self, but in other stories he kills himself upon realizing he cannot have his own reflection as a lover. Either way, he dies, and in his place sprouts a flower bearing his name.','1606655185845.png',NULL,NULL,'2020-11-29 14:58:26',20,41),(143,'Silently a flower blooms','Silently a flower blooms,\nIn silence it falls away;\nYet here now, at this moment, at this place,\nThe world of the flower, the whole of\nthe world is blooming.\nThis is the talk of the flower, the truth\nof the blossom:\nThe glory of eternal life is fully shining here.','1606655859206_0f341787de9b705b3981decfbe2721c9.jpg',NULL,NULL,'2020-11-29 15:11:39',17,41),(144,'North Lake','The first afternoon I can recall,\nyou grabbed my hand\nand took me outside.\nYou surprised me, I said.\nBecause that noon\nis the first time\nI saw that lake.\n\nThe second afternoon I can recall,\nyou called me by name\nand we went outside.\nI brought you lunch, and\nwe drank some\nmind-boggling liquid\nwhich you stole from that old man\nliving beside that lake.\nWe lied on the grass, and\nif that was not a dream, I hope not,\nI felt your breath with mine, and your lips\non mine.\n\nThe third afternoon I can recall,\nyou went to my bed\nand shook me awake.\nI was mesmerized to see you again,\nbut you’ve changed.\nThe colour in your eyelids, your cheeks,\nand your lips was artificial.\nIf you haven’t spoken, I\nwouldn’t be able to recognize you.\nSitting at the edge of my bed,\nyou’ve said the name of that lake,\nand I knew  it was you still.\n\nThe fourth afternoon I can recall,\nyou were 18 and still cried on my shoulder\nnot because you were hurt, but\nbecause you were happy  getting married.\nFlowers, chairs, and a priest\nwaited  for you beside that lake.\nI was about to cry at that moment, knowing\nit wasn’t me you were marrying.\n\nThe fifth afternoon I can recall,\nyou yelled at me,\n“I can’t live this way!”\nI asked you why, but\nyou didn’t tell me, you showed me.\nThat kiss beside that lake was wrong.\nIn all of the reasons why it was wrong,\nI found one which is right.\nYou loved me the way I loved you.\n\nThe sixth afternoon I can recall,\nyou left me\nalone beside that lake.\nYes, you loved me, but\nas you have said you need to love yourself more.\nI can’t hold you any blame for leaving,\nI understood, and I lived with the promise\nthat you’ll come back to me –\nin one piece or even in ashes.\n\nThis afternoon,\nI carry you, with all but  my shattered heart,\ninside a jar.\nMy tears are one with that lake,\nbut I’ll bury you beside it.\nI know you’re happy.\nYour soul one with that lake.','1606656249304_8249102491a48f0983caabacb9913f33.jpg',NULL,NULL,'2020-11-29 15:19:04',15,41),(145,'Shutdown','Our sun maintains a wondrous cycle;\nfrom dawn to dusk, day to night,\nbirth to death, always moving.\n\nAt sundown….','1606656428220_sunset-over-sea-borchee.jpg',NULL,NULL,'2020-11-29 15:25:08',13,41),(146,'CCTV Headquarters','The CCTV Headquarters serves as the headquarters for China Central Television (CCTV) that was formerly at the old China Central Television Building located at 11 Fuxin Road some 15 km (9.3 mi) to the west. The tower is a 234-metre (768 ft), 51-story skyscraper on East Third Ring Road, Guanghua Road in the Beijing Central Business District (CBD). Groundbreaking took place on 1 June 2004 and the building\'s facade was completed in January 2008. After the construction was delayed by a fire that engulfed the adjacent Television Cultural Center in February 2009, the headquarters was completed in May 2012[5] and was officially inaugurated in June 2013. The CCTV Headquarters won the 2013 Best Tall Building Worldwide from the Council on Tall Buildings and Urban Habitat.','1606656651526_dam-images-daily-2015-06-50-modern-buildings-modern-architecture-design-museum-01.jpg',NULL,NULL,'2020-11-29 15:28:17',12,41),(147,'Alpine Song','With alpenstock and knapsack light','1606657210935_summer-1606656740477-2076.jpg',5,7,'2020-11-29 15:28:17',11,19),(148,'The cherry blossoms','The cherry blossoms:\nBeing ill, how many things\nI remember about them\n\nScatter layer\nby layer, eight-layered\ncherry blossoms\n\nMoon at twilight\na cluster of petals falling\nfrom the cherry tree\n\ncherry blossom petals\nblown by the spring breeze against\nthe undried wall','1606657668977_1521217609.jpg',NULL,NULL,'2020-11-29 15:45:19',21,19),(149,'Candy light','Get free shipping on qualified Christmas Pathway Lights or Buy Online Pick Up in Store ... Tall Candy Cane Pathway with Red and White LED Lights.','1606657635548_christmas-18106_1280-1024x769.jpg',NULL,NULL,'2020-11-29 15:45:24',10,41),(150,'Morning touch','Times even in the grip of  trouble\nget no less a sunrise than sun is capable\nthe capable beauty all we have\nto expect—     to ask more from some incompetent   laughs\nat the proposition we have trumped all that\nfrom...','1606657770727_Golden_Morning_VIC_CGF_CCP.jpg',NULL,NULL,'2020-11-29 15:48:25',9,41),(151,'Mirror, mirror','The glass I got in Venice\nis a mirror\nis the iris in your eye\nis the color of bruises.','1606657824388_81492-0jyszde3pn-bvzwib.jpg',NULL,NULL,'2020-11-29 15:48:31',20,19),(152,'This Chocolate Strawberry Roll','Ingredientsprint\nMakes about 12 servings\nFor Flower Pattern\n1 small egg\n2 tbsp (30g) sugar\n? cup+ 1 tbsp (50g) all-purpose flour\n1 tbsp (14g) butter, melted\nred food coloring\nChocolate Sponge Cake\n4 eggs\n1/2 cup (100g) sugar\n2 tbsp (28g) vegetable oil\n1 tsp (5g) vanilla extract\n1/3 cup+ 1 tbsp (50g) all-purpose flour\n1/3 cup (40g) dark unsweetened cocoa powder\n1 tsp (4g) baking powder\n1/2 tsp (2g) salt\nFilling\n5.5 oz (150g) white chocolate\n3 tbsp (45g) whipping cream\n1 cup (240g) whipping cream, chilled\n9 oz (250g) fresh strawberries, finely diced','1606657921658_chocolate_strawberry_roll_3.jpg',NULL,NULL,'2020-11-29 15:51:15',8,41),(153,'Lifting the Trophy','Liverpool defeated Tottenham 2-0 to capture the 2019 UEFA Champions League trophy.','1606658142954_GettyImages-1153214330-1024x683.jpg',NULL,NULL,'2020-11-29 15:52:23',7,41),(154,'We are not alone','I said you wanna be startin\' somethin\'\nYou got to be startin\' somethin\'\nI said you wanna be startin\' somethin\'\nYou got to be startin\' somethin\'\nIt\'s too high to get over (yeah, yeah)\nToo low to get under (yeah, yeah)\nYou\'re stuck in the middle (yeah, yeah)\nAnd the pain is thunder (yeah, yeah)\nIt\'s too high to get over (yeah, yeah)\nToo low to get under (yeah, yeah)\nYou\'re stuck in the middle (yeah, yeah)\nAnd the pain is thunder (yeah, yeah)\nI took my baby to the doctor\nWith a fever, but nothing he found\nBy the time this hit the street\n\nSo you believe in you\nHelp me sing it, ma ma se\nMa ma sa, ma ma coo sa\nMa ma se, ma ma sa\nMa ma coo sa','1606658340576_stars-1245902_960_720.png',NULL,NULL,'2020-11-29 15:56:12',6,41),(155,'Sofia Vitosha','Great view in the a capital of Makedonia.','1606658921363_Sofia-vitosha-kempinski.jpg',NULL,NULL,'2020-11-29 16:06:20',5,41),(156,'Nothing Gold Can Stay','Nature’s first green is gold,\nHer hardest hue to hold.\nHer early leaf’s a flower;\nBut only so an hour.\nThen leaf subsides to leaf.\nSo Eden sank to grief,\nSo dawn goes down to day.\nNothing gold can stay.','1606658935585_maple-leaves-mixed-fall-colors-background-david-gn.jpg',NULL,NULL,'2020-11-29 16:06:44',1,21),(157,'Bambi','The forest prince.','160693311463.png',NULL,NULL,'2020-11-29 16:09:17',4,41),(158,'And Her Fragrance Spreads','She hid away her colors in the frail,\nfragrant flower petals.\nHer radiance that once so bright,\nforgotten and taken from your sight.\nThe long lost splendor,\nthe missed glorious moments.\nHer brilliant wild joined the joyous,\nunceasing winds.','1606659498224_Japan-sakura-trees-pink-flowers-night-pond-temple-garden_1600x1200.jpg',NULL,NULL,'2020-11-29 16:17:12',21,22),(159,'In Search of Peace','Head full of thoughts, soul lingering, body unfeeling\nDo I know me?','1606659988563_maxresdefault.jpg',NULL,NULL,'2020-11-29 16:19:19',20,22),(160,'Fuji at spring','Pink Sakura, Pink Sakura.\nOh where do I begin.\nAdrenaline, knees separate, a touch upon your chin.\n\nPink Sakura, Pink Sakura.\nFrom there do I descend.\nAnd down your neck and collarbone, sensation stirs within.\n\nPink Sakura. I whisper words.\nYou bite your lip again.\nI feel them all, Pink Sakura, the goosebumps on your skin.\n\nAnother inch. Pink Sakura.\nI reach your abdomen.\nAnother breath you can\'t contain; the fire and the sin.\n\nPink Sakura. It\'s getting warm.\nI wonder where you\'ve been.\nI\'m drawing near, it\'s softer here. I pass a subtle grin.\n\nPink Sakura, Pink Sakura.\nYour heart is beating fast.\nI find my way beneath the lace.\nPink Sakura, at last.','1606659776463.png',NULL,NULL,'2020-11-29 16:22:00',21,20),(161,'Pink Sakura','Pink Sakura, Pink Sakura.\nOh where do I begin.\nAdrenaline, knees separate, a touch upon your chin.\n\nPink Sakura, Pink Sakura.\nFrom there do I descend.\nAnd down your neck and collarbone, sensation stirs within.','1606660463183_391de54644bc198c2115a214ff7f13c3.jpg',NULL,NULL,'2020-11-29 16:32:04',21,25),(162,'Sun Castle','Where the Sun King plans his next move in the war with the Moon King.','1606660462335_TDP-L-SMOKY_-SUNSET_094.jpg',NULL,NULL,'2020-11-29 16:32:22',13,20),(163,'Reflections','The world dances, we\ndo or dream the difference\nbetween light and dark','1606660705557_20483.jpg',NULL,NULL,'2020-11-29 16:35:20',20,25),(164,'Violet night','A moment of Barcelona sunset...','1606661412442_barcelona-morning-sky.jpg',NULL,NULL,'2020-11-29 16:48:46',13,23),(165,'込む水の音','古池や蛙飛び込む水の音\nふるいけやかわずとびこむみずのおと','1606661765865_pexels-photo-268533.jpeg',NULL,NULL,'2020-11-29 16:52:17',20,23),(166,'One little monkey','1 little monkey\nwas goin\' 2 the store\nwhen he saw a banana 3\nhe\'d never climbed be4.\nBy 5 o\'clock that evenin\'\nhe was 6 with a stomach ache\n\'cause 7 green bananas\nwas what that monkey 8.\n\nBy 9 o\'clock that evenin\'\nthat monkey was quite ill,\nso 10 we called the doctor\nwho was 11 on the hill.\nThe doctor said, \'You\'re almost dead.\nDon\'t eat green bananas no more.\'\nThe sick little monkey groaned and said,\n\'But that\'s what I 1-2 the 3-4.\'','1606661917990_pexels-photo-1207875.jpeg',NULL,NULL,'2020-11-29 16:57:07',20,28),(167,'Liquid rainbow','Water is such a great reflector.','1606672811173_watersplashphotography.jpg',10,1,'2020-11-29 13:42:54',3,41),(168,'Hello there!','I can show you around. First time meeting a dolphin? What do you drink - coffee or tea? Just kidding - I can\'t talk. You are just hallucinating because you are losing oxigen.','1606727154050_dolphin-203875_960_720.png',9.25,1,'2020-11-30 10:57:41',22,20),(169,'Buddies','They are waiting for their friend Carl - who is always late.','1606727334016_animal-1869337_960_720.png',5.75,6,'2020-11-30 11:06:27',22,23),(170,'Yellow Dolphins','Filmed in Grape Sea. Magnificent creatures.','1606727530016_delfin-bananas-1737840_960_720.png',2,12,'2020-11-30 11:10:48',22,35),(171,'Race me!','Jumping dolphins.','1606727875401_dolphins-blue-sea-mammal-sky.jpg',4.75,7,'2020-11-30 11:16:11',22,37),(172,'I\'ve got, I\'ve got it...Auch.','Dolphin playing with a ball.','1606728050238_dolphin-3730716_960_720.jpg',6.75,4,'2020-11-30 11:19:20',22,39),(173,'Excuse me.','Dolphin making water bubbles.','1606728171230_39933676_101.jpg',7,3,'2020-11-30 11:21:42',22,43),(174,'Release me!','Dolphin and a little kid.','1606728336470_dolphin-1548448_960_720.png',8.5,2,'2020-11-30 11:23:09',22,47),(175,'Unusual couple','Mom, dad  - meet my friend Fin.\nSup paps! Let\'s go babe. Hey, bring my daughter back till 21:00 young man. Daaad!','1606728612422_dolphin-955749_960_720.jpg',6.75,4,'2020-11-30 11:27:20',22,19),(176,'Hey - wanna go skating?','Sure why not.','1606729824605_girl-68819_960_720.jpg',4.5,8,'2020-11-30 11:38:47',22,22),(177,'Unusual Couple','Mom, Dad - this is my girlfriend Lisa. We are so happy for you hunny. Have fun kids.','1606729977750_boy-1790327_960_720.jpg',4.25,9,'2020-11-30 11:51:20',22,24),(178,'Swimming','splash splash splash','1606730257021_dolphin-14715_960_720.jpg',2.5,11,'2020-11-30 11:57:19',22,25),(179,'Lets play fetch','A playfull little dolphin.','1606730357365.jpg',3,10,'2020-11-30 11:58:40',22,27),(180,'Are there yet? No.','Are there yet? No. Are there yet? No. Are there yet? No. Are there yet? No. \nAre there yet?\n.... Are there yet? No!','1606730503492_Discovery-Cove-Baby-Dolphin-1000x480.jpg',6.25,5,'2020-11-30 11:59:55',22,29),(181,'Urulu The Blue','Giant blue rock in Australia.','1606751495212_e5095ebad5d2910f7ffeb1a7aecb0b6a.jpg',NULL,NULL,'2020-11-30 17:50:19',19,28),(182,'Waiting for winter.','Boston Public Garden','1606751643620_Boston-Public-Garden.jpg',NULL,NULL,'2020-11-30 17:53:07',1,28),(183,NULL,NULL,NULL,NULL,NULL,'2020-12-01 18:53:08',17,3),(184,'Lilium ponds','During the day I was walking in the park and took a shot of this marvel of nature.','1606901800856_1e99d3209b43ded516f4590dd74eab0c.png',NULL,NULL,'2020-12-02 10:39:46',23,20),(185,'Couple of Liliums','Flowers in the swamp.','1606898885793_8bb2a309d4196f359ed3095b437b5053.jpg',NULL,NULL,'2020-12-02 10:39:46',23,21),(186,'Pond sheet','Covering up the entire swamp with green.','1606901958775_nature-flowers-lilies-water-pink-water-lilies-pond-water-lily-lotus-water-lily.jpg',NULL,NULL,'2020-12-02 10:39:46',23,25),(187,'Tall Lillies','Lillies going up above the swamp or the swamp is going down under the lillies?','1606901897823_lily-lilly-pond-white-flower-lake-water-water-lilies-spring.jpg',NULL,NULL,'2020-12-02 10:39:46',23,47),(191,'High Up','Very cold up here.','1606933361510.jpg',NULL,NULL,'2020-12-02 20:21:15',5,28),(192,NULL,NULL,NULL,NULL,NULL,'2020-12-02 20:29:05',10,28),(193,'Stars','there are many stars of all kinds','1606934047283.jpg',NULL,NULL,'2020-12-02 20:29:42',6,28),(194,'Golden morning indeed','Amazing sunlight colors.','1606934573675.jpg',NULL,NULL,'2020-12-02 20:34:27',9,28),(195,'Reading Romance','I love to read but sometimes I get dreamy.','1606935240981.jpg',NULL,NULL,'2020-12-02 20:52:46',17,28),(196,'Pebbles','Found them at the beach last Sunday.','1606978647049.jpg',NULL,NULL,'2020-12-03 08:56:29',19,47);
/*!40000 ALTER TABLE `participations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `is_organizer` tinyint(1) DEFAULT 0,
  `points` int(11) DEFAULT 0,
  `level_id` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_users_levels1_idx` (`level_id`),
  CONSTRAINT `fk_users_levels1` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'anthony','Anthony','Tonev','$2b$10$MqE55xhUR.9ScFNL6GllgeOOLrXdKlfl1pO0vgNzlLdtZ.shfEA1i','2020-11-07 16:49:08',1,0,1),(2,'antchev','Nikolay','Antchev','$2b$10$HNF4JfJZU9px7BHga4iVlOPvwDObHJa6ivn5QpYmJGDGaxR/ulJim','2020-11-07 16:49:08',1,0,1),(3,'gosho','Georgi','Georgiev','$2b$10$mV.sXaKeOCq5gpTu5aTGteETlSTK5hfCVbVz4bLjybGwbfEC5fCKm','2020-11-07 16:49:08',0,6,1),(4,'pesho','Petyr','Petrov','$2b$10$34KFjPKOU5iAqW/DgZXyhOPs93yBjDfeg9vnkA.NkZRmtHA5gDGQ2','2020-11-07 16:49:08',1,0,1),(19,'robert','Robert','Capa','$2b$10$2qDTlrme.6fcbxj5w3/KvumtRtEw8kLb33su4ixdB8mvX7YkvjniK','2020-11-29 13:25:32',0,4,1),(20,'Henri','Henri','Cartier-Bresson','$2b$10$/t21N67chEXSqVY1httlsuNA18YODVwi2RjQR7FTfYZ6H5Og0aQyS','2020-11-29 13:25:41',0,54,2),(21,'alfred','Alfred','Stieglitz','$2b$10$oDdtxKNgvKi/Yb4jGeD02OmsZ4lcUJ231vAQRxQl89M7UK9dDkI66','2020-11-29 13:25:54',0,46,1),(22,'Joel','Joel','Meyerowitz','$2b$10$sHD5HSBWIyPnV0MiOFq2Oep8CZsrpLkgfcH0RxRT70bMufXlRelAG','2020-11-29 13:26:40',0,4,1),(23,'Ansel','Ansel','Adams','$2b$10$ryqBP8P1XA0dXKOMWdG7du3b/d4qeJifJ45ciAvEhGeMcpI.u3AB6','2020-11-29 13:26:43',0,27,1),(24,'Eve','Eve','Arnold','$2b$10$0PWeKy4hPDYAQgrg9PZVl.VRyKxunRNLzVr3ti52j1VJ1ZJvz43Ca','2020-11-29 13:26:57',0,1,1),(25,'bruce','Bruce','Davidson','$2b$10$idLErVp7rmd9kBRNqh3Y5eHZVPXtJ9eHB7XHc0izWHOPGHdG8PBom','2020-11-29 13:27:13',0,39,1),(26,'Sebastio','Sebastião','Salgado','$2b$10$Ch1EPHrircDYIQNf/pzQdes9N/OMQPn1w2Ym/5wSbakht5FvPV.IC','2020-11-29 13:27:24',0,3,1),(27,'dorothea','Dorothea','Lange','$2b$10$ZsSLIljhv9d5T.g9DwqxMOupGHco9ZPr1f5ggZI1g4AcWMnROf2g6','2020-11-29 13:27:35',0,24,1),(28,'Bill','Bill','Brandt','$2b$10$y/Vg05Iu/gWrSWy/lYwzV.DJ8ZbnKJ4G6mJE3n2Q7iPT93vDYc7Pq','2020-11-29 13:27:51',0,6,1),(29,'arnold','Arnold','Newman','$2b$10$WPxokPxy8fnucVYMbpjsluNv5naispQt3vkiBi9U9wx6looqYqfCe','2020-11-29 13:27:52',0,4,1),(30,'robertD','Robert','Doisneu','$2b$10$v74KO6FQfAlf8OQ0Y2wTQ.eKdC/T7DUP061cK7vKLjc96eYyUZ/Ki','2020-11-29 13:28:19',0,38,1),(31,'edy','Edward','Weston','$2b$10$u5tFMGt9V0eTrQe6UjPK9.0nPWMZoDKyms3pbX3Mw0wLLV.0rlCwK','2020-11-29 13:28:41',0,26,1),(32,'harry','Harold','Edgerton','$2b$10$OkPgRYi6Y1kWhTV0J9xEaeG4cIqQd.BvTWf.Uu8Nfl1XV5aPQlBLe','2020-11-29 13:28:59',0,3,1),(33,'bert','Bert','Hardy','$2b$10$fMouEBv1s.rjYF30gpkz6eepqaCIz7A0WIRUWYtbJNms9Cmrx1T2S','2020-11-29 13:29:16',0,0,1),(34,'elliot','Elliot','Erwin','$2b$10$8XrhYgO45sUTgb6Vh803puUDpS1JWuQMpjzo0dnI7kDSHmiKdhg1W','2020-11-29 13:29:35',0,3,1),(35,'Julia','Margaret','Cameron','$2b$10$cNqTZ3gDVzq4BUsvAjRlpua125TCIFHsDlI1ohLvTqOry7zkHf3N.','2020-11-29 13:29:39',0,4,1),(36,'steve','Steve','McCurry','$2b$10$SPM3NZ8Zqc6/iAEySgMSsumzsDVZNZFVXz.yAtBffm/8icn.LRyg2','2020-11-29 13:29:52',0,3,1),(37,'Richard','Richard','Avedon','$2b$10$2559WR3W51o1plP3PtjSQeVWNQ8JpWdF2bXWkuFgTY0wmHyEsTxcC','2020-11-29 13:30:10',0,1,1),(38,'philippe','Philippe','Halsman','$2b$10$n/abFxZ8smpWNKgimn6/B.pm3cyUY.R3hGmBYiWVv/HeNSVzrZu1m','2020-11-29 13:30:18',0,0,1),(39,'Irving','Irving','Penn','$2b$10$jkAoFcAsRCIhpctRDVivju2tGKhvQjNDs9FFCw27H/KQ3u6zvWuQ2','2020-11-29 13:30:27',0,4,1),(41,'Don','Don','McCullin','$2b$10$II8HqIdK6yaMegKaAcJOge07RSqicK3zphq17Vjo2Q8bYb1zA8d56','2020-11-29 13:30:58',0,157,3),(42,'man','Man','Ray','$2b$10$uoWTeDb.Zb6M/BrgqIVHr.uyVkczlmyixp9QbUoglK0b19KhVsc7i','2020-11-29 13:31:13',0,6,1),(43,'Margaret','Margaret','Bourke-White','$2b$10$iF3J/Y2hS.Z.rzG74zDxjOFsHG2L7eYZP0B0323ZHUNjE4jlAUjXS','2020-11-29 13:31:22',0,24,1),(44,'Martin','Martin','Parr','$2b$10$gO7sFGtNzc7Ke16DyGujjOk.RKwb5W.gWmjB3pHsKxcW.0KogryfS','2020-11-29 13:31:28',0,3,1),(45,'Frans','Frans','Lanting','$2b$10$iCNWeBVhS/dvRVegP412BefYGyzdA/tjnX8lTB9qVAY3i6J2.JFBq','2020-11-29 13:31:51',0,3,1),(46,'Annie','Annie','Leibovitz','$2b$10$8LG.csgAr5fzsOuYwxg9juHVHvXIHXGsU89uqqYSpXakKFV5YKDkS','2020-11-29 13:32:09',0,6,1),(47,'Cecil','Cecil','Beaton','$2b$10$DQ3IDZR7KOMIQwtAIjFwm.SpnhmvM6Pn/4b.ut9AfUPzX8baei7Ey','2020-11-29 13:32:10',0,101,2),(48,'Nick','Nick','Knight','$2b$10$4USExpAzkqIN/AQ.quhnReBFGxkNqVqqz0YZ5QIitV9MYrNAYgF4G','2020-11-29 13:32:37',0,3,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_contests`
--

DROP TABLE IF EXISTS `v_contests`;
/*!50001 DROP VIEW IF EXISTS `v_contests`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_contests` AS SELECT 
 1 AS `id`,
 1 AS `title`,
 1 AS `category`,
 1 AS `cover`,
 1 AS `isInvitational`,
 1 AS `hasAdditionalJurors`,
 1 AS `createDate`,
 1 AS `phaseOneDate`,
 1 AS `phaseTwoDate`,
 1 AS `isInPhaseOne`,
 1 AS `isInPhaseTwo`,
 1 AS `isGraded`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_finished_and_not_graded_contests`
--

DROP TABLE IF EXISTS `v_finished_and_not_graded_contests`;
/*!50001 DROP VIEW IF EXISTS `v_finished_and_not_graded_contests`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_finished_and_not_graded_contests` AS SELECT 
 1 AS `id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_grades`
--

DROP TABLE IF EXISTS `v_grades`;
/*!50001 DROP VIEW IF EXISTS `v_grades`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_grades` AS SELECT 
 1 AS `id`,
 1 AS `score`,
 1 AS `comment`,
 1 AS `createDate`,
 1 AS `participationId`,
 1 AS `jurorId`,
 1 AS `username`,
 1 AS `userFirstName`,
 1 AS `userLastName`,
 1 AS `userLevel`,
 1 AS `userIsOrganizer`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_notifications`
--

DROP TABLE IF EXISTS `v_notifications`;
/*!50001 DROP VIEW IF EXISTS `v_notifications`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_notifications` AS SELECT 
 1 AS `id`,
 1 AS `message`,
 1 AS `type`,
 1 AS `createDate`,
 1 AS `isRead`,
 1 AS `contestId`,
 1 AS `userId`,
 1 AS `contestTitle`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_participations`
--

DROP TABLE IF EXISTS `v_participations`;
/*!50001 DROP VIEW IF EXISTS `v_participations`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_participations` AS SELECT 
 1 AS `id`,
 1 AS `title`,
 1 AS `story`,
 1 AS `photo`,
 1 AS `score`,
 1 AS `ranking`,
 1 AS `createDate`,
 1 AS `contestId`,
 1 AS `userId`,
 1 AS `username`,
 1 AS `userFirstName`,
 1 AS `userLastName`,
 1 AS `userLevel`,
 1 AS `userIsOrganizer`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_phase2_and_not_notified_contests`
--

DROP TABLE IF EXISTS `v_phase2_and_not_notified_contests`;
/*!50001 DROP VIEW IF EXISTS `v_phase2_and_not_notified_contests`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_phase2_and_not_notified_contests` AS SELECT 
 1 AS `id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_user_leaderboard`
--

DROP TABLE IF EXISTS `v_user_leaderboard`;
/*!50001 DROP VIEW IF EXISTS `v_user_leaderboard`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_user_leaderboard` AS SELECT 
 1 AS `id`,
 1 AS `ranking`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_users`
--

DROP TABLE IF EXISTS `v_users`;
/*!50001 DROP VIEW IF EXISTS `v_users`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_users` AS SELECT 
 1 AS `id`,
 1 AS `username`,
 1 AS `firstName`,
 1 AS `lastName`,
 1 AS `createDate`,
 1 AS `isOrganizer`,
 1 AS `level`,
 1 AS `points`,
 1 AS `ranking`,
 1 AS `levelId`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'photocontest'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `clear_read_notifications` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`%`*/ /*!50106 EVENT `clear_read_notifications` ON SCHEDULE EVERY 1 WEEK STARTS '2020-11-21 17:02:38' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM notifications
	WHERE notifications.is_read = 1 AND (SELECT DATEDIFF(NOW(), notifications.create_date)) > 7; */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
/*!50106 DROP EVENT IF EXISTS `notify_jurors` */;;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`%`*/ /*!50106 EVENT `notify_jurors` ON SCHEDULE EVERY 5 MINUTE STARTS '2020-11-21 16:21:29' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
            DECLARE id INT;
            DECLARE contests_cursor CURSOR FOR SELECT * FROM v_phase2_and_not_notified_contests;

            OPEN contests_cursor;
            contests_loop: LOOP
                FETCH contests_cursor into id;
				CALL send_grade_notification(id);
            END LOOP;
END */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
/*!50106 DROP EVENT IF EXISTS `score_contests` */;;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`%`*/ /*!50106 EVENT `score_contests` ON SCHEDULE EVERY 5 MINUTE STARTS '2020-11-21 16:21:42' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
            DECLARE id INT;
            DECLARE contests_cursor CURSOR FOR SELECT * FROM v_finished_and_not_graded_contests;

            OPEN contests_cursor;
            contests_loop: LOOP
                FETCH contests_cursor into id;
				CALL score_contest(id);
            END LOOP;
END */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'photocontest'
--
/*!50003 DROP FUNCTION IF EXISTS `has_graded` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `has_graded`(participation_id INT, juror_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE hasGraded BOOL;
    SET hasGraded = 0;
    SELECT 	EXISTS(	SELECT * FROM grades AS g
					WHERE g.participation_id = participation_id
                    AND g.juror_id = juror_id) 
			INTO hasGraded;
    RETURN hasGraded;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `has_joined` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `has_joined`(contest_id INT, user_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE hasJoined BOOL;
    SET hasJoined = 0;
    SELECT 	EXISTS(	SELECT * FROM participations AS p
					WHERE p.contest_id = contest_id
                    AND p.user_id = user_id) 
			INTO hasJoined ;
    RETURN hasJoined;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `has_submitted` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `has_submitted`(contest_id INT, user_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE hasJoined BOOL;
    SET hasJoined = 0;
    SELECT 	EXISTS(	SELECT * FROM participations AS p
					WHERE p.contest_id = contest_id
                    AND p.user_id = user_id
                    AND p.photo IS NOT NULL)
			INTO hasJoined ;
    RETURN hasJoined;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_contest_in_phase1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `IS_CONTEST_IN_PHASE1`(contest_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE isContestInPhase1 BOOL;
    SET isContestInPhase1 = 0;
	SELECT 
		((SELECT 
				phase1_date
			FROM
				contests c
			WHERE
				c.id = contest_id) > NOW())
	INTO isContestInPhase1;
    RETURN isContestInPhase1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_contest_in_phase2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `IS_CONTEST_IN_PHASE2`(contest_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE isContestInPhase2 BOOL;
    SET isContestInPhase2 = 0;
	SELECT 
		(NOW() between (SELECT 
				phase1_date
			FROM
				contests c
			WHERE
				c.id = contest_id) AND (SELECT 
				phase2_date
			FROM
				contests c
			WHERE
				c.id = contest_id))
	INTO isContestInPhase2;
    RETURN isContestInPhase2;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_contest_over` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `is_contest_over`(contest_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE isContestOver BOOL;
    SET isContestOver = 0;
	SELECT 
		((SELECT 
				phase2_date
			FROM
				contests c
			WHERE
				c.id = contest_id) < NOW())
	INTO isContestOver;
    RETURN isContestOver;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_juror` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `is_juror`(contest_id INT, user_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE isJuror BOOL;
    SET isJuror = 0;
    SELECT 	EXISTS(	SELECT * FROM jurors AS j
					WHERE j.user_id = user_id
                    AND j.contest_id = contest_id) 
			INTO isJuror ;
    RETURN isJuror;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_participating` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `is_participating`(contest_id INT, user_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE isParticipating BOOL;
    SET isParticipating = 0;
    SELECT 	EXISTS(	SELECT * FROM participations AS p
					WHERE p.user_id = user_id
                    AND p.contest_id = contest_id) 
			INTO isParticipating ;
    RETURN isParticipating;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_additional_jurors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `add_additional_jurors`(IN contest_id INT, IN users TEXT)
BEGIN
	SET @sql = CONCAT(
		'INSERT INTO jurors (contest_id, user_id) ',
		'SELECT ', contest_id, ', id FROM users WHERE id IN (', users, ')'
    );
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_contest_invites` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `add_contest_invites`(IN contest_id INT, IN users TEXT)
BEGIN
	SET @sql = CONCAT(
		'INSERT INTO participations (contest_id, user_id) ',
		'SELECT ', contest_id, ', id FROM users WHERE id IN (', users, ')'
    );
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_organizers_as_jurors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `add_organizers_as_jurors`(IN contest_id INT)
BEGIN
	INSERT INTO jurors (contest_id, user_id)
    SELECT contest_id, id FROM users WHERE is_organizer = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calculate_level` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `calculate_level`(IN user_id INT)
BEGIN

	UPDATE users AS u
	SET u.level_id = (
		SELECT l.id 
        FROM levels l 
        WHERE u.points >= l.required_points 
        ORDER BY required_points DESC 
        LIMIT 1 
    )
	WHERE u.id = user_id;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_contest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `create_contest`(IN title VARCHAR(255), IN category VARCHAR(255), IN cover VARCHAR(100),
														IN is_invitational TINYINT(1), IN has_additional_jurors TINYINT(1),
                                                        IN phase1_date INT, IN phase2_date INT, IN user_id INT)
BEGIN
	INSERT INTO contests (title, category, cover, is_invitational, has_additional_jurors, phase1_date, phase2_date, user_id)
    VALUES (title, category, cover, is_invitational, has_additional_jurors, DATE_ADD(NOW(), INTERVAL phase1_date HOUR), DATE_ADD(NOW(), INTERVAL (phase1_date + phase2_date) HOUR), user_id);
	
    CALL get_contest_by('id', LAST_INSERT_ID(), user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `create_user`(IN username VARCHAR(45), 
													IN password VARCHAR(100), 
													IN first_name VARCHAR(45), 
                                                    IN last_name VARCHAR(45))
BEGIN
	INSERT INTO users (username, password, first_name, last_name)
    VALUES (username, password, first_name, last_name);

    CALL get_user_by('id', LAST_INSERT_ID());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `distribute_points` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `distribute_points`(IN contest_id INT)
BEGIN
	SET @participationBonus = IF((SELECT is_invitational AS isInvitational FROM contests c WHERE c.id = contest_id), 3, 1);
    SET @winnerMultiplier = IF((SELECT score FROM participations p WHERE p.ranking = 1 AND p.contest_id = contest_id LIMIT 1) >= 
								(SELECT score FROM participations p WHERE p.ranking = 2 AND p.contest_id = contest_id LIMIT 1) * 2, 1.5, 1);

SELECT COUNT(p.ranking) INTO @countFirst FROM participations p WHERE p.ranking = 1 AND p.contest_id = contest_id;
SELECT COUNT(p.ranking) INTO @countSecond FROM participations p WHERE p.ranking = 2 AND p.contest_id = contest_id;
SELECT COUNT(p.ranking) INTO @countThird FROM participations p WHERE p.ranking = 3 AND p.contest_id = contest_id;

UPDATE users u,
    participations p 
SET 
    u.points = (CASE
        WHEN p.ranking = 1 THEN (u.points + @participationBonus + @winnerMultiplier * IF(@countFirst > 1, 40, 50))
        WHEN p.ranking = 2 THEN (u.points + @participationBonus + IF(@countSecond > 1, 25, 35))
        WHEN p.ranking = 3 THEN (u.points + @participationBonus + IF(@countThird > 1, 10, 20))
        ELSE (u.points + @participationBonus)
    END)
WHERE
    u.id = p.user_id
        AND p.contest_id = contest_id;
    
UPDATE users u,
    participations p 
SET 
    u.level_id = (SELECT 
            l.id
        FROM
            levels l
        WHERE
            u.points >= l.required_points
        ORDER BY required_points DESC
        LIMIT 1)
WHERE
    u.id = p.user_id
        AND p.contest_id = contest_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_contests` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_contests`(IN page_number INT, IN per_page INT, 
													IN sort_by VARCHAR(15), IN sort_order VARCHAR(5), IN user_id INT)
BEGIN
    DECLARE _offset INT DEFAULT 0;
    DECLARE _order VARCHAR(20) DEFAULT 'title asc';
    
    SET _offset = (page_number - 1) * per_page;
    SET _order = CONCAT(sort_by, ' ', sort_order);
    
	SET @sql_ = CONCAT('SELECT c.*, 
						(SELECT is_juror(c.id, ', user_id, ')) AS isJuror, 
                        (SELECT is_participating(c.id, ', user_id, ')) AS isParticipating, 
                        (SELECT has_submitted(c.id, ', user_id, ')) AS hasSubmitted 
                        FROM v_contests c 
                        ORDER BY ', _order, ' LIMIT ', per_page, ' OFFSET ', _offset);
    
	PREPARE stmt FROM @sql_;
	EXECUTE stmt;    
    DEALLOCATE PREPARE stmt;
    
	SELECT COUNT(*) FROM v_contests INTO @count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_notifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_notifications`()
BEGIN
	SELECT * FROM v_notifications n;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_users`(IN page_number INT, IN per_page INT, 
													IN sort_by VARCHAR(15), IN sort_order VARCHAR(5))
BEGIN
    DECLARE _offset INT DEFAULT 0;
    DECLARE _order VARCHAR(20) DEFAULT 'username asc';
    
    SET _offset = (page_number - 1) * per_page;
    SET _order = CONCAT(sort_by, ' ', sort_order);
    
	SET @sql_ = CONCAT('SELECT * FROM v_users WHERE v_users.isOrganizer = 0 ORDER BY ', _order, ' LIMIT ', per_page, ' OFFSET ', _offset);
    
	PREPARE stmt FROM @sql_;
	EXECUTE stmt;    
    DEALLOCATE PREPARE stmt;
    
    SELECT count(*) FROM v_users WHERE v_users.isOrganizer = 0 INTO @count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_contests_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_contests_by_user`(IN user_id INT)
BEGIN
	SELECT c.*, part.photo IS NOT NULL AS hasSubmitted, 1 AS isParticipating, 0 AS isJuror FROM v_contests c
    JOIN (SELECT * FROM participations p WHERE p.user_id = user_id) AS part ON (part.contest_id = c.id)
    UNION
	SELECT con.*, 0 AS hasSubmitted, 0 AS isParticipating, 1 AS isJuror FROM v_contests con
    JOIN (SELECT * FROM jurors j WHERE j.user_id = user_id) AS jur ON (jur.contest_id = con.id)
    ORDER BY createDate desc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_contest_by` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_contest_by`(IN col_name VARCHAR(255), IN term VARCHAR(255), IN user_id INT)
BEGIN
	SET @sql_ = CONCAT('SELECT c.*, 
						(SELECT is_juror(c.id, ', user_id, ')) AS isJuror, 
                        (SELECT is_participating(c.id, ', user_id, ')) AS isParticipating, 
                        (SELECT has_submitted(c.id, ', user_id, ')) AS hasSubmitted 
                        FROM v_contests c WHERE c.', col_name, ' = ', '"', term, '"');
	PREPARE stmt FROM @sql_;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_grade` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_grade`(IN participation_id INT, IN juror_id INT)
BEGIN
	SELECT * FROM v_grades g
    WHERE g.participationId = participation_id AND g.jurorId = juror_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_grades_by_participation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_grades_by_participation`(IN participation_id INT)
BEGIN
	SELECT * FROM v_grades g
    WHERE g.participationId = participation_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_notifications_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_notifications_by_user`(IN user_id INT)
BEGIN
	SELECT * FROM v_notifications n
    WHERE n.userId = user_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_notifications_by_users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_notifications_by_users`(IN users TEXT)
BEGIN
	SET @sql = CONCAT(
		'SELECT * FROM v_notifications n WHERE n.userId IN (', users, ') AND n.isRead = 0 ORDER BY n.userId;' 
    );
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_participation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_participation`(IN participation_id INT)
BEGIN
	SELECT * FROM v_participations
    WHERE id = participation_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_participations_by_contest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_participations_by_contest`(IN contest_id INT)
BEGIN
	SELECT * FROM v_participations p
    WHERE p.contestId = contest_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_participations_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_participations_by_user`(IN user_id INT)
BEGIN
	SELECT * FROM v_participations p
    WHERE p.userId = user_id
    ORDER BY p.createDate desc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_participation_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_participation_by_user`(IN contest_id INT, IN user_id INT)
BEGIN
	SELECT * FROM v_participations p
    WHERE p.userId = user_id AND p.contestId = contest_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_recent_winners` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_recent_winners`()
BEGIN
	SELECT * FROM v_participations p
    WHERE p.ranking = 1 AND p.photo IS NOT NULL
    ORDER BY p.createDate desc
    LIMIT 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_users_by_contest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_users_by_contest`(IN contest_id INT)
BEGIN
	SELECT * FROM v_users
    JOIN participations p ON (p.contest_id = contest_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_by` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_user_by`(IN col_name VARCHAR(255), IN term VARCHAR(255))
BEGIN
	SET @sql_ = CONCAT('SELECT * FROM v_users WHERE ', col_name, ' = ', '"', term, '"');
	PREPARE stmt FROM @sql_;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_password` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_user_password`(IN username VARCHAR(45))
BEGIN
	SELECT u.password AS password
    FROM users u
    WHERE u.username = username;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `grade_participation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `grade_participation`(IN participation_id INT, 
													IN juror_id INT, 
													IN score INT, 
                                                    IN comment TEXT)
BEGIN
	INSERT INTO grades (score, comment, participation_id, juror_id)
    VALUES (score, comment, participation_id, juror_id);

    CALL get_grade(participation_id, juror_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `join_contest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `join_contest`(IN contest_id INT, IN user_id INT)
BEGIN
	INSERT INTO participations (contest_id, user_id)
    VALUES (contest_id, user_id);
	
    CALL get_participation(LAST_INSERT_ID());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mark_notification_as_read` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `mark_notification_as_read`(IN notification_id INT)
BEGIN
	UPDATE notifications n
    SET n.is_read = 1
    WHERE n.id = notification_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `score_contest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `score_contest`(IN contest_id INT)
BEGIN
	IF ((SELECT is_graded FROM contests c WHERE c.id = contest_id) = 0) THEN
		SET @rank_ = 1;
        SET @lastScore = 0;
		
		UPDATE participations AS part,

		(SELECT temp.participationId, temp.totalScore, if(@lastScore > temp.totalScore, @rank_ := @rank_ + 1, @rank_) AS ranking, @lastScore := temp.totalScore 
			FROM 
				(SELECT p.id AS participationId, SUM(COALESCE(g.score, 3)) AS totalScore
				FROM participations p 
				LEFT JOIN jurors j ON p.contest_id = j.contest_id
				LEFT JOIN grades g ON j.user_id = g.juror_id AND p.id = g.participation_id
				WHERE p.contest_id = contest_id
				GROUP BY p.id
				ORDER BY totalScore desc) AS temp)
        AS r
		
		SET part.ranking = r.ranking,
			part.score = ROUND(r.totalScore / (SELECT count(*) FROM jurors j WHERE j.contest_id = contest_id), 2)
		WHERE part.id = r.participationId AND part.contest_id = contest_id;
		
		UPDATE contests c
		SET c.is_graded = 1
		WHERE c.id = contest_id;
		
		CALL distribute_points(contest_id);
        CALL send_scored_notification(contest_id);
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `send_grade_notification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `send_grade_notification`(IN contest_id INT)
BEGIN
	INSERT INTO notifications (message, type, contest_id, user_id)
    SELECT CONCAT('The contest is now in grading phase till ', DATE_FORMAT(c.phase2_date, "%W, %k:%i")), 'grade', contest_id, j.user_id FROM jurors j, contests c
    WHERE j.contest_id = contest_id AND c.id = contest_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `send_open_notification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `send_open_notification`(IN contest_id INT)
BEGIN
	SET @isInvitational = 0;
    SET @tillDate = '';
    SELECT is_invitational, phase1_date INTO @isInvitational, @tillDate FROM contests c WHERE c.id = contest_id;
    
    SET @sql_ = CONCAT('INSERT INTO notifications (message, type, contest_id, user_id) ',
						'SELECT CONCAT("The contest has just started, you can join until ", DATE_FORMAT(@tillDate, "%b %D, %H:%i")), "open", ', contest_id, ', ');
	
    SET @sql_users_open = CONCAT('u.id AS user_id FROM users u WHERE u.id NOT IN (SELECT j.user_id FROM jurors j WHERE j.contest_id = ', contest_id, ')');
    SET @sql_users_inv = CONCAT('user_id FROM participations p WHERE p.contest_id = ', contest_id);
    
	IF(@isInvitational) THEN SET @sql_ = CONCAT(@sql_, @sql_users_inv);
						ELSE SET @sql_ = CONCAT(@sql_, @sql_users_open);
    END IF;
    
	PREPARE stmt FROM @sql_;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `send_scored_notification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `send_scored_notification`(IN contest_id INT)
BEGIN
	INSERT INTO notifications (message, type, contest_id, user_id)
    SELECT CONCAT('You have finished at ',   CONCAT(p.ranking, CASE
												WHEN p.ranking%100 BETWEEN 11 AND 13 THEN "th"
												WHEN p.ranking%10 = 1 THEN "st"
												WHEN p.ranking%10 = 2 THEN "nd"
												WHEN p.ranking%10 = 3 THEN "rd"
												ELSE "th"
											  END), ' place with an average score of ', p.score), 
    'scored', contest_id, user_id FROM participations p
    WHERE p.contest_id = contest_id;
    
    INSERT INTO notifications (message, type, contest_id, user_id)
    SELECT 'Contest has finished.', 'scored', contest_id, user_id FROM jurors j WHERE j.contest_id = contest_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `submit_participation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `submit_participation`(IN participation_id INT, IN title VARCHAR(255), IN story TEXT,
														IN photo VARCHAR(255))
BEGIN
	UPDATE participations AS p
    SET p.title = title,
		p.story = story,
        p.photo = photo
	WHERE p.id = participation_id;
	
    CALL get_participation(participation_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `v_contests`
--

/*!50001 DROP VIEW IF EXISTS `v_contests`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_contests` AS select `c`.`id` AS `id`,`c`.`title` AS `title`,`c`.`category` AS `category`,`c`.`cover` AS `cover`,`c`.`is_invitational` AS `isInvitational`,`c`.`has_additional_jurors` AS `hasAdditionalJurors`,`c`.`create_date` AS `createDate`,`c`.`phase1_date` AS `phaseOneDate`,`c`.`phase2_date` AS `phaseTwoDate`,(select `IS_CONTEST_IN_PHASE1`(`c`.`id`)) AS `isInPhaseOne`,(select `IS_CONTEST_IN_PHASE2`(`c`.`id`)) AS `isInPhaseTwo`,`c`.`is_graded` AS `isGraded` from `contests` `c` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_finished_and_not_graded_contests`
--

/*!50001 DROP VIEW IF EXISTS `v_finished_and_not_graded_contests`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_finished_and_not_graded_contests` AS select `c`.`id` AS `id` from `contests` `c` where `c`.`is_graded` = 0 and (select `is_contest_over`(`c`.`id`)) = 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_grades`
--

/*!50001 DROP VIEW IF EXISTS `v_grades`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_grades` AS select `g`.`id` AS `id`,`g`.`score` AS `score`,`g`.`comment` AS `comment`,`g`.`create_date` AS `createDate`,`g`.`participation_id` AS `participationId`,`g`.`juror_id` AS `jurorId`,`u`.`username` AS `username`,`u`.`firstName` AS `userFirstName`,`u`.`lastName` AS `userLastName`,`u`.`level` AS `userLevel`,`u`.`isOrganizer` AS `userIsOrganizer` from (`grades` `g` left join `v_users` `u` on(`g`.`juror_id` = `u`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_notifications`
--

/*!50001 DROP VIEW IF EXISTS `v_notifications`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_notifications` AS select `n`.`id` AS `id`,`n`.`message` AS `message`,`n`.`type` AS `type`,`n`.`create_date` AS `createDate`,`n`.`is_read` AS `isRead`,`n`.`contest_id` AS `contestId`,`n`.`user_id` AS `userId`,`c`.`title` AS `contestTitle` from (`notifications` `n` left join `v_contests` `c` on(`n`.`contest_id` = `c`.`id`)) where `n`.`is_read` = 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_participations`
--

/*!50001 DROP VIEW IF EXISTS `v_participations`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_participations` AS select `p`.`id` AS `id`,`p`.`title` AS `title`,`p`.`story` AS `story`,`p`.`photo` AS `photo`,`p`.`score` AS `score`,`p`.`ranking` AS `ranking`,`p`.`create_date` AS `createDate`,`p`.`contest_id` AS `contestId`,`p`.`user_id` AS `userId`,`u`.`username` AS `username`,`u`.`firstName` AS `userFirstName`,`u`.`lastName` AS `userLastName`,`u`.`level` AS `userLevel`,`u`.`isOrganizer` AS `userIsOrganizer` from (`participations` `p` left join `v_users` `u` on(`p`.`user_id` = `u`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_phase2_and_not_notified_contests`
--

/*!50001 DROP VIEW IF EXISTS `v_phase2_and_not_notified_contests`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_phase2_and_not_notified_contests` AS select `c`.`id` AS `id` from `contests` `c` where (select `IS_CONTEST_IN_PHASE2`(`c`.`id`)) = 1 and !exists(select 1 from `notifications` `n` where `n`.`contest_id` = `c`.`id` and `n`.`type` = 'grade' limit 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_user_leaderboard`
--

/*!50001 DROP VIEW IF EXISTS `v_user_leaderboard`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_user_leaderboard` AS select `users`.`id` AS `id`,row_number() over ( order by `users`.`points` desc) AS `ranking` from `users` where `users`.`is_organizer` = 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_users`
--

/*!50001 DROP VIEW IF EXISTS `v_users`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_users` AS select `u`.`id` AS `id`,`u`.`username` AS `username`,`u`.`first_name` AS `firstName`,`u`.`last_name` AS `lastName`,`u`.`create_date` AS `createDate`,`u`.`is_organizer` AS `isOrganizer`,`l`.`name` AS `level`,`u`.`points` AS `points`,`ul`.`ranking` AS `ranking`,`u`.`level_id` AS `levelId` from ((`users` `u` join `levels` `l` on(`l`.`id` = `u`.`level_id`)) left join `v_user_leaderboard` `ul` on(`ul`.`id` = `u`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-03  9:28:01
