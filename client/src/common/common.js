const API_URL = `http://${process.env.REACT_APP_SERVER_HOST}:${process.env.REACT_APP_SERVER_PORT}/`;
const CLIENT_URL = `http://${process.env.REACT_APP_CLIENT_HOST}:${process.env.REACT_APP_CLIENT_PORT}/`;
const SESSION_KEY = 'sessionGHA';
const REFRESH_TIMEOUT = 5000;
const REFRESH_TIMEOUT_KEY = 'refreshTimeoutId';
const PART_IMG_PATH = `${API_URL}images/participations/`;
const COV_IMG_PATH = `${API_URL}images/covers/`;

export { API_URL, CLIENT_URL, SESSION_KEY, REFRESH_TIMEOUT, REFRESH_TIMEOUT_KEY, PART_IMG_PATH, COV_IMG_PATH };
