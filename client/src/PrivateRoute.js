import { Route, Redirect } from 'react-router-dom';
import { useAuth } from './context/auth';

function PrivateRoute({ component: Component, organizer, ...rest }) {
  const { sessionAuth } = useAuth();

  return (
    <Route
      {...rest}
      render={(props) =>
        sessionAuth ? (
          !sessionAuth?.data?.isOrganizer && organizer ? (
            <Redirect to="/" />
          ) : (
            <Component {...props} />
          )
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { referer: props.location },
            }}
          />
        )
      }
    />
  );
}

export default PrivateRoute;
