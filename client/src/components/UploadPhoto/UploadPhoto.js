import { useState, useEffect, useRef } from 'react';
import { FormLabel, Image, Tooltip, Box, Text } from '@chakra-ui/react';
import uploadPhoto from '../../assets/icons/upload-photo.png';
import httpProvider from '../../providers/http-provider.js';

const UploadImage = ({ trigger, setter, setterNew }) => {
  const [upload, setUpload] = useState(null);
  const [fileUrl, setFileUrl] = useState(() => null);
  const [fileName, setFileName] = useState(() => null);

  const inputReff = useRef(null);

  const sendUpload = (upload) => {
    if (!upload) {
      return;
    }
    const imageType = upload.name.split('.').pop();
    const fd = new FormData();

    const fileName = Date.now().toString() + '.' + imageType;

    fd.append('file', upload, fileName);

    setter(fileName);
    httpProvider.post('upload/', fd).catch((err) => {
      console.error(err);
    });
  };
  const dragOver = (e) => {
    e.preventDefault();
  };

  const dragEnter = (e) => {
    e.preventDefault();
  };

  const dragLeave = (e) => {
    e.preventDefault();
  };

  const processImage = (file) => {
    const imageUrl = URL.createObjectURL(file);
    setFileUrl(imageUrl);
    setUpload(file);
    setterNew && setterNew(file);
    setFileName(file.name);
  };
  const fileDrop = (e) => {
    e.preventDefault();
    const files = e.dataTransfer.files;
    if (files.length) {
      processImage(files[0]);
    }
  };
  useEffect(() => {
    sendUpload(upload);
  }, [trigger]);

  return (
    <Box marginTop="1%">
      <Tooltip hasArrow placement="right">
        <Image
          opacity={fileUrl ? 1 : 0.5}
          src={fileUrl || uploadPhoto}
          background="transparent"
          fallbackSrc={uploadPhoto}
          shadow="md"
          width={{ sm: '90%', md: '1200px' }}
          alt="Uplaod-Photo"
          border="solid 1px"
          borderColor="brand.gray1"
          cursor="pointer"
          onClick={() => inputReff.current.click()}
          draggable="true"
          onDragOver={dragOver}
          onDragEnter={dragEnter}
          onDragLeave={dragLeave}
          onDrop={fileDrop}
        ></Image>
      </Tooltip>
      <Text>{fileName}</Text>
      <FormLabel style={{ display: 'none' }}>Select image:</FormLabel>
      <input
        style={{ display: 'none' }}
        ref={inputReff}
        type="file"
        accept="image/*"
        onChange={(e) => {
          processImage(e.target.files[0]);
        }}
      ></input>
      <input style={{ padding: '10px', display: 'none' }} type="submit" name="btn_upload_pic" value="Upload" />
    </Box>
  );
};

export default UploadImage;
