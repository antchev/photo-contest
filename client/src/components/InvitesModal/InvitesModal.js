import { useState, useEffect } from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalFooter,
  ModalBody,
  Button,
  ModalHeader,
  Stack,
  Switch,
} from '@chakra-ui/react';
import httpProvider from '../../providers/http-provider';
import { enumData } from '../../validator/validator.js';
import UserAvatar from '../UserAvatar/UserAvatar';
import UserModal from '../UserModal/UserModal';
import Pagination from '../Pagination/Pagination';

const UsersModal = ({ setContestSettings, isOpen, onOpen, onClose, usersSet, jurorsSet, invitesContext }) => {
  const [allUsers, setAllUsers] = useState([]);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(5);

  const clearState = () => {
    setPage(1);
    setPerPage(5);
  };

  const getUsersByPage = (users, page, perPage) => {
    const offset = (page - 1) * perPage;
    return users.filter((user, i) => {
      if (i >= offset && i < offset + perPage) {
        return user;
      }
      return null;
    });
  };

  const getCheckedStatus = (userId) => {
    if (invitesContext === 'invites') {
      return usersSet.has(userId);
    } else {
      return jurorsSet.has(userId);
    }
  };

  const getAllUsers = () => {
    httpProvider.get('users/').then((res) => {
      switch (invitesContext) {
        case 'invites':
          setAllUsers(res.data.data.filter((item) => !jurorsSet.has(item.id)));
          break;
        case 'jurors':
          setAllUsers(
            res.data.data.filter((item) => item.points >= enumData.levels[2].requiredPoints && !usersSet.has(item.id)),
          );
          break;
        default:
          break;
      }
    });
  };
  useEffect(() => {
    getAllUsers();
    return clearState();
  }, [isOpen]);

  const contestManager = (user) => {
    switch (invitesContext) {
      case 'invites':
        if (!jurorsSet.has(user.id)) {
          if (usersSet.has(user.id)) {
            usersSet.delete(user.id);
          } else {
            usersSet.set(user.id, user);
          }
        }
        setContestSettings('invites', new Map(usersSet));
        break;
      case 'jurors':
        if (!usersSet.has(user.id)) {
          if (jurorsSet.has(user.id)) {
            jurorsSet.delete(user.id);
          } else {
            jurorsSet.set(user.id, user);
          }
        }
        setContestSettings('jurors', new Map(jurorsSet));
        break;
      default:
        break;
    }
  };
  return (
    <>
      <Modal closeOnOverlayClick={false} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{invitesContext === 'invites' ? 'Invite users' : 'Add jurors'}</ModalHeader>
          <ModalBody>
            {allUsers &&
              getUsersByPage(allUsers, page, perPage).map((user) => {
                return (
                  <Stack isInline justifyContent="space-between" ml="6%" mr="6%" key={user.id}>
                    <UserModal userId={user.id} isOrganizer={user.isOrganizer}>
                      <UserAvatar
                        userFirstName={user.firstName}
                        userLastName={user.lastName}
                        userName={user.username}
                        userLevel={user.level}
                        avatarSize="sm"
                      />
                    </UserModal>

                    <Switch
                      size="md"
                      colorScheme={invitesContext === 'invites' ? 'orange' : 'blue'}
                      isChecked={getCheckedStatus(user.id)}
                      onChange={(e) => {
                        contestManager(user);
                      }}
                    />
                  </Stack>
                );
              })}
            <Pagination totalCount={allUsers.length} page={page} perPage={perPage} setPage={(page) => setPage(page)} />
          </ModalBody>
          <ModalFooter>
            <Button colorScheme={invitesContext === 'invites' ? 'orange' : 'blue'} mr={3} onClick={() => onClose()}>
              {invitesContext === 'invites' ? 'Invite' : 'Add'}
            </Button>
            <Button
              onClick={() => {
                onClose();
                setContestSettings(invitesContext, new Map([]));
              }}
            >
              Cancel
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default UsersModal;
