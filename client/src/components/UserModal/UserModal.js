import {
  Text,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalFooter,
  ModalBody,
  Button,
  useDisclosure,
  TabList,
  Tab,
  Tabs,
  TabPanel,
  TabPanels,
  Grid,
  Box,
  Image,
  Stack,
  Tooltip,
  Avatar,
  Skeleton,
} from '@chakra-ui/react';
import useUserData from '../../hooks/user-data';
import ErrorAlert from '../ErrorAlert/ErrorAlert';
import DateView from '../DateView/DateView';
import { NavLink } from 'react-router-dom';
import { useAuth } from '../../context/auth';
import { COV_IMG_PATH, PART_IMG_PATH } from '../../common/common.js';
import { getOrdinalNum } from '../../common/utils.js';

const UserModal = (props) => {
  const { sessionAuth } = useAuth();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const userData = useUserData(props.userId, isOpen);
  const user = userData.data;
  const isLoggedUser = props.userId === sessionAuth.data.id;

  return (
    <>
      <Box onClick={!props.isOrganizer ? onOpen : null} style={{ cursor: 'pointer', maxWidth: '26.5%' }}>
        {props.children}
      </Box>
      <Skeleton isLoaded={!userData.isLoading}>
        <Modal isOpen={isOpen} onClose={onClose} isCentered>
          <ModalOverlay />
          <ModalContent>
            <>
              <ModalBody>
                <Tabs variant="solid-rounded">
                  <TabList>
                    <Tab
                      _selected={{
                        color: 'brand.black1',
                        bg: 'brand.gold1',
                      }}
                      fontWeight="semibold"
                    >
                      User
                    </Tab>
                    <Tab
                      _selected={{
                        color: 'brand.black1',
                        bg: 'brand.gold1',
                      }}
                      fontWeight="semibold"
                    >
                      Contests
                    </Tab>
                    <Tab
                      _selected={{
                        color: 'brand.black1',
                        bg: 'brand.gold1',
                      }}
                      fontWeight="semibold"
                    >
                      Photos
                    </Tab>
                  </TabList>
                  {userData.isLoading ? null : userData.error ? (
                    <ErrorAlert error={userData.error} />
                  ) : (
                    <TabPanels>
                      <TabPanel height="300px">
                        <>
                          <Stack isInline>
                            <Avatar name={`${user.firstName} ${user.lastName}`} />
                            <Text
                              fontWeight="bold"
                              mb="1rem"
                              style={{
                                position: 'absolute',
                                top: '18%',
                                left: '20%',
                              }}
                            >
                              {user.username}
                            </Text>
                          </Stack>
                          <br />
                          <Stack isInline>
                            <Text mb="1rem">Name:</Text>
                            <Text mb="1rem" fontWeight="bold">
                              {user.firstName} {user.lastName}
                            </Text>
                          </Stack>
                          <Stack isInline>
                            <Text mb="1rem">Level:</Text>
                            <Text fontWeight="bold" mb="1rem">
                              {user.isOrganizer ? 'Organizer' : user.level}
                            </Text>
                          </Stack>
                          {!user.isOrganizer && (
                            <>
                              <Stack isInline>
                                <Text mb="1rem">Points:</Text>
                                <Text fontWeight="bold" mb="1rem">
                                  {user.points}
                                </Text>
                              </Stack>
                              <Stack isInline>
                                <Text mb="1rem">Leaderboard:</Text>
                                <Text fontWeight="bold" mb="1rem">
                                  {getOrdinalNum(user.ranking)}
                                </Text>
                              </Stack>
                              <Text mb="1rem">Member since:</Text>
                              <Text fontWeight="bold">
                                <DateView date={user.createDate} short />
                              </Text>
                            </>
                          )}
                        </>
                      </TabPanel>
                      <TabPanel height="300px" overflowY="scroll;">
                        <Grid rowGap={0}>
                          {userData.contests.length ? (
                            userData.contests
                              .sort((a, b) => new Date(b.phaseOneDate).getTime() - new Date(a.phaseOneDate).getTime())
                              .map((contest) => {
                                const participation = userData.participations.find((p) => p.contestId === contest.id);

                                return (
                                  participation && (
                                    <Tooltip
                                      shouldWrapChildren
                                      hasArrow
                                      label={
                                        contest.isGraded
                                          ? `Finished at ${getOrdinalNum(
                                              participation.ranking,
                                            )} place with a score of ${participation.score}`
                                          : 'In progress'
                                      }
                                      _hover={{
                                        bg: 'gray.400',
                                      }}
                                      placement="bottom"
                                      zIndex="10000"
                                      key={contest.id}
                                    >
                                      <NavLink to={`/contests/${contest.id}`} onClick={onClose}>
                                        <Box
                                          padding="2%"
                                          _hover={{
                                            bg: 'gray.500',
                                          }}
                                          width="370px"
                                        >
                                          <Stack isInline justifyContent="left" alignItems="flex-start">
                                            <Box minWidth="17%">
                                              <Image
                                                width="60px"
                                                height="45px"
                                                src={`${COV_IMG_PATH}thumbs/${contest.cover}`}
                                                alt={contest.title}
                                              ></Image>
                                            </Box>
                                            <Box>
                                              <Text fontSize="md" fontWeight="semibold" isTruncated noOfLines={1}>
                                                {contest.title}
                                              </Text>
                                              <Text fontSize="sm" fontStyle="italic" isTruncated noOfLines={1}>
                                                {contest.category}
                                              </Text>
                                            </Box>
                                          </Stack>
                                        </Box>
                                      </NavLink>
                                    </Tooltip>
                                  )
                                );
                              })
                          ) : (
                            <Box fontSize="md" fontWeight="semibold" fontStyle="italic">
                              No joined contests yet.
                            </Box>
                          )}
                        </Grid>
                      </TabPanel>
                      <TabPanel height="300px" overflow="auto">
                        <Grid rowGap={0}>
                          {userData.participations.length && userData.participations.some((p) => p.photo) ? (
                            userData.participations.map((participation) => {
                              const contest = userData.contests.find((c) => c.id === participation.contestId);
                              return (
                                /* If contest is finished or logged user looks at his own profile or logged user is Organizer then show the photos */
                                participation.photo &&
                                  (contest.isGraded || isLoggedUser || sessionAuth.data.isOrganizer) ? (
                                  <Tooltip
                                    shouldWrapChildren
                                    hasArrow
                                    label={`${contest.title}`}
                                    _hover={{
                                      bg: 'gray.400',
                                    }}
                                    placement="bottom"
                                    zIndex={-1}
                                    key={participation.id}
                                  >
                                    <NavLink
                                      key={participation.id}
                                      to={{
                                        pathname: `/contests/${participation.contestId}/participations/${participation.id}`,
                                        state: {
                                          isInPhaseOne: contest.isInPhaseOne,
                                          isInPhaseTwo: contest.isInPhaseTwo,
                                          isJuror: contest.isJuror,
                                        },
                                      }}
                                      onClick={onClose}
                                    >
                                      <Box
                                        padding="2%"
                                        _hover={{
                                          bg: 'gray.500',
                                        }}
                                        width="370px"
                                      >
                                        <Stack isInline justifyContent="left" alignItems="flex-start">
                                          <Box minWidth="17%" className="thumbnail">
                                            <Image
                                              width="60px"
                                              height="45px"
                                              src={`${PART_IMG_PATH}thumbs/${participation.photo.split('.')[0]}.jpg`}
                                              alt={participation.title}
                                            />
                                          </Box>
                                          <Box>
                                            <Text fontSize="md" fontWeight="semibold" isTruncated noOfLines={1}>
                                              {participation.title}
                                            </Text>
                                            <Text fontSize="sm" fontStyle="italic" isTruncated noOfLines={1}>
                                              {participation.story}
                                            </Text>
                                          </Box>
                                        </Stack>
                                      </Box>
                                    </NavLink>
                                  </Tooltip>
                                ) : null
                              );
                            })
                          ) : (
                            <Box fontSize="md" fontWeight="semibold" fontStyle="italic">
                              No uploaded photos yet yet.
                            </Box>
                          )}
                        </Grid>
                      </TabPanel>
                    </TabPanels>
                  )}
                </Tabs>
              </ModalBody>
              <ModalFooter>
                <Button
                  mr={3}
                  onClick={onClose}
                  _hover={{
                    bg: 'brand.gold1',
                    color: 'brand.black1',
                  }}
                  bg="brand.black1"
                  color="brand.white1"
                >
                  Close
                </Button>
              </ModalFooter>
            </>
          </ModalContent>
        </Modal>
      </Skeleton>
    </>
  );
};

export default UserModal;
