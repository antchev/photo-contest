import { useState } from 'react';
import { Button } from '@chakra-ui/react';
import httpProvider from '../../providers/http-provider';
import AlertPopUp from '../AlertPopUp/AlertPopUp';

const JoinButton = ({ contest, triggerReload }) => {
  const [isOpen, setIsOpen] = useState(false);
  const onClose = () => setIsOpen(false);

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [participation, setParticipation] = useState(null);

  const joinContest = () => {
    setLoading(true);
    httpProvider
      .post(`contests/${contest.id}/participations/`)
      .then((res) => {
        setParticipation(res.data.id);

        triggerReload();
        setError(null);
        onClose();
      })
      .catch((error) => {
        if (error.response) {
          setError(error.response.data.error);
        }
      })
      .finally(() => setLoading(false));
  };

  return (
    <>
      {loading ? (
        <>
          {!contest.isParticipating && !contest.isInvitational && contest.isInPhaseOne && !contest.isJuror ? (
            <Button onClick={() => !participation?.id && setIsOpen(true)} background="brand.gold1" color="brand.black1">
              Join
            </Button>
          ) : null}

          {contest.isInvitational && !contest.isParticipating && !contest.isGraded ? (
            <Button disabled={true} background="brand.gray1" color="brand.alwaysWhite1">
              Invitational only
            </Button>
          ) : null}

          {contest.isInPhaseTwo && !contest.isJuror && !contest.isParticipating ? (
            <Button disabled={true} background="brand.gray1" color="brand.alwaysWhite1">
              Closed for submissions
            </Button>
          ) : null}
        </>
      ) : null}

      <AlertPopUp
        isOpen={isOpen}
        onClose={onClose}
        callback={joinContest}
        error={error}
        message={"Are you sure you want to join this contest? You can't undo this action afterwards."}
        title={'Join Contest'}
        confirmText={'Join'}
        cancelText={'Cancel'}
      />
    </>
  );
};

export default JoinButton;
