import { Spinner, Box } from '@chakra-ui/react';

const Loader = () => {
  return (
    <Box
      style={{
        position: 'absolute',
        top: '50%',
        left: '50%',
        width: '100px',
        height: '100px',
        marginTop: '390px',
        marginLeft: '-50px',
      }}
    >
      <Spinner size="xl" />
    </Box>
  );
};

export default Loader;
