import { Image, Box, Text, Tooltip } from '@chakra-ui/react';
import { StarIcon } from '@chakra-ui/icons';
import { PART_IMG_PATH } from '../../common/common';
import uploadPhoto from '../../assets/icons/upload-photo.png';
import './ParticipationItem.css';
import { getOrdinalNum } from '../../common/utils';

const ParticipationItem = ({ title, photo, ranking }) => {
  const src = photo
    ? `${PART_IMG_PATH}thumbs/${photo.replace(/.(png)$/, '.jpg').replace(/.(jpeg)$/, '.jpg')}`
    : uploadPhoto;
  return (
    <Box className="participationItem">
      <Text
        position="absolute"
        marginLeft="1rem"
        marginTop="0.5rem"
        fontSize="md"
        fontWeight="semibold"
        color="brand.alwaysWhite"
      >
        {title}
      </Text>
      <Tooltip label={ranking ? `${getOrdinalNum(ranking)} place` : null}>
        <Box position="absolute" fontWeight="semibold" marginLeft="348px" marginTop="0.5rem" fontSize="sm">
          {ranking === 1 ? (
            <StarIcon color="gold" />
          ) : ranking === 2 ? (
            <StarIcon color="silver" />
          ) : ranking === 3 ? (
            <StarIcon color="#8a5a2d" />
          ) : ranking ? (
            <Text color="brand.alwaysWhite" fontWeight="semi-bold" size="xs">
              {getOrdinalNum(ranking)}
            </Text>
          ) : null}
        </Box>
      </Tooltip>
      <Image src={src} boxShadow="sm" width="sm" />
    </Box>
  );
};

export default ParticipationItem;
