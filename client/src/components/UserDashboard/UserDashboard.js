import React, { useEffect, useState } from 'react';
import { Skeleton, Box } from '@chakra-ui/react';
import httpProvider from '../../providers/http-provider.js';
import ContestsSlider from '../ContestsSlider/ContestsSlider';
import ErrorAlert from '../ErrorAlert/ErrorAlert';
import Loader from '../Loader/Loader';
import { useAuth } from '../../context/auth.js';

const UserDashboard = () => {
  const { sessionAuth } = useAuth();
  const [userContests, setUserContests] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const loggedUser = sessionAuth.data;
  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`users/${loggedUser.id}/contests`)
      .then((res) => {
        setUserContests(res.data);
      })
      .catch((err) => setError(err.message))
      .finally(() => setLoading(false));
  }, []);

  return loading ? (
    <Loader />
  ) : error ? (
    <ErrorAlert error={error} />
  ) : (
    <Skeleton isLoaded={!loading}>
      {userContests && (
        <>
          {loggedUser.isOrganizer ? (
            <Box mb="2%">
              <ContestsSlider
                contests={userContests
                  .sort((a, b) => new Date(a.phaseTwoDate).getTime() - new Date(b.phaseTwoDate).getTime())
                  .filter((c) => c.isInPhaseTwo)}
                label={'Review'}
                items={4}
              />

              <ContestsSlider
                contests={userContests
                  .sort((a, b) => new Date(a.phaseOneDate).getTime() - new Date(b.phaseOneDate).getTime())
                  .filter((c) => c.isInPhaseOne)}
                label={'Open'}
                items={4}
              />

              <ContestsSlider
                contests={userContests
                  .sort((a, b) => new Date(b.phaseTwoDate).getTime() - new Date(a.phaseTwoDate).getTime())
                  .filter((c) => c.isGraded)}
                label={'Finished'}
                items={4}
              />
            </Box>
          ) : (
            <Box mb="2%">
              <ContestsSlider
                contests={userContests
                  .sort((a, b) => b.isInPhaseTwo - a.isInPhaseTwo)
                  .sort((a, b) => new Date(b.phaseTwoDate).getTime() - new Date(a.phaseTwoDate).getTime())
                  .filter((c) => c.isJuror)}
                label={'Juror'}
                items={4}
                noLabel
              />
              <ContestsSlider
                contests={userContests
                  .sort((a, b) => new Date(a.phaseOneDate).getTime() - new Date(b.phaseOneDate).getTime())
                  .filter((c) => c.isInPhaseOne)}
                label={'Open'}
                items={4}
              />
              <ContestsSlider
                contests={userContests
                  .sort((a, b) => new Date(a.phaseTwoDate).getTime() - new Date(b.phaseTwoDate).getTime())
                  .filter((c) => c.isInPhaseTwo)}
                label={'Review'}
                items={4}
              />
              <ContestsSlider
                contests={userContests
                  .sort((a, b) => new Date(b.phaseTwoDate).getTime() - new Date(a.phaseTwoDate).getTime())
                  .filter((c) => c.isGraded)}
                label={'Finished'}
                items={4}
              />
            </Box>
          )}
        </>
      )}
    </Skeleton>
  );
};

export default UserDashboard;
