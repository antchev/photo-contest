import { Box, Stack, Text } from '@chakra-ui/react';
import Timer from 'react-compound-timer';

const ContestItemOverlay = ({ contest }) => {
  const time = contest.isInPhaseOne
    ? new Date(contest.phaseOneDate).getTime() - new Date().getTime()
    : contest.isInPhaseTwo
    ? new Date(contest.phaseTwoDate).getTime() - new Date().getTime()
    : null;

  return (
    <>
      <Box fontWeight="semibold">
        {contest.isGraded ? (
          <>
            <Text>Finished</Text>
            <Text>View photos</Text>
          </>
        ) : (
          <>
            <Box>{contest.isInPhaseOne ? 'Open for' : 'In review for'}</Box>
            <Timer initialTime={time} direction="backward">
              <Stack isInline>
                {!!contest.isInPhaseOne && (
                  <Stack>
                    <Timer.Days />
                    <Box fontSize="xs">days</Box>
                  </Stack>
                )}
                <Stack>
                  <Timer.Hours />
                  <Box fontSize="xs">hours</Box>
                </Stack>
                <Stack>
                  <Timer.Minutes />
                  <Box fontSize="xs">minutes</Box>
                </Stack>
                <Stack>
                  <Timer.Seconds />
                  <Box fontSize="xs">seconds</Box>
                </Stack>
              </Stack>
            </Timer>
          </>
        )}
      </Box>
    </>
  );
};

export default ContestItemOverlay;
