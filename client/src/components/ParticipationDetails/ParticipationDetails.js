import {
  Image,
  Box,
  Text,
  Grid,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Button,
  Skeleton,
  Stack,
  Heading,
  Center,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { PART_IMG_PATH } from '../../common/common.js';
import httpProvider from '../../providers/http-provider.js';
import ErrorAlert from '../ErrorAlert/ErrorAlert';
import RatingStars from '../RatingStars/RatingStars.js';
import { NavLink, withRouter } from 'react-router-dom';
import ReviewModal from '../../containers/ReviewModal.js';
import UserAvatar from '../UserAvatar/UserAvatar.js';
import UserModal from '../UserModal/UserModal.js';
import DateView from '../DateView/DateView.js';
import { getOrdinalNum } from '../../common/utils.js';
import { useAuth } from '../../context/auth.js';
import BackgroundImage from '../BackgroundImage/BackgroundImage.js';

const ParticipationDetails = withRouter(({ match, history, location }) => {
  const [participation, setParticipation] = useState(null);
  const { sessionAuth } = useAuth();
  const [grades, setGrades] = useState([]);
  const { isJuror, isInPhaseOne, isInPhaseTwo } = location.state;
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const participationId = match.params.id;
  const contestId = match.params.contestId;
  const [trigger, triggerReload] = useState(false);

  const checkAccess = async (participation) => {
    const isLoggedUserAJuror = async () => {
      const isJuror = await httpProvider.get(`contests/${participation.contestId}/isJuror`);
      return isJuror.data;
    };

    if (participation.ranking || sessionAuth.data.id === participation.userId || sessionAuth.data.isOrganizer) {
      return true;
    } else if (await isLoggedUserAJuror()) {
      return true;
    } else {
      history.push('/');
      return false;
    }
  };
  const getReviews = (ranking, id) => {
    if (ranking) {
      httpProvider
        .get(`contests/${contestId}/participations/${id}/grade`)
        .then((res) => {
          setGrades(res.data);
        })
        .catch((error) => {
          if (error.response) {
            setGrades([
              {
                comment: error.response.data.error,
                score: 'N/A',
              },
            ]);
          }
        });
    } else if (isJuror && !ranking) {
      httpProvider
        .get(`contests/${contestId}/participations/${participationId}/grade/${sessionAuth.data.id}`)
        .then((res) => {
          const grade = res.data;
          if (grade) {
            setGrades([grade]);
          }
        });
    }
  };

  useEffect(() => {
    setLoading(true);
    setGrades([]);
    httpProvider
      .get(`contests/${contestId}/participations/${participationId}`)
      .then((res) => {
        const participation = res.data;
        if (checkAccess(participation)) {
          setParticipation(participation);
          getReviews(participation.ranking, participation.id);
          if (!participation.photo) {
            history.replace(`/contests/${contestId}`);
            history.push('/submit-photo', { contestId, participationId });
          }
        }
      })
      .catch((error) => {
        if (error.response) {
          setError(error.response.data.error);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, [participationId, contestId, trigger]);

  return error ? (
    <ErrorAlert status="error" error={error} />
  ) : (
    <Skeleton isLoaded={!loading}>
      {participation && (
        <>
          <BackgroundImage src={`${PART_IMG_PATH}org/${participation.photo || 'logo.png'}`} />
          <Grid gap={2} marginTop="2%" templateColumns={{ lg: null, xl: '70% 30%' }}>
            <Center>
              <Box width="100%">
                <Image
                  src={`${PART_IMG_PATH}org/${participation.photo || 'logo.png'}`}
                  boxShadow="sm"
                  marginLeft="5%"
                  width="90%"
                  maxWidth="1200px"
                />
              </Box>
            </Center>
            <Box marginLeft={{ sm: '5%', xl: '0%' }} minW="400px">
              <Heading fontWeight="semi-bold" fontSize="2xl" color="brand.gold1">
                {participation.title}
              </Heading>
              <Stack isInline verticalAlign="center">
                <Box>
                  <RatingStars isHalf={false} rating={participation.score} edit={false} />
                </Box>

                {participation?.ranking && (
                  <Text fontSize="sm" color="brand.gold1">
                    {getOrdinalNum(participation.ranking)} place
                  </Text>
                )}
              </Stack>
              <span>&nbsp;</span>
              <Box mr="15%">
                <UserModal userId={participation.userId} isOrganizer={participation.userIsOrganizer}>
                  <UserAvatar
                    userFirstName={participation.userFirstName}
                    userLastName={participation.userLastName}
                    userName={participation.username}
                    userLevel={participation.userIsOrganizer ? 'Organizer' : participation.userLevel}
                    avatarSize="sm"
                  />
                </UserModal>

                <Text fontSize="lg" textAlign="left" color="brand.white1" display="inline-block">
                  {participation.story}
                </Text>
                <Text>
                  <DateView date={participation.createDate} size="xs" />
                </Text>
              </Box>
              <Box>
                <span>&nbsp;</span>
                {grades.length ? (
                  <Accordion maxW="70%" defaultIndex={grades[0] ? 0 : -1}>
                    <AccordionItem>
                      <AccordionButton>
                        <Box flex="1" textAlign="left">
                          Reviews
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                      <AccordionPanel pb={4}>
                        {grades && grades.length > 0
                          ? grades.map((grade) => {
                              return (
                                <Box key={grade.id} marginTop="2%">
                                  <UserModal userId={grade.jurorId} isOrganizer={grade.userIsOrganizer}>
                                    <UserAvatar
                                      userFirstName={grade.userFirstName}
                                      userLastName={grade.userLastName}
                                      userName={grade.username}
                                      userLevel={grade.userIsOrganizer ? 'Organizer' : grade.userLevel}
                                      avatarSize="sm"
                                    />
                                  </UserModal>
                                  <RatingStars isHalf={false} edit={false} rating={grade.score} />
                                  <Text fontSize="md">{grade.comment}</Text>
                                  <DateView date={grade.createDate} size="xs" />
                                </Box>
                              );
                            })
                          : 'No grades yet'}
                      </AccordionPanel>
                    </AccordionItem>
                  </Accordion>
                ) : null}
              </Box>
              {isJuror ? (
                isInPhaseTwo && !grades.length ? (
                  <Box>
                    <ReviewModal
                      contestId={contestId}
                      participationId={participationId}
                      participation={participation}
                      triggerReload={() => triggerReload(true)}
                    />
                  </Box>
                ) : (
                  !isInPhaseTwo && (
                    <Box mt="2%">
                      <Button disabled="true">{isInPhaseOne ? 'Not in Grading Phase' : 'Finished'}</Button>
                    </Box>
                  )
                )
              ) : null}
              <Box align="end" mr="30%" mt="1%">
                <NavLink
                  to={{
                    pathname: '/contests/' + participation.contestId,
                  }}
                >
                  <Text color="brand.gold1" _hover={{ color: 'white' }}>
                    Back to contest
                  </Text>
                </NavLink>
              </Box>
            </Box>
          </Grid>
        </>
      )}
    </Skeleton>
  );
});

export default ParticipationDetails;
