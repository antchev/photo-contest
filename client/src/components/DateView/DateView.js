import { Box } from '@chakra-ui/react';

const DateView = ({ date, size, short }) => {
  return (
    <Box as="span" fontSize={size}>
      {short
        ? new Date(date).toLocaleDateString('en-US', {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
          })
        : new Date(date).toLocaleDateString('en-US', {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
          })}
    </Box>
  );
};

export default DateView;
