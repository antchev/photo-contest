import ReactStars from 'react-rating-stars-component';
import { Tooltip } from '@chakra-ui/react';

const RatingStars = ({ rating, isHalf, edit, onChange }) => {
  const settings = {
    size: 25,
    count: 10,
    color: '#b3b4b5',
    activeColor: '#ffb700',
    value: rating,
    isHalf: isHalf,
    edit: edit,
  };

  return (
    <Tooltip hasArrow label={rating?.toFixed(2) || 'No rating'} placement="bottom">
      <div style={{ maxWidth: '220px' }}>
        <ReactStars onChange={onChange} {...settings} style={{ float: 'left' }} />
      </div>
    </Tooltip>
  );
};

export default RatingStars;
