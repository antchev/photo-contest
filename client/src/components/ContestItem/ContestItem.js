import { Image, Box, Text } from '@chakra-ui/react';
import { COV_IMG_PATH } from '../../common/common';
import uploadPhoto from '../../assets/icons/upload-photo.png'; //make it
import ContestItemOverlay from '../ContestItemOverlay/ContestItemOverlay';
import './ContestItem.css';

const ContestItem = ({ contest }) => {
  return (
    <>
      <Box
        className="contest-item"
        opacity={1}
        style={{
          filter: contest.isGraded ? 'grayscale(100%)' : contest.isInPhaseTwo ? 'grayscale(70%)' : 'grayscale(0%)',
        }}
      >
        <Text
          position="absolute"
          fontWeight="semibold"
          marginLeft="1rem"
          marginTop="0.5rem"
          fontSize="sm"
          color="brand.alwaysWhite"
        >
          {contest.category}
        </Text>
        <Text
          position="absolute"
          marginLeft="1rem"
          marginTop="11rem"
          textAlign="center"
          fontWeight="semibold"
          fontSize="md"
          color="brand.alwaysWhite"
        >
          {contest.title}
        </Text>
        <Image
          fallbackSrc={uploadPhoto}
          src={`${COV_IMG_PATH}thumbs/${contest.cover}`}
          maxH="14rem"
          width="100%"
          height="auto"
          display="block"
        />
        <Box className="contest-overlay">
          <Box className="contest-overlay-text" fontSize="sm">
            <ContestItemOverlay contest={contest} />
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default ContestItem;
