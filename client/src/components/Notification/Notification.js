import { Image, Box, Text } from '@chakra-ui/react';
import DateView from '../DateView/DateView';
import logo from '../../assets/icons/logo.svg';
import './Notification.css';
import { NavLink } from 'react-router-dom';

const Notification = ({ title, date, message, contestId }) => {
  return (
    <Box padding="2.2%">
      <Image position="absolute" src={logo} left="60%" top="-2%" width="120px" opacity="20%" alt="logo"></Image>
      <DateView date={date} size="xs" />
      <br />
      <NavLink to={`/contests/${contestId}`} className="notification-contest-title">
        <Text fontSize="22px" color="brand.gold1" fontWeight="bold">
          {title}
        </Text>
      </NavLink>
      <br />
      <Text fontWeight="bold">{message}</Text>
    </Box>
  );
};

export default Notification;
