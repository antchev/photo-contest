import React from 'react';
import { Box } from '@chakra-ui/react';

const ErrorLabel = ({ message }) => {
  return (
    <>
      <Box className="ErrorLabel" color="red.500">
        {message}
      </Box>
    </>
  );
};

export default ErrorLabel;
