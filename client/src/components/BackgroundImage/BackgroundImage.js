import { Image } from '@chakra-ui/react';
import backgroundDark from '../../assets/images/background.jpg';
import backgroundLight from '../../assets/images/backgroundlight.jpg';
import { getColorTheme } from '../../context/theme-settings';

const BackgroundImage = ({ src }) => {
  if (!src) {
    src = getColorTheme() === 'dark' ? backgroundDark : backgroundLight;
  }
  return (
    <Image
      style={{
        filter: 'blur(60px)',
        opacity: '60%',
        position: 'fixed',
      }}
      zIndex="-1000"
      position="absolute"
      src={src}
      boxShadow="sm"
      width="100%"
      top="0px"
    />
  );
};

export default BackgroundImage;
