import { Image, Tooltip } from '@chakra-ui/react';
import { COV_IMG_PATH } from '../../common/common';
import './SelectableImage.css';

const SelectableImage = ({ src, cover, setCover, text }) => {
  return (
    <Tooltip hasArrow label={text} zIndex="1001">
      <Image
        maxW="180px"
        padding="10px"
        src={`${COV_IMG_PATH}thumbs/${src}`}
        onClick={() => {
          setCover(src);
        }}
        style={{
          cursor: 'pointer',
          filter: cover === src ? 'grayscale(0%)' : 'grayscale(80%)',
          border: cover === src ? 'solid  2px' : null,
        }}
      ></Image>
    </Tooltip>
  );
};

export default SelectableImage;
