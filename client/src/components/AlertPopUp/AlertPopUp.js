import { useRef } from 'react';
import {
  Button,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from '@chakra-ui/react';
import ErrorAlert from '../ErrorAlert/ErrorAlert';

const AlertPopUp = ({ callback, isOpen, onClose, message, error, confirmText, cancelText, title }) => {
  const cancelRef = useRef();

  return (
    <AlertDialog isOpen={isOpen} leastDestructiveRef={cancelRef} onClose={onClose}>
      <AlertDialogOverlay>
        <AlertDialogContent>
          <AlertDialogHeader fontSize="lg" fontWeight="bold">
            {title}
          </AlertDialogHeader>
          {error ? <ErrorAlert error={error} /> : null}

          <AlertDialogBody>{message}</AlertDialogBody>
          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onClose}>
              {cancelText || 'Cancel'}
            </Button>
            <Button
              colorScheme="red"
              onClick={() => {
                callback();
              }}
              ml={3}
            >
              {confirmText || 'Join'}
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialogOverlay>
    </AlertDialog>
  );
};

export default AlertPopUp;
