import { Box, Grid, Stack, Heading, Skeleton, Button, Center } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { COV_IMG_PATH } from '../../common/common';
import httpProvider from '../../providers/http-provider';
import ErrorAlert from '../ErrorAlert/ErrorAlert';
import ParticipationItem from '../ParticipationItem/ParticipationItem';
import { NavLink } from 'react-router-dom';
import JoinButton from '../JoinButton/JoinButton';
import { useAuth } from '../../context/auth';
import Loader from '../Loader/Loader';
import SubmitParticipation from '../SubmitParticipation/SubmitParticipation';
import ParticipationView from '../ParticipationView/ParticipationView';
import ContestItemOverlay from '../ContestItemOverlay/ContestItemOverlay';
import Masonry from 'react-masonry-component';
import BackgroundImage from '../BackgroundImage/BackgroundImage';

const ContestDetails = ({ match }) => {
  const { sessionAuth } = useAuth();
  const [contest, setContest] = useState(null);
  const [participations, setParticipations] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [userParticipation, setUserParticipation] = useState(null);
  const [trigger, setTrigger] = useState(null);
  const contestId = match.params.id;
  const triggerReload = () => {
    setTrigger(!trigger);
  };

  const getParticipations = (contestData) => {
    if (contestData.isGraded || contestData.isJuror) {
      httpProvider
        .get(`contests/${contestId}/participations`)
        .then((res) => {
          const participationsData = res.data;
          if (contestData.isParticipating) {
            const foundParticipation = participationsData.find((par) => par.userId === sessionAuth.data.id);
            setUserParticipation(foundParticipation);
            setParticipations(participationsData.filter((par) => par.photo));
          } else if (contestData.isJuror || contestData.isGraded) {
            setParticipations(participationsData);
          }
        })
        .catch((error) => {
          if (error.response) {
            setError(error.response.data.error);
          }
        });
    } else if (contest.isParticipating) {
      httpProvider
        .get(`users/${sessionAuth.data.id}/contests/${contestId}`)
        .then((res) => {
          setUserParticipation(res.data);
        })
        .catch((error) => {
          if (error.response) {
            setError(error.response.data.error);
          }
        });
    }
  };

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get(`contests/${contestId}/`)
      .then((res) => {
        const contestData = res.data;
        setContest(contestData);
        getParticipations(contestData);
      })
      .catch((error) => {
        if (error.response) {
          setError(error.response.data.error);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, [contestId, trigger]);

  useEffect(() => {
    if (contest && !userParticipation) {
      getParticipations(contest);
    }
  }, [userParticipation, contest]);

  return loading ? (
    <Loader />
  ) : error ? (
    <ErrorAlert error={error} />
  ) : (
    <Skeleton isLoaded={!loading}>
      {contest && (
        <Box>
          <Stack isInline justifyContent="space-between">
            <Grid marginLeft="2%">
              <Heading fontWeight="bold" fontSize="4xl" color="brand.gold1">
                {contest.title}
              </Heading>
              <Heading fontSize="xl" color="brand.gold1">
                {contest.category}
              </Heading>
              {!sessionAuth.data.isOrganizer && <JoinButton contest={contest} triggerReload={triggerReload} />}
            </Grid>
            {!contest.isGraded && (
              <Box marginRight="3%" textAlign="center">
                <ContestItemOverlay contest={contest} />
              </Box>
            )}
          </Stack>
          <BackgroundImage src={`${COV_IMG_PATH}org/${contest.cover || 'logo.png'}`} />
          {!!(userParticipation && contest.hasSubmitted) && (
            <Center>
              <Box>
                <NavLink
                  to={{
                    pathname: `/contests/${contest.id}/participations/${userParticipation.id}`,
                    state: {
                      isInPhaseOne: contest.isInPhaseOne,
                      isInPhaseTwo: contest.isInPhaseTwo,
                      isJuror: contest.isJuror,
                    },
                  }}
                >
                  <Box marginBottom="1%">
                    <ParticipationView participation={userParticipation} />
                  </Box>
                </NavLink>
              </Box>
            </Center>
          )}

          {contest && userParticipation && !contest.hasSubmitted && contest.isInPhaseOne && !userParticipation.photo ? (
            <Grid gap={2} alignItems="top" justifyItems="center" autoColumns marginLeft="2%" width="90%">
              <SubmitParticipation
                contestId={contest.id}
                participationId={userParticipation.id}
                triggerReload={triggerReload}
              />
            </Grid>
          ) : null}
          <Center>
            <Box width="80%" mt="1%" mb="2%">
              {participations && (contest.isGraded || contest.isJuror) ? (
                <Masonry
                  className={'my-gallery-class'}
                  elementType={'div'}
                  options={{}}
                  disableImagesLoaded={false}
                  updateOnEachImageLoad={false}
                  imagesLoadedOptions={{}}
                >
                  {participations
                    .sort((a, b) => a.ranking - b.ranking)
                    .map((participation) => {
                      if (!participation.photo) return null;
                      return (
                        <NavLink
                          key={participation.id}
                          to={{
                            pathname: `/contests/${contestId}/participations/${participation.id}`,
                            state: {
                              isInPhaseOne: contest.isInPhaseOne,
                              isInPhaseTwo: contest.isInPhaseTwo,
                              isJuror: contest.isJuror,
                            },
                          }}
                        >
                          <ParticipationItem
                            title={participation.title}
                            ranking={participation.ranking}
                            photo={participation.photo}
                          />
                        </NavLink>
                      );
                    })}
                </Masonry>
              ) : contest.isGraded && !participations?.length ? (
                <Button marginLeft="-10%" disabled={true} background="brand.gray1" color="brand.alwaysWhite1">
                  Contest is over with no photos
                </Button>
              ) : null}
            </Box>
          </Center>
        </Box>
      )}
    </Skeleton>
  );
};

export default ContestDetails;
