import { Image, Box, Text, Grid, Stack, Heading, Center } from '@chakra-ui/react';
import { PART_IMG_PATH } from '../../common/common.js';
import RatingStars from '../RatingStars/RatingStars.js';
import UserAvatar from '../UserAvatar/UserAvatar.js';
import UserModal from '../UserModal/UserModal.js';
import DateView from '../DateView/DateView.js';
import { getOrdinalNum } from '../../common/utils.js';

const ParticipationView = ({ participation }) => {
  return (
    participation && (
      <>
        <Grid gap={2} marginTop="1%" templateColumns={{ md: null, lg: '65% 35%' }} width={{ sm: '420px', lg: '820px' }}>
          <Center>
            <Box>
              <Image src={`${PART_IMG_PATH}org/${participation.photo}`} boxShadow="sm" width="500px" />
            </Box>
          </Center>
          <Box>
            <Heading fontWeight="semi-bold" fontSize="2xl" color="brand.gold1" mt="-3%">
              {participation.title}
            </Heading>
            <Stack isInline verticalAlign="center">
              <Box>
                <RatingStars isHalf={false} rating={participation.score} edit={false} />
              </Box>
              {participation?.ranking && (
                <Text fontSize="sm" color="brand.gold1">
                  {getOrdinalNum(participation.ranking)} place
                </Text>
              )}
            </Stack>
            <span>&nbsp;</span>
            <UserModal userId={participation.userId} isOrganizer={participation.userIsOrganizer}>
              <UserAvatar
                userFirstName={participation.userFirstName}
                userLastName={participation.userLastName}
                userName={participation.username}
                userLevel={participation.userIsOrganizer ? 'Organizer' : participation.userLevel}
                avatarSize="sm"
              />
            </UserModal>
            <Text fontSize="lg" textAlign="left" marginLeft="5%" noOfLines={5} isTruncated color="brand.white1">
              {participation.story}
            </Text>
            <Text marginLeft="5%">
              <DateView date={participation.createDate} size="xs" />
            </Text>
          </Box>
        </Grid>
      </>
    )
  );
};

export default ParticipationView;
