import { Image, Box, Text } from '@chakra-ui/react';
import { PART_IMG_PATH } from '../../common/common';
import './HomePageParticipation.css';

const HomePageParticipation = ({ title, photo }) => {
  return (
    <Box className="home-participation-item">
      <Text
        position="absolute"
        fontWeight="semibold"
        marginLeft="1.5rem"
        marginTop="1rem"
        fontSize="lg"
        color="brand.alwaysWhite"
      >
        {title}
      </Text>
      <Image src={`${PART_IMG_PATH}org/${photo}`} boxShadow="sm" maxH="600px" />
    </Box>
  );
};

export default HomePageParticipation;
