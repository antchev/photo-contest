import { Box, Heading, Flex, Image, Button, Grid, useColorMode, Tooltip, Stack } from '@chakra-ui/react';
import { SunIcon, MoonIcon } from '@chakra-ui/icons';
import logo from '../../assets/icons/logo.svg';
import './Header.css';
import { NavLink } from 'react-router-dom';
import { useAuth } from '../../context/auth';
import { logOut } from '../../providers/session-provider.js';
import UserModal from '../UserModal/UserModal.js';
import UserAvatar from '../UserAvatar/UserAvatar.js';
import socket from '../../providers/websocket-provider.js';
import { setColorTheme, getColorTheme } from '../../context/theme-settings';

const Header = ({ rerenderTheme }) => {
  const { sessionAuth, setSessionAuth } = useAuth();
  const { toggleColorMode } = useColorMode();

  const handleLogOut = () => {
    logOut(setSessionAuth);
    socket.emit('logout');
  };

  return (
    <Flex as="nav" className="NavBar" height={{ sm: '100px', md: '100px' }} background="brand.black1">
      <Box display={{ sm: 'flex', md: 'flex' }} mt={{ base: 1, md: 0 }}>
        <NavLink to={`/`}>
          <Button
            bg="transparent"
            size="sm"
            marginTop="-10px"
            color="brand.white1"
            _hover={{ bg: 'brand.gold1', color: 'brand.black1' }}
            display={{ sm: 'none', md: 'inline-block' }}
          >
            {sessionAuth ? 'Dashboard' : 'Home'}
          </Button>
        </NavLink>
        <span>&nbsp;</span>
        <NavLink to={`/contests`}>
          <Button
            bg="transparent"
            size="sm"
            marginTop="-10px"
            color="brand.white1"
            _hover={{ bg: 'brand.gold1', color: 'brand.black1' }}
          >
            Contests
          </Button>
        </NavLink>
        <span>&nbsp;</span>
        {!!sessionAuth?.data?.isOrganizer && (
          <NavLink to={`/contests/create`}>
            <Button
              bg="transparent"
              size="sm"
              marginTop="-10px"
              color="brand.white1"
              _hover={{ bg: 'brand.gold1', color: 'brand.black1' }}
            >
              Create contest
            </Button>
          </NavLink>
        )}
      </Box>
      <Stack position="absolute" top="10px" left="48.8%">
        <NavLink to={`/`}>
          <Image src={logo} width="50px" alt="logo"></Image>
          <Heading
            display={{ sm: 'none', md: 'block' }}
            marginLeft="-70px"
            as="h2"
            color="brand.gold1"
            marginTop="-5px"
            fontSize="20px"
            fontFamily="brand.serif2"
          >
            Golden Hour Awards
          </Heading>
        </NavLink>
      </Stack>
      <Box
        display={{ sm: 'block', md: 'flex' }}
        width={{ sm: 'full', md: 'auto' }}
        alignItems="center"
        flexGrow={1}
        marginRight={{ sm: '20%', md: '0%' }}
      ></Box>
      <Box display={{ sm: 'flex', md: 'flex' }} mt={{ base: 1, md: 0 }}>
        <Box cursor="pointer" mr={5}>
          <Tooltip label={`${getColorTheme() === 'dark' ? 'Light' : 'Dark'} theme`}>
            {getColorTheme() === 'dark' ? (
              <SunIcon
                style={{ filter: getColorTheme() === 'light' ? 'invert(100%)' : 'invert(0%)' }}
                onClick={() => {
                  toggleColorMode();
                  rerenderTheme(setColorTheme());
                }}
              />
            ) : (
              <MoonIcon
                style={{ filter: getColorTheme() === 'light' ? 'invert(100%)' : 'invert(0%)' }}
                onClick={() => {
                  toggleColorMode();
                  rerenderTheme(setColorTheme());
                }}
              />
            )}
          </Tooltip>
        </Box>
        {!sessionAuth ? (
          <>
            <NavLink to={`/login`}>
              <Button
                bg="transparent"
                marginTop="-10px"
                size="sm"
                color="brand.white1"
                _hover={{
                  bg: 'brand.gold1',
                  color: 'brand.black1',
                }}
              >
                Login
              </Button>
            </NavLink>
            <span>&nbsp;</span>
            <NavLink to={`/signup`}>
              <Button
                bg="transparent"
                marginTop="-10px"
                size="sm"
                color="brand.white1"
                _hover={{
                  bg: 'brand.gold1',
                  color: 'brand.black1',
                }}
              >
                Sign up
              </Button>
            </NavLink>
          </>
        ) : (
          <Grid templateColumns={{ sm: '25% 50%', md: '50% 50%' }} gap={2} marginTop="-5px">
            <Box display={{ sm: 'none', md: 'flex' }}>
              <UserModal userId={sessionAuth.data.id} isOrganizer={sessionAuth.data.isOrganizer}>
                <UserAvatar
                  userFirstName={sessionAuth.data.firstName}
                  userLastName={sessionAuth.data.lastName}
                  userName={sessionAuth.data.username}
                  userLevel={sessionAuth.data.isOrganizer ? 'Organizer' : sessionAuth.data.level}
                  avatarSize="sm"
                />
              </UserModal>
            </Box>
            <Box display={{ sm: 'flex', md: 'none' }}>
              <UserModal userId={sessionAuth.data.id} isOrganizer={sessionAuth.data.isOrganizer}>
                <UserAvatar
                  userFirstName={sessionAuth.data.firstName}
                  userLastName={sessionAuth.data.lastName}
                  userName={null}
                  userLevel={null}
                  avatarSize="sm"
                />
              </UserModal>
            </Box>
            <Box>
              <Button
                marginLeft={{ xs: '-50px', md: '0px' }}
                bg="transparent"
                color="brand.white1"
                size="sm"
                _hover={{
                  bg: 'brand.gold1',
                  color: 'brand.black1',
                }}
                onClick={handleLogOut}
              >
                Logout
              </Button>
            </Box>
          </Grid>
        )}
      </Box>
    </Flex>
  );
};

export default Header;
