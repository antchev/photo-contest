import React from 'react';
import { Box } from '@chakra-ui/react';

const PaginationButton = ({ label, value, selected, handler }) => {
  return (
    <Box
      as="button"
      rounded="md"
      bg={selected ? `gray.500` : `none`}
      py={0}
      px={2}
      fontWeight="semibold"
      _hover={{ bg: 'gray.500' }}
      _focus={{ boxShadow: 'outline' }}
      onClick={() => handler(value)}
    >
      {label}
    </Box>
  );
};

export default PaginationButton;
