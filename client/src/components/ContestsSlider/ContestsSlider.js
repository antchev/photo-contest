import { Stack, Text } from '@chakra-ui/react';
import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import { NavLink } from 'react-router-dom';
import ContestItem from '../ContestItem/ContestItem';
import './ContestsSlider.css';

const ContestsSlider = ({ contests, label, items, noLabel }) => {
  const size = contests.length >= items ? items : contests.length ? contests.length : items;
  const emptyMessage =
    label === 'Review' ? `No contests under ${label.toLowerCase()}.` : `No ${label.toLowerCase()} contests.`;
  return (
    <>
      <Stack width={`${size * 16}%`} ml="10%">
        {noLabel && !contests.length ? null : <Text fontWeight="semibold">{label}</Text>}
        {contests.length ? (
          <Stack justifyContent="space-between">
            <Splide
              options={{
                rewind: true,
                height: '16rem',
                perPage: size,
                gap: '1rem',
                pagination: true,
                cover: true,
                type: 'slider',
                arrows: false,
              }}
            >
              {contests.map((contest) => (
                <SplideSlide key={contest.id} minHeight="100%">
                  <NavLink
                    to={{
                      pathname: '/contests/' + contest.id,
                    }}
                  >
                    <ContestItem contest={contest} />
                  </NavLink>
                </SplideSlide>
              ))}
            </Splide>
          </Stack>
        ) : (
          !noLabel && (
            <>
              <Text fontStyle="italic">{emptyMessage}</Text>
              <br />
            </>
          )
        )}
      </Stack>
    </>
  );
};

export default ContestsSlider;
