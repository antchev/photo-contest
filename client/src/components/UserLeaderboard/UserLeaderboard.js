import React, { useState } from 'react';
import { Text, Stack, Box, Progress, Tooltip, Grid } from '@chakra-ui/react';
import UserModal from '../UserModal/UserModal';
import UserAvatar from '../UserAvatar/UserAvatar';
import Pagination from '../Pagination/Pagination';
import { useAuth } from '../../context/auth';
import { enumData } from '../../validator/validator';

const UserLeaderboard = ({ usersData }) => {
  const { sessionAuth } = useAuth();
  const [page, setPage] = useState(1);
  const [perPage] = useState(10);
  const loggedUser = usersData.data.find((user) => user.id === sessionAuth.data.id);

  const getUsersByPage = (users, page, perPage) => {
    const offset = (page - 1) * perPage;
    return users.filter((user, i) => {
      if (i >= offset && i < offset + perPage) {
        return user;
      }
      return null;
    });
  };

  return (
    <>
      <Grid gap={2}>
        {loggedUser && (
          <>
            <Stack mb="5%">
              <Text fontWeight="bold" mb="1%">
                My position
              </Text>
              <Stack isInline justifyContent="space-between" padding="1%">
                <Stack isInline marginLeft="1%">
                  <Text fontWeight="bold" mt="5px">
                    {loggedUser.ranking}
                  </Text>
                  <UserModal userId={loggedUser.id} isOrganizer={loggedUser.isOrganizer}>
                    <UserAvatar
                      userFirstName={loggedUser.firstName}
                      userLastName={loggedUser.lastName}
                      userName={loggedUser.username}
                      userLevel={loggedUser.level}
                      avatarSize="sm"
                    />
                  </UserModal>
                </Stack>
                <Text fontWeight="semibold" mr="10px" mt="12px">
                  {loggedUser.points}
                </Text>
              </Stack>
              <Box>
                <Tooltip hasArrow placement="top" label="Next level" shouldWrapChildren>
                  <Text fontStyle="italic">{enumData.levels[loggedUser.levelId].name}</Text>
                </Tooltip>
                <Tooltip
                  hasArrow
                  placement="bottom"
                  label={`${loggedUser.points} of ${enumData.levels[loggedUser.levelId].requiredPoints}`}
                  shouldWrapChildren
                >
                  <Progress
                    min={enumData.levels[loggedUser.levelId - 1].requiredPoints}
                    max={enumData.levels[loggedUser.levelId].requiredPoints}
                    value={loggedUser.points}
                  />
                </Tooltip>
              </Box>
            </Stack>
          </>
        )}

        <Text fontWeight="bold" mb="1%">
          User Leaderboard
        </Text>
        {usersData &&
          getUsersByPage(usersData.data, page, perPage).map((user, i) => {
            return (
              <Stack
                isInline
                justifyContent="space-between"
                justifyItems="center"
                padding="1%"
                _hover={{
                  bg: 'gray.500',
                }}
                key={user.id}
              >
                <Stack isInline marginLeft="1%">
                  <Text fontWeight="bold" mt="5px">
                    {user.ranking}
                  </Text>
                  <UserModal userId={user.id} isOrganizer={user.isOrganizer}>
                    <UserAvatar
                      userFirstName={user.firstName}
                      userLastName={user.lastName}
                      userName={user.username}
                      userLevel={user.level}
                      avatarSize="sm"
                    />
                  </UserModal>
                </Stack>
                <Text fontWeight="semibold" mr="10px" mt="12px">
                  {user.points}
                </Text>
              </Stack>
            );
          })}
      </Grid>

      <Pagination totalCount={usersData.totalCount} page={page} perPage={perPage} setPage={(page) => setPage(page)} />
    </>
  );
};

export default UserLeaderboard;
