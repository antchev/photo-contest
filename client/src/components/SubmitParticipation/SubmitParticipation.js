import { useEffect, useState } from 'react';
import UploadPhoto from '../UploadPhoto/UploadPhoto';
import ErrorAlert from '../ErrorAlert/ErrorAlert';
import validateParam, { enumData } from '../../validator/validator';
import { Textarea, Box, Grid, Button, Input } from '@chakra-ui/react';
import ErrorLabel from '../ErrorLabel/ErrorLabel';
import httpProvider from '../../providers/http-provider.js';

const schema = { title: true, story: true };
const quote = enumData.photoQuotes[Math.floor(Math.random() * enumData.photoQuotes.length)];

const SubmitParticipation = ({ contestId, participationId, triggerReload }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [validationError, setValidationError] = useState(() => schema);
  const [trigger, setTrigger] = useState(false);
  const [title, setTitle] = useState('');
  const [story, setStory] = useState('');
  const [photo, setPhoto] = useState('');
  const [isPhotoSelected, setIsPhotoSelected] = useState(false);

  useEffect(() => {
    if (!photo) {
      return;
    }
    setLoading(true);
    const data = {
      title,
      story,
      photo,
    };
    httpProvider
      .put(`contests/${contestId}/participations/${participationId}`, data)
      .then((res) => {
        triggerReload();
        setError(null);
      })
      .catch((error) => {
        if (error.response) {
          setError(error.response.data.error);
        }
      })
      .finally(() => setLoading(false));
  }, [photo]);

  const handleInputChange = (e, setter) => {
    const inputValue = e.target.value;
    const name = e.target.name;
    const error = validateParam(name, inputValue);
    const errorObj = { ...validationError };
    setter(inputValue);
    if (error) {
      errorObj[name] = error;
      return setValidationError(errorObj);
    } else {
      errorObj[name] = null;
      return setValidationError(errorObj);
    }
  };
  const isDisabled = !Object.values(validationError).every((v) => !v);

  return (
    <>
      {!loading ? error && <ErrorAlert error={error} /> : null}
      <Box alignItems="center" background="transparent">
        <Grid margin="3%" gap={6} display={{ sm: 'block', md: 'flex' }}>
          <Box p={5}>
            {!isPhotoSelected ? <ErrorLabel message={'Please upload a photo'}></ErrorLabel> : <div>&nbsp;</div>}
            <UploadPhoto trigger={trigger} setter={setPhoto} setterNew={setIsPhotoSelected}></UploadPhoto>
            <Box>&nbsp;</Box>
            <Button
              _hover={{
                bg: isDisabled ? 'brand.red1' : 'brand.gold1',
                color: isDisabled ? 'brand.white1' : 'brand.black1',
              }}
              bg="brand.black1"
              color="brand.white1"
              marginLeft={{ sm: '60%', md: '87%' }}
              maxWidth="100px"
              onClick={() => {
                setTrigger(!trigger);
              }}
              disabled={isDisabled || !isPhotoSelected}
            >
              Submit
            </Button>
          </Box>
          <Box p={5} width={{ sm: '80%', md: '100%' }} height={{ sm: '80%', md: '400px' }}>
            <Box fontStyle="italic" marginBottom="1%">
              “{quote}”
            </Box>
            <Input
              name="title"
              placeholder="Title"
              maxW="700px"
              value={title}
              onChange={(e) => handleInputChange(e, setTitle)}
            />

            {validationError['title'] || !title ? (
              <Box>
                <ErrorLabel message={validationError['title'] || '  '}></ErrorLabel>
              </Box>
            ) : (
              <Box>&nbsp;</Box>
            )}
            <Textarea
              maxW="700px"
              height="550px"
              name="story"
              onChange={(e) => handleInputChange(e, setStory)}
              placeholder="story"
            />
            {validationError['story'] || !story ? (
              <Box>
                <ErrorLabel message={validationError['story'] || '  '}></ErrorLabel>
              </Box>
            ) : (
              <Box>&nbsp;</Box>
            )}
          </Box>
        </Grid>
      </Box>
    </>
  );
};

export default SubmitParticipation;
