import './UserAvatar.css';
import { Avatar, Text, Box, Grid, Tooltip } from '@chakra-ui/react';

const UserAvatar = ({ userFirstName, userLastName, userName, userLevel, avatarSize }) => {
  return (
    <Tooltip hasArrow label={userLevel === 'Organizer' ? null : 'View Profile'} placement="bottom" zIndex="1000">
      <Grid className="avatar-grid" gap={2}>
        <Box className="box1">
          <Avatar name={`${userFirstName} ${userLastName}`} size={avatarSize || 'md'} />
        </Box>
        <Box alignItems="end">
          {userName && (
            <Text marginTop="-5px" fontSize="md" color="brand.gold1" fontWeight="bold">
              {userName}
            </Text>
          )}
          {userLevel && (
            <Text color="brand.gold1" fontSize="xs">
              {userLevel}
            </Text>
          )}
        </Box>
      </Grid>
    </Tooltip>
  );
};

export default UserAvatar;
