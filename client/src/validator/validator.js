export const enumData = {
  userNameMinLength: 3,
  userNameMaxLength: 45,
  passwordMinLength: 3,
  passwordMaxLength: 45,
  titleMinLength: 5,
  titleMaxLength: 200,
  storyMinLength: 10,
  storyMaxLength: 2000,
  commentMinLength: 10,
  commentMaxLength: 2000,
  categoryMinLength: 5,
  categoryMaxLength: 200,
  minId: 1,
  minPage: 1,
  minRating: 0,
  maxRating: 10,
  minDays: 1,
  defaultPage: 1,
  defaultPerPage: 12,
  minPhaseOneDate: 1,
  maxPhaseOneDate: 30,
  minPhaseTwoDate: 1,
  maxPhaseTwoDate: 24,
  phaseOneDateDefault: 7,
  phaseTwoDateDefault: 6,
  sortOrderValues: new Set(['asc', 'desc']),
  defaultSortContestBy: 'createDate',
  defaultSortOrderContestBy: 'desc',
  sortByContestValues: new Set(['title', 'category', 'createDate', 'phaseOneDate', 'phaseTwoDate']),
  defaultSortUserBy: 'username',
  defaultSortOrderUserBy: 'asc',
  sortByUserValues: new Set(['username', 'firstName', 'lastName', 'points', 'level', 'isOrganizer', 'createDate']),
  thumbnailImageThreshold: 400,
  contestCoverOptions: [
    { value: 'cover_default.jpg', text: 'Default' },
    { value: 'cover_0.jpg', text: 'Abstract' },
    { value: 'cover_1.jpg', text: 'Water' },
    { value: 'cover_2.jpg', text: 'Sport' },
    { value: 'cover_3.jpg', text: 'Autumn' },
    { value: 'cover_4.jpg', text: 'Bugs' },
    { value: 'cover_5.jpg', text: 'Flowers' },
    { value: 'cover_6.jpg', text: 'Food' },
    { value: 'cover_7.jpg', text: 'Flow' },
    { value: 'cover_8.jpg', text: 'Sunrise' },
    { value: 'cover_9.jpg', text: 'Ocean' },
    { value: 'cover_10.jpg', text: 'Abstract nature' },
    { value: 'cover_11.jpg', text: 'Reflection' },
    { value: 'cover_12.jpg', text: 'Abstract flowers' },
    { value: 'cover_13.jpg', text: 'Architecture' },
    { value: 'cover_14.jpg', text: 'Spring' },
    { value: 'cover_15.jpg', text: 'Animals' },
    { value: 'cover_16.jpg', text: 'Sunset' },
    { value: 'cover_17.jpg', text: 'Landscape' },
    { value: 'cover_18.jpg', text: 'Winter' },
    { value: 'cover_19.jpg', text: 'Stars' },
  ],
  levels: [
    { id: 1, name: 'Junkie', requiredPoints: 0 },
    { id: 2, name: 'Enthusiast', requiredPoints: 51 },
    { id: 3, name: 'Master', requiredPoints: 151 },
    {
      id: 4,
      name: 'Wise and Benevolent Photo Dictator',
      requiredPoints: 1001,
    },
  ],
  toastTypes: { open: 'dark', grade: 'success', scored: 'warn' },
  photoQuotes: [
    'You don’t take a photograph. You ask quietly to borrow it',
    'Photography is the story I fail to put into words......',
    'We are making photographs to understand what our lives mean to us.',
    'In photography there is a reality so subtle that it becomes more real than reality.',
    'There is one thing the photograph must contain, the humanity of the moment.',
    'Taking an image, freezing a moment, reveals how rich reality truly is.',
  ],
};

const inputFieldsAliases = {
  username: 'User name',
  firstName: 'First name',
  lastName: 'Last name',
};

/**
 * Validate the type of the value.
 * @param {any} val - The passed value to be checked.
 * @param {string} type - The type of the value.
 * @param {string} sub - The type of array elements.
 * @example
 * const func = (a,b,arr) => {
 * checkType( a,'integer');
 * checkType( b,'number');
 * checkType( arr,'array','integer');
 * }
 * func(1,2,[1,2,1.23,3]) // throws an error: 1.23 is not an integer.
 */
const checkType = (val, type, sub) => {
  const error = new Error(`${val} is not of type ${type}`);
  if (type === 'integer') {
    if (!Number.isInteger(val)) throw error;
  } else if (type === 'array') {
    if (!Array.isArray(val)) throw error;
    if (sub) val.every((item) => checkType(item, sub));
  } else if (typeof val !== type) throw error;
  return val;
};

const validateStringOfRange = (value, minLen, maxLen) => {
  return [
    (value) => value,
    (value) => typeof value === 'string',
    (value) => value.trim(),
    (value) => value.trim().length >= minLen,
    (value) => value.trim().length <= maxLen,
  ];
};

/**
   * Calls a every callback function on a given value 
   * @param {any} value - The passed value to be asserted.
   * @param {string} errorMsg - The error message thrown if atleast one fails
   * @param {callback} callback fn that tests what the value SHOULD be. The rest of the arguments are callbacks too
   * @example
   *  predicate(
          12,
          `ID should be A valid integer greater or equal to 1`,
          (id) => Number.isInteger(+id),
          (id) => +id >= enumData.minId
        );
   * // all validation functions have to test what the value SHOULD be
   */
const predicate = (value, errorMsg, ...callbacks) => (!callbacks.every((fn) => fn(value)) ? errorMsg : null);

/**
 * Validate by given type.
 * @param {string} type - The type of validation.
 * @param {any} type - The passed value to be validated.
 * @example
 * validateParam('username',username);
 * // all validation functions have to test what the value SHOULD be
 */

const validateParam = (type, value) => {
  switch (type) {
    case 'id':
    case 'userId':
      return predicate(
        value,
        `${type} should be a number greater or equal to ${enumData.minId}`,
        (id) => Number.isInteger(+id),
        (id) => +id >= enumData.minId,
      );

    case 'username':
    case 'firstName':
    case 'lastName':
      return predicate(
        value,
        `${inputFieldsAliases[type]} should be between ${enumData.userNameMinLength} and ${enumData.userNameMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.userNameMinLength, enumData.userNameMaxLength),
      );

    case 'password':
      return predicate(
        value,
        `Password should be between ${enumData.passwordMinLength} and ${enumData.passwordMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.passwordMinLength, enumData.passwordMaxLength),
      );

    case 'title':
      return predicate(
        value,
        `Title should be between ${enumData.titleMinLength} and ${enumData.titleMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.titleMinLength, enumData.titleMaxLength),
      );

    case 'story':
      return predicate(
        value,
        `Story should be between ${enumData.storyMinLength} and ${enumData.storyMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.storyMinLength, enumData.storyMaxLength),
      );

    case 'photo':
      return predicate(
        value,
        'Photo must be uploaded',
        (photo) => photo,
        (photo) => checkType(photo, 'string'),
      );

    case 'jurors':
    case 'users':
      return predicate(
        !value || value,
        type + ' should be a text of comma separated IDs',
        (CSV) => checkType(CSV, 'string'),
        (CSV) => checkType(CSV.split(',').map(Number), 'array', 'integer'),
      );

    case 'category':
      return predicate(
        value,
        `Category should be between ${enumData.categoryMinLength} and ${enumData.categoryMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.categoryMinLength, enumData.categoryMaxLength),
      );

    case 'isInvitational':
    case 'hasAdditionalJurors':
      return predicate(
        value,
        `${type} hould be a 0 or a 1`,
        (value) => Number.isInteger(+value),
        (value) => value === 0 || value === 1,
      );

    //put hardcoded values in enumData
    case 'phaseOneDate':
      return predicate(
        value,
        `${type} should be between ${enumData.minPhaseOneDate} and ${enumData.maxPhaseOneDate} days`,
        (value) => Number.isInteger(+value),
        (value) => +value >= enumData.minPhaseOneDate,
        (value) => +value <= enumData.maxPhaseOneDate,
      );

    case 'phaseTwoDate':
      return predicate(
        value,
        `${type} should be between ${enumData.minPhaseTwoDate} and ${enumData.maxPhaseTwoDate} hours`,
        (value) => Number.isInteger(+value),
        (value) => +value >= enumData.minPhaseTwoDate,
        (value) => +value <= enumData.maxPhaseTwoDate,
      );

    case 'score':
      return predicate(
        value,
        `${type} should be between ${enumData.minRating} and ${enumData.maxRating}`,
        (value) => Number.isInteger(+value),
        (value) => +value >= enumData.minRating,
        (value) => +value <= enumData.maxRating,
      );

    case 'comment':
      return predicate(
        value,
        `Comment should be between ${enumData.commentMinLength} and ${enumData.commentMaxLength} characters long`,
        ...validateStringOfRange(value, enumData.commentMinLength, enumData.commentMaxLength),
      );

    case 'page':
    case 'perPage':
      return (
        !value ||
        predicate(
          value,
          `Page should be a number greater or equal to ${enumData.minPage}`,
          (page) => typeof +page === 'number',
          (page) => Number.isInteger(+page),
          (page) => +page >= enumData.minPage,
        )
      );

    case 'sortBy':
      return (
        !value ||
        predicate(
          value,
          `Sorting users column should be one of '${[...enumData.sortByUserValues].join("','")}'`,
          (sortBy) => typeof sortBy === 'string',
          (sortBy) => enumData.sortByUserValues.has(sortBy),
        ) ||
        predicate(
          value,
          `Sorting contests column should be one of '${[...enumData.sortByContestValues].join("','")}'`,
          (sortBy) => typeof sortBy === 'string',
          (sortBy) => enumData.sortByContestValues.has(sortBy),
        )
      );

    case 'sortOrder':
      return (
        !value ||
        predicate(
          value,
          'Sorting order should be "asc" or "desc"',
          (sortOrder) => typeof sortOrder === 'string',
          (sortOrder) => enumData.sortOrderValues.has(sortOrder),
        )
      );

    default:
      break;
  }
};

export default validateParam;
