import './App.css';
import { useState } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import './providers/websocket-provider.js';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Dashboard from './containers/Dashboard';
import Home from './containers/Home';
import Credentials from './containers/Credentials';
import Header from './components/Header/Header';
import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import { AuthContext } from './context/auth';
import PrivateRoute from './PrivateRoute';
import { breakpoints, colors, config, fonts } from './context/theme-settings.js';
import ContestList from './containers/ContestsList';
import ContestDetails from './components/ContestDetails/ContestDetails';
import { SESSION_KEY, REFRESH_TIMEOUT_KEY } from '../src/common/common.js';
import { manageSession } from './providers/session-provider.js';
import ParticipationDetails from './components/ParticipationDetails/ParticipationDetails';
import CreateContest from './containers/CreateContest';
import { ToastContainer } from 'react-toastify';
import socket from './providers/websocket-provider.js';
import NotFound from './containers/NotFound';

const App = () => {
  const [theme, setTheme] = useState(extendTheme({ breakpoints, colors, config, fonts }));
  const storageData = JSON.parse(localStorage.getItem(SESSION_KEY));
  const [sessionAuth, setSessionAuth] = useState(storageData);

  const rerenderTheme = (newColors) => {
    setTheme(extendTheme({ breakpoints, colors: newColors, config, fonts }));
  };

  const setSession = (data) => {
    if (data) {
      localStorage.setItem(SESSION_KEY, JSON.stringify(data));
      socket.emit('login', data.data.id);
    } else {
      clearTimeout(localStorage.getItem(REFRESH_TIMEOUT_KEY));
      localStorage.removeItem(REFRESH_TIMEOUT_KEY);
      localStorage.removeItem(SESSION_KEY);
      socket.emit('logout');
    }
    setSessionAuth(data);
  };

  manageSession(setSession);

  return (
    <ChakraProvider theme={theme} resetCS backgroundColor="red">
      <AuthContext.Provider value={{ sessionAuth, setSessionAuth: setSession }}>
        <div className="App">
          <BrowserRouter>
            <Header rerenderTheme={rerenderTheme} />
            <ToastContainer style={{ width: '400px', marginTop: '3%' }} autoClose={false} newestOnTop={true} />
            <Switch>
              {/* Public routes */}
              <Route path="/" exact component={Home} />
              <Route path="/login" component={Credentials} />
              <Route path="/signup" component={Credentials} />
              {/*Private routes */}
              <PrivateRoute path="/dashboard" exact component={Dashboard} />
              <PrivateRoute path="/contests/" exact component={ContestList} />
              <PrivateRoute path="/contests/create" exact component={CreateContest} organizer />
              <PrivateRoute path="/contests/:id" exact component={ContestDetails} />
              <PrivateRoute path="/contests/:contestId/participations/:id" exact component={ParticipationDetails} />

              <Route path="" component={NotFound} />
            </Switch>
          </BrowserRouter>
        </div>
      </AuthContext.Provider>
    </ChakraProvider>
  );
};

export default App;
