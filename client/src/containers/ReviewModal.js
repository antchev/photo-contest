import { useState, useRef } from 'react';
import ErrorAlert from '../components/ErrorAlert/ErrorAlert';
import {
  Button,
  Modal,
  useDisclosure,
  ModalFooter,
  ModalOverlay,
  ModalContent,
  ModalBody,
  ModalHeader,
  Textarea,
  ModalCloseButton,
  Box,
  Stack,
  Switch,
  FormLabel,
} from '@chakra-ui/react';
import Loader from '../components/Loader/Loader';
import RatingStars from '../components/RatingStars/RatingStars';
import validateParam from '../validator/validator';
import ErrorLabel from '../components/ErrorLabel/ErrorLabel';
import httpProvider from '../providers/http-provider';
import AlertPopUp from '../components/AlertPopUp/AlertPopUp';

const ReviewModal = ({ contestId, participationId, participation, triggerReload }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [errorClient, setErrorClient] = useState(null);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [rating, setRating] = useState(1);
  const [editStars, setEditStars] = useState(true);
  const [invalidCategory, setInvalidCategory] = useState(false);
  const [starsKey, setStarsKey] = useState(0);
  const [review, setReview] = useState('');

  const [isOpenAlert, setIsOpenAlert] = useState(false);
  const onCloseAlert = () => setIsOpenAlert(false);

  const initialRef = useRef();

  const handleInvalidCategory = () => {
    if (invalidCategory) {
      setInvalidCategory(false);
      setReview('');
      setRating(1);
      setEditStars(true);
      setStarsKey(Math.random());
    } else {
      setInvalidCategory(true);
      setReview('Wrong category.');
      setRating(0);
      setEditStars(false);
      setStarsKey(Math.random());
    }
  };

  const reviewPhoto = (comment, score) => {
    setLoading(true);
    setError(null);
    const data = { score, comment };
    httpProvider
      .post(`contests/${contestId}/participations/${participationId}/grade`, data)
      .then((res) => {
        onClose();
        triggerReload();
        setIsOpenAlert(false);
      })
      .catch((error) => {
        if (error.response) {
          setError(error.response.data.error);
          setIsOpenAlert(false);
        }
      })
      .finally(() => {
        setLoading(false);
        setIsOpenAlert(false);
      });
  };

  const submitReviewRating = (comment, score) => {
    const validatedReview = validateParam('comment', comment);
    if (validatedReview) {
      setIsOpenAlert(false);
      return setErrorClient(validatedReview);
    }
    const validatedRating = validateParam('score', score);
    if (validatedRating) {
      setIsOpenAlert(false);
      return setErrorClient(validatedRating);
    } else {
      reviewPhoto(comment, score);
      setErrorClient(null);
    }
  };

  const ratingChanged = (stars) => {
    setRating(stars);
  };

  return (
    <>
      <Button
        _hover={{ bg: 'brand.gold1', color: 'brand.black1' }}
        bg="brand.gold1"
        color="brand.black1"
        onClick={onOpen}
      >
        {'Review'}
      </Button>
      <Modal
        initialFocusRef={initialRef}
        blockScrollOnMount={false}
        closeOnOverlayClick={false}
        isOpen={isOpen}
        onClose={onClose}
        isCentered
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader style={{ fontWeight: 750 }}>{'Rate and Review'}</ModalHeader>
          <ModalHeader>{participation.title}</ModalHeader>
          <ModalBody>
            <Box>Please, give a rating</Box>

            <ModalCloseButton
              bg="brand.red1"
              color="brand.alwaysWhite"
              _hover={{ bg: 'brand.red1', color: 'brand.alwaysWhite' }}
            />
            {!loading ? (
              <>
                <Box maxW="220px">
                  <RatingStars
                    key={starsKey}
                    rating={rating}
                    isHalf={false}
                    edit={editStars}
                    style={{ float: 'left' }}
                    onChange={ratingChanged}
                  />
                </Box>
                <Stack isInline justofyContent="start" alignItems="start" mt="1%">
                  <FormLabel htmlFor="invalidCategory">Invalid category</FormLabel>
                  <Switch
                    colorScheme="red"
                    id="invalidCategory"
                    onChange={handleInvalidCategory}
                    isChecked={invalidCategory}
                  />
                </Stack>
              </>
            ) : (
              <Loader />
            )}
            <Box>Leave your thoughts on the photo</Box>
            {!loading ? errorClient && <ErrorLabel message={errorClient} /> : null}
            {!loading ? error && <ErrorAlert error={error} /> : null}
            <Textarea
              ref={initialRef}
              marginTop="2%"
              value={review}
              onChange={(e) => setReview(e.target.value)}
            ></Textarea>
          </ModalBody>
          <ModalFooter>
            {!loading && (
              <Button
                marginRight="80%"
                _hover={{ bg: 'brand.gold1', color: 'brand.black1' }}
                bg="brand.gold1"
                color="brand.black1"
                onClick={() => setIsOpenAlert(true)}
              >
                Submit
              </Button>
            )}
          </ModalFooter>
        </ModalContent>
      </Modal>

      <AlertPopUp
        isOpen={isOpenAlert}
        onClose={onCloseAlert}
        callback={() => submitReviewRating(review, rating)}
        message={`Are you sure you want to grade this photo with score ${rating} You can't undo this action afterwards.`}
        title={'Grade Photo'}
        confirmText={'Grade'}
        cancelText={'Cancel'}
      />
    </>
  );
};

export default ReviewModal;
