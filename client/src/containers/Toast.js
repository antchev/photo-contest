import { toast } from 'react-toastify';
const toastManager = {
  toast: toast.dark,
  themes: { dark: toast.dark, light: toast },
};

export default toastManager;
