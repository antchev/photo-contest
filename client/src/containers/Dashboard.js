import React, { useEffect, useState } from 'react';
import { Box, Skeleton, Stack } from '@chakra-ui/react';
import httpProvider from '../providers/http-provider.js';
import ErrorAlert from '../components/ErrorAlert/ErrorAlert';
import UserLeaderboard from '../components/UserLeaderboard/UserLeaderboard';
import UserDashboard from '../components/UserDashboard/UserDashboard.js';
import BackgroundImage from '../components/BackgroundImage/BackgroundImage.js';

const Dashboard = (props) => {
  const [usersData, setUsersData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get('users')
      .then((res) => {
        setUsersData(res.data);
      })
      .catch((err) => setError(err.message))
      .finally(() => setLoading(false));
  }, []);

  return error ? (
    <ErrorAlert error={error} />
  ) : (
    <Skeleton isLoaded={!loading}>
      <BackgroundImage />
      <Stack isInline position="relative">
        <Box position="absolute" width="100%" minHeight="100%">
          <UserDashboard />
        </Box>
        <Box position="absolute" left="78%" width="14%">
          {usersData && <UserLeaderboard usersData={usersData} />}
        </Box>
      </Stack>
    </Skeleton>
  );
};

export default Dashboard;
