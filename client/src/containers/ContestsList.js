import React, { useEffect, useState } from 'react';
import { Skeleton } from '@chakra-ui/react';
import httpProvider from '../providers/http-provider.js';
import ContestsSlider from '../components/ContestsSlider/ContestsSlider';
import ErrorAlert from '../components/ErrorAlert/ErrorAlert';
import Loader from '../components/Loader/Loader';
import BackgroundImage from '../components/BackgroundImage/BackgroundImage';

const ContestList = (props) => {
  const [contests, setContests] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  
  useEffect(() => {
    setLoading(true);
    httpProvider
      .get('contests')
      .then((res) => {
        setContests(res.data.data);
      })
      .catch((err) => setError(err.message))
      .finally(() => setLoading(false));
  }, []);

  return loading ? (
    <Loader />
  ) : error ? (
    <ErrorAlert error={error} />
  ) : (
    <Skeleton isLoaded={!loading}>
      <BackgroundImage />
      {contests && (
        <>
          <ContestsSlider
            contests={contests
              .sort((a, b) => new Date(a.phaseOneDate).getTime() - new Date(b.phaseOneDate).getTime())
              .filter((c) => c.isInPhaseOne)}
            label={'Open'}
            items={5}
          />
          <ContestsSlider
            contests={contests
              .sort((a, b) => new Date(a.phaseTwoDate).getTime() - new Date(b.phaseTwoDate).getTime())
              .filter((c) => c.isInPhaseTwo)}
            label={'Review'}
            items={5}
          />
          <ContestsSlider
            contests={contests
              .sort((a, b) => new Date(b.phaseTwoDate).getTime() - new Date(a.phaseTwoDate).getTime())
              .filter((c) => c.isGraded)}
            label={'Finished'}
            items={5}
          />
        </>
      )}
    </Skeleton>
  );
};

export default ContestList;
