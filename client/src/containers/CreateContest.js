import { useState } from 'react';
import {
  Box,
  Grid,
  Button,
  Input,
  Switch,
  FormLabel,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  Text,
  useDisclosure,
  Skeleton,
  Stack,
  Tag,
  TagLabel,
  TagCloseButton,
} from '@chakra-ui/react';
import httpProvider from '../providers/http-provider.js';
import ErrorAlert from '../components/ErrorAlert/ErrorAlert.js';
import validateParam, { enumData } from '../validator/validator.js';
import ErrorLabel from '../components/ErrorLabel/ErrorLabel';
import InvitesModal from '../components/InvitesModal/InvitesModal';
import SelectableImage from '../components/SelectableImage/SelectableImage.js';
import { withRouter } from 'react-router-dom';
import { getColorTheme } from '../context/theme-settings.js';
import AlertPopUp from '../components/AlertPopUp/AlertPopUp.js';
import BackgroundImage from '../components/BackgroundImage/BackgroundImage.js';

const schema = {
  title: true,
  category: true,
};

const CreateContest = withRouter(({ history }) => {
  const { isOpen, onOpen, onClose } = useDisclosure({ defaultIsOpen: false });
  const [isOpenAlert, setIsOpenAlert] = useState(false);
  const onCloseAlert = () => setIsOpenAlert(false);

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [validationError, setValidationError] = useState(() => schema);
  const [usersSet, setUsers] = useState(new Map([]));
  const [jurorsSet, setJurors] = useState(new Map([]));
  const [invitesContext, setInvitesContext] = useState(null);
  const [phaseOneDate, setPhaseOneDate] = useState(enumData.phaseOneDateDefault);
  const [phaseTwoDate, setPhaseTwoDate] = useState(enumData.phaseTwoDateDefault);
  const [title, setTitle] = useState('');
  const [category, setCategory] = useState('');
  const [isInvitational, setInvitational] = useState(0);
  const [hasAdditionalJurors, setHasAdditionalJurors] = useState(0);
  const [cover, setCover] = useState('cover_default.jpg');

  const setContestSettings = (type, usersSet) => {
    switch (type) {
      case 'invites':
        setInvitational(!!usersSet.size ? 1 : 0);
        setUsers(usersSet);
        break;
      case 'jurors':
        setHasAdditionalJurors(!!usersSet.size ? 1 : 0);
        setJurors(usersSet);
        break;
      default:
        break;
    }
  };

  const removeUser = (userId, type) => {
    switch (type) {
      case 'invite':
        usersSet.delete(userId);
        if (!usersSet.size) {
          setInvitational(false);
        }
        setUsers(new Map(usersSet));
        break;
      case 'juror':
        jurorsSet.delete(userId);
        if (!jurorsSet.size) {
          setHasAdditionalJurors(false);
        }
        setJurors(new Map(jurorsSet));
        break;
      default:
        break;
    }
  };

  const createContest = () => {
    setLoading(true);

    const phaseOneDateToDays = phaseOneDate * 24;
    const usersCSV = Array.from(usersSet?.keys()).join(',');
    const jurosCSV = Array.from(jurorsSet?.keys()).join(',');
    const data = {
      title,
      category,
      cover,
      phaseOneDate: phaseOneDateToDays,
      phaseTwoDate,
      users: usersCSV,
      jurors: jurosCSV,
      isInvitational,
      hasAdditionalJurors,
    };

    httpProvider
      .post(`contests/`, data)
      .then((res) => {
        setError(null);
        setIsOpenAlert(false);
        setTimeout(() => history.push('/contests'), 0);

        // setCover('');
      })
      .catch((error) => {
        if (error.response) {
          setError(error.response.data.error);
        }
      })
      .finally(() => setLoading(false));
  };
  const handleChangePhaseOne = (phaseOneDate) => setPhaseOneDate(phaseOneDate);
  const handleChangePhaseTwo = (phaseTwoDate) => setPhaseTwoDate(phaseTwoDate);

  const handleInputChange = (e, setter) => {
    const inputValue = e.target.value;
    const name = e.target.name;
    const error = validateParam(name, inputValue);
    const errorObj = { ...validationError };
    setter(inputValue);
    if (error) {
      errorObj[name] = error;
      return setValidationError(errorObj);
    } else {
      errorObj[name] = null;
      return setValidationError(errorObj);
    }
  };
  const isDisabled = !Object.values(validationError).every((v) => !v);
  return (
    <>
      {error ? (
        <ErrorAlert status="error" error={error} />
      ) : (
        <Skeleton isLoaded={!loading}>
          <BackgroundImage />
          <Grid>
            <Stack
              position="absolute"
              right={{ md: '10px', sm: '20px', lg: '300px' }}
              maxH="780px"
              maxW="200px"
              align="center"
              style={{
                borderBottom:
                  getColorTheme() === 'dark'
                    ? '2px rgba(255, 255, 255, 0.16) solid'
                    : '2px rgba(25, 25, 25, 0.46) solid',
                overflowY: 'scroll',
              }}
            >
              {enumData.contestCoverOptions.map((item) => (
                <SelectableImage
                  src={item.value}
                  key={item.text}
                  text={item.text}
                  cover={cover}
                  setCover={setCover}
                ></SelectableImage>
              ))}
            </Stack>
            <Box display="flex" alignItems="center" justifyContent="space-between">
              <Grid margin="1%" marginLeft={{ sm: '5%', md: '10%', lg: '18%' }} height="800px" width="50%" gap={6}>
                <Box p={5} borderWidth="1px">
                  <Input
                    name="title"
                    placeholder="Title"
                    value={title}
                    onChange={(e) => handleInputChange(e, setTitle)}
                  />
                  {validationError['title'] ? (
                    <ErrorLabel message={validationError['title']}></ErrorLabel>
                  ) : (
                    <div>&nbsp;</div>
                  )}
                  <Input name="category" placeholder="Category" onChange={(e) => handleInputChange(e, setCategory)} />
                  {validationError['category'] ? (
                    <ErrorLabel message={validationError['category']}></ErrorLabel>
                  ) : (
                    <div>&nbsp;</div>
                  )}
                </Box>
                <Text>Phase One Duration (Days)</Text>
                <Slider
                  min={enumData.minPhaseOneDate}
                  max={enumData.maxPhaseOneDate}
                  flex="1"
                  value={phaseOneDate}
                  onChange={handleChangePhaseOne}
                >
                  <SliderTrack>
                    <SliderFilledTrack />
                  </SliderTrack>
                  <SliderThumb
                    fontSize="sm"
                    color="brand.black1"
                    background="brand.gold1"
                    boxSize="32px"
                    children={phaseOneDate}
                  />
                </Slider>
                <Text>Phase Two Duration (Hours)</Text>
                <Slider
                  min={enumData.minPhaseTwoDate}
                  max={enumData.maxPhaseTwoDate}
                  flex="1"
                  value={phaseTwoDate}
                  onChange={handleChangePhaseTwo}
                >
                  <SliderTrack>
                    <SliderFilledTrack />
                  </SliderTrack>
                  <SliderThumb
                    fontSize="sm"
                    boxSize="32px"
                    color="brand.black1"
                    background="brand.gold1"
                    children={phaseTwoDate}
                  />
                </Slider>

                <Stack isInline>
                  <Box width="50%">
                    <FormLabel htmlFor="invitationalSwitch">Invitational</FormLabel>
                    <Switch
                      id="invitationalSwitch"
                      size="lg"
                      colorScheme="orange"
                      isChecked={isInvitational}
                      onChange={() => {
                        setInvitesContext('invites');
                        if (!isInvitational) {
                          setInvitational(true);
                          setTimeout(() => {
                            onOpen();
                          }, 250);
                        } else {
                          setInvitational(false);
                          setUsers(new Map([]));
                        }
                      }}
                    />

                    {!!usersSet.size &&
                      Array.from(usersSet.values()).map((user) => {
                        return (
                          <Tag ml={1} mr={1} colorScheme="orange">
                            <TagLabel>{user.username}</TagLabel>
                            <TagCloseButton onClick={() => removeUser(user.id, 'invite')} />
                          </Tag>
                        );
                      })}
                  </Box>
                  <Box width="50%">
                    <FormLabel htmlFor="addJurorsSwitch">Add jurors</FormLabel>
                    <Switch
                      id="addJurorsSwitch"
                      size="lg"
                      colorScheme="blue"
                      isChecked={hasAdditionalJurors}
                      onChange={() => {
                        setInvitesContext('jurors');
                        if (!hasAdditionalJurors) {
                          setHasAdditionalJurors(true);
                          setTimeout(() => {
                            onOpen();
                          }, 250);
                        } else {
                          setHasAdditionalJurors(false);
                          setJurors(new Map([]));
                        }
                      }}
                    />

                    {!!jurorsSet.size &&
                      Array.from(jurorsSet.values()).map((user) => {
                        return (
                          <Tag ml={1} mr={1} colorScheme="blue">
                            <TagLabel>{user.username}</TagLabel>
                            <TagCloseButton onClick={() => removeUser(user.id, 'juror')} />
                          </Tag>
                        );
                      })}
                  </Box>
                </Stack>
                <InvitesModal
                  invitesContext={invitesContext}
                  isOpen={isOpen}
                  onOpen={onOpen}
                  onClose={onClose}
                  usersSet={usersSet}
                  jurorsSet={jurorsSet}
                  setContestSettings={setContestSettings}
                ></InvitesModal>

                <Grid templateColumns="repeat(1, 1fr)" justifyItems="end">
                  <Button
                    _hover={{
                      bg: isDisabled ? 'brand.red1' : 'brand.gold1',
                      color: isDisabled ? 'brand.white1' : 'brand.black1',
                    }}
                    bg="brand.black1"
                    color="brand.white1"
                    maxWidth="100px"
                    onClick={() => setIsOpenAlert(true)}
                    disabled={isDisabled}
                  >
                    Submit
                  </Button>

                  <AlertPopUp
                    isOpen={isOpenAlert}
                    onClose={onCloseAlert}
                    callback={createContest}
                    error={error}
                    message={"Are you sure you want to create this contest? You can't undo this action afterwards."}
                    title={'Create Contest'}
                    confirmText={'Create'}
                    cancelText={'Cancel'}
                  />
                </Grid>
              </Grid>
            </Box>
          </Grid>
        </Skeleton>
      )}
    </>
  );
});

export default CreateContest;
