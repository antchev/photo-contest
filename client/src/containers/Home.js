import { useEffect, useState } from 'react';
import { Box, Center, Heading, Button, Skeleton } from '@chakra-ui/react';
import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import httpProvider from '../providers/http-provider.js';
import ErrorAlert from '../components/ErrorAlert/ErrorAlert.js';
import { useAuth } from '../context/auth';
import Dashboard from '../containers/Dashboard';
import HomePageParticipation from '../components/HomePageParticipation/HomePageParticipation.js';
import { NavLink } from 'react-router-dom';
import BackgroundImage from '../components/BackgroundImage/BackgroundImage.js';

const Home = (props) => {
  const { sessionAuth } = useAuth();
  const [recentWinners, setRecentWinners] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    httpProvider
      .get('/home')
      .then((res) => {
        setRecentWinners(res.data);
      })
      .catch((error) => {
        if (error.response) {
          setError(error.response.data.error);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  return loading ? null : error ? (
    <ErrorAlert error={error} />
  ) : sessionAuth ? (
    <Dashboard />
  ) : (
    <>
      <Skeleton isLoaded={!loading}>
        <BackgroundImage />
        <Center mt="1%">
          <Heading size="md">Check out some of our recent winners</Heading>
        </Center>
        <Center>
          <Box mt="1%">
            <Splide
              options={{
                rewind: true,
                autoplay: true,
                pauseOnHover: true,
                resetProgress: false,
                arrows: false,
                pagination: false,
                cover: true,
                type: 'fade',
              }}
            >
              {recentWinners &&
                recentWinners.map((participation) => (
                  <SplideSlide key={participation.id} minHeight="100%">
                    <HomePageParticipation
                      key={participation.id}
                      photo={participation.photo}
                      title={participation.title}
                      ranking={participation.ranking}
                    />
                  </SplideSlide>
                ))}
            </Splide>
          </Box>
        </Center>
        <Center mt="2%">
          <NavLink to={`/signup`}>
            <Button color="brand.black1" bg="brand.gold1" size="lg">
              <Box>JOIN NOW</Box>
            </Button>
          </NavLink>
        </Center>
      </Skeleton>
    </>
  );
};

export default Home;
