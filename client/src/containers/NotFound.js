import { Box, Heading } from '@chakra-ui/react';
import BackgroundImage from '../components/BackgroundImage/BackgroundImage';

const NotFound = () => {
  return (
    <>
      <BackgroundImage />
      <Box
        style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          width: '100%',
          height: '100px',
          marginTop: '390px',
          marginLeft: '-50%',
        }}
      >
        <Box textAlign="center" color="brand.gold1">
          <Heading size="4xl">404</Heading>
          <Heading>PAGE NOT FOUND</Heading>
        </Box>
      </Box>
    </>
  );
};

export default NotFound;
