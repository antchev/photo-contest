import { useEffect, useState } from 'react';
import validateParam from '../validator/validator.js';
import ErrorLabel from '../components/ErrorLabel/ErrorLabel';
import ErrorAlert from '../components/ErrorAlert/ErrorAlert';
import SuccessAlert from '../components/SuccessAlert/SuccessAlert';
import httpProvider from '../providers/http-provider.js';
import { NavLink, Redirect } from 'react-router-dom';
import { useAuth } from '../context/auth';
import { FormControl, Button, Input, Grid, Text, Tooltip, Stack, Skeleton } from '@chakra-ui/react';
import BackgroundImage from '../components/BackgroundImage/BackgroundImage.js';

const Credentials = (props) => {
  const currentLocation = props.location.pathname;
  const [registering, setRegistering] = useState(currentLocation === '/signup');
  const { sessionAuth, setSessionAuth } = useAuth();
  const [username, setUsername] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password, setPassword] = useState('');
  const [usernameErr, setUsernameErr] = useState('');
  const [firstNameErr, setFirstNameErr] = useState('');
  const [lastNameErr, setLastNameErr] = useState('');
  const [passwordErr, setPasswordErr] = useState('');
  const [isRegistered, setIsRegistered] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const referer = props?.location?.state?.referer || '/';

  useEffect(() => {
    setRegistering(currentLocation === '/signup');
    setError(null);
    setLoading(false);
    setIsRegistered(false);
    setUsernameErr('');
    setFirstNameErr('');
    setLastNameErr('');
    setPasswordErr('');
  }, [currentLocation]);

  const sendRegisterData = () => {
    const data = {
      username: username,
      password: password,
      firstName: firstName,
      lastName: lastName,
    };
    setLoading(true);
    setError(null);
    httpProvider
      .post(`session/registration`, data)
      .then((res) => {
        setIsRegistered(true);
        loginRedirect();
      })
      .catch((error) => {
        if (error.response) {
          setError(error.response.data.error);
        }
      })
      .finally(setLoading(false));
  };

  const login = () => {
    const data = {
      username: username,
      password: password,
    };
    setLoading(true);
    setError(null);
    httpProvider
      .post('session', data, { withCredentials: true })
      .then((res) => {
        setSessionAuth(res.data);
      })
      .catch((error) => {
        if (error.response) {
          setError(error.response.data.error);
        }
      })
      .finally(setLoading(false));
  };

  const loginRedirect = () => {
    return setTimeout(() => {
      login();
    }, 1000);
  };

  const validateInput = () => {
    const usernameError = validateParam('username', username);
    if (usernameError) {
      setUsernameErr(usernameError);
      return false;
    }
    const passwordError = validateParam('password', password);
    if (passwordError) {
      setPasswordErr(passwordError);
      return false;
    }
    if (registering) {
      const firstNameError = validateParam('firstName', firstName);
      if (firstNameError) {
        setFirstNameErr(firstNameError);
        return false;
      }

      const lastNameError = validateParam('lastName', lastName);
      if (lastNameError) {
        setLastNameErr(lastNameError);
        return false;
      }
    }

    return true;
  };

  const checkForEnterKey = (e) => {
    if (e.which === 13) {
      if (validateInput()) {
        registering ? sendRegisterData() : login();
      }
    }
  };

  return (
    <>
      {sessionAuth && <Redirect to={referer} />}
      {isRegistered && registering && <SuccessAlert message={'Account created, logging you in...'} loader />}
      {!loading ? error && <ErrorAlert error={error} /> : null}
      <Skeleton isLoaded={!loading}>
        <BackgroundImage />
        <Grid
          gap={3}
          style={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            width: '300px',
            marginTop: '15%',
            marginLeft: '-150px',
          }}
        >
          <Text fontWeight="bold">{registering ? 'Register' : 'Login'}</Text>
          <FormControl isRequired>
            <Input
              placeholder="Username"
              onChange={(e) => {
                setUsername(e.target.value);
                setUsernameErr('');
              }}
            />
            {usernameErr ? <ErrorLabel message={usernameErr} /> : null}
          </FormControl>
          <FormControl isRequired>
            <Input
              placeholder="Password"
              type="password"
              onChange={(e) => {
                setPassword(e.target.value);
                setPasswordErr('');
              }}
              onKeyPress={(e) => checkForEnterKey(e)}
            />
            {passwordErr ? <ErrorLabel message={passwordErr} /> : null}
          </FormControl>
          {registering && (
            <>
              <Stack isInline>
                <FormControl isRequired>
                  <Input
                    placeholder="First name"
                    onChange={(e) => {
                      setFirstName(e.target.value);
                      setFirstNameErr('');
                    }}
                    onKeyPress={(e) => checkForEnterKey(e)}
                  />
                  {firstNameErr ? <ErrorLabel message={firstNameErr} /> : null}
                </FormControl>
                <FormControl isRequired>
                  <Input
                    placeholder="Last name"
                    onChange={(e) => {
                      setLastName(e.target.value);
                      setLastNameErr('');
                    }}
                    onKeyPress={(e) => checkForEnterKey(e)}
                  />
                  {lastNameErr ? <ErrorLabel message={lastNameErr} /> : null}
                </FormControl>
              </Stack>
            </>
          )}
          <Grid templateColumns="50% 50%">
            <NavLink
              to={{
                pathname: registering ? '/login' : '/signup',
                state: { referer: referer },
              }}
            >
              <Tooltip
                hasArrow
                label={registering ? 'Click to proceed to login page' : 'Click to make a registration'}
                placement="bottom"
                zIndex="1000"
              >
                <Text fontSize="md">{registering ? 'Already have an account?' : "Don't have an account?"}</Text>
              </Tooltip>
            </NavLink>
            <Button
              color="brand.black1"
              bg="brand.gold1"
              onClick={() => {
                if (validateInput()) registering ? sendRegisterData() : login();
              }}
            >
              {registering ? 'Sign up' : 'Log in'}
            </Button>
          </Grid>
        </Grid>
      </Skeleton>
    </>
  );
};

export default Credentials;
