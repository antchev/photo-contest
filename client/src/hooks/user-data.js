import { useState, useEffect } from 'react';
import httpProvider from '../providers/http-provider.js';
import axios from 'axios';

const useUserData = (userId, trigger) => {
  const [user, setUser] = useState({
    data: null,
    contests: null,
    participations: null,
    isLoading: true,
    error: null,
  });

  useEffect(() => {
    if (!trigger) return;

    axios
      .all([
        httpProvider.get(`users/${userId}`),
        httpProvider.get(`users/${userId}/contests/`),
        httpProvider.get(`users/${userId}/participations/`),
      ])
      .then(
        axios.spread((user, contests, participations) => {
          setUser({
            data: user.data,
            contests: contests.data,
            participations: participations.data,
            isLoading: false,
            error: null,
          });
        }),
      )
      .catch((error) => {
        if (error.response) {
          setUser({
            data: null,
            contests: null,
            participations: null,
            isLoading: false,
            error: error.response.data.error,
          });
        }
      });
  }, [userId, trigger]);

  return user;
};

export default useUserData;
