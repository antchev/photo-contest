import decode from 'jwt-decode';
import { SESSION_KEY, REFRESH_TIMEOUT, REFRESH_TIMEOUT_KEY } from '../common/common.js';
import httpProvider from './http-provider.js';

export const manageSession = (setSession) => {
  const storageData = JSON.parse(localStorage.getItem(SESSION_KEY));

  if (storageData) {
    const token = storageData.token;
    const currentTime = new Date().valueOf();
    const decoded = decode(token);
    const tokenExp = new Date(0).setUTCSeconds(decoded.exp).valueOf();

    if (tokenExp < currentTime) {
      refreshAccessToken(setSession);
    } else {
      clearTimeout(localStorage.getItem(REFRESH_TIMEOUT_KEY));
      const timeoutId = setTimeout(() => {
        refreshAccessToken(setSession);
      }, tokenExp - currentTime - REFRESH_TIMEOUT);
      localStorage.setItem(REFRESH_TIMEOUT_KEY, timeoutId);
    }
  } else {
    refreshAccessToken(setSession);
  }
};

const refreshAccessToken = (setSession) => {
  httpProvider
    .post('session/token', {}, { withCredentials: true })
    .then((res) => {
      setSession(res.data);
    })
    .catch((err) => {
      console.log(err.message);
      setSession(null);
    });
};

export const deleteRefreshToken = (userId) => {
  httpProvider.remove(`session/token/${userId}`);
};

export const logOut = (setSession) => {
  httpProvider
    .remove(`session`)
    .then((res) => {
      setSession(null);
    })
    .catch((err) => {
      console.log(err.message);
    });
};
