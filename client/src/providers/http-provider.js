import axios from 'axios';
import { API_URL, SESSION_KEY } from '../common/common.js';

const getInstance = (options) => {
  const sessionData = localStorage.getItem(SESSION_KEY);
  const token = sessionData ? JSON.parse(sessionData).token : null;

  const defaultOptions = {
    baseURL: API_URL,
    withCredentials: false,
    headers: {
      'Content-Type': 'application/json',
      Authorization: token ? `Bearer ${token}` : '',
    },
  };
  return axios.create({ ...defaultOptions, ...options });
};

const get = (endpoint, options = {}) => {
  return getInstance(options).get(endpoint);
};

const post = (endpoint, data, options = {}) => {
  return getInstance(options).post(endpoint, data);
};

const put = (endpoint, data, options = {}) => {
  return getInstance(options).put(endpoint, data);
};

const remove = (endpoint, options = {}) => {
  return getInstance(options).delete(endpoint);
};

const httpProvider = {
  get,
  post,
  put,
  remove,
};

export default httpProvider;
