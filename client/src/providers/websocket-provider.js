import { API_URL, SESSION_KEY } from '../common/common.js';
import { Manager } from 'socket.io-client';
import toastManager from '../containers/Toast.js';
import Notification from '../components/Notification/Notification';
import { toast } from 'react-toastify';
import { getColorTheme } from '../context/theme-settings.js';

const options = { transports: ['websocket'] };
const manager = new Manager(API_URL, options);
const socket = manager.socket('/');

socket.on('connect', () => {
  const userId = JSON.parse(localStorage.getItem(SESSION_KEY))?.data?.id;
  if (+userId) {
    socket.emit('login', userId);
  }
  console.log(`connect ${socket.id}`);
});

const oldNotifications = new Map();

socket.on('notifications', (notifications) => {
  if (notifications) {
    toastManager.toast = toastManager.themes[getColorTheme() || 'dark'];
    notifications.map((notification, index) => {
      if (!oldNotifications.has(notification.id)) {
        oldNotifications.set(notification.id, notification);

        setTimeout(() => {
          toastManager.toast(
            <Notification
              key={notification.id}
              contestId={notification.contestId}
              date={notification.createDate}
              message={notification.message}
              title={notification.contestTitle}
            />,
            {
              position: 'top-right',
              autoClose: false,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              pauseOnFocusLoss: false,
              draggable: true,
              progress: undefined,
              onClick: () => socket.emit('mark as read', notification.id),
            },
          );
        }, (index * 5000) / notifications.length);
      }
      return notification;
    });
  }
});
socket.on('destroy notifications', () => {
  toast.dismiss();
  oldNotifications.clear();
});

export default socket;
