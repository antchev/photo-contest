import { createBreakpoints } from '@chakra-ui/theme-tools';
import theme from '@chakra-ui/theme';

export const breakpoints = createBreakpoints({
  sm: '320px',
  md: '768px',
  lg: '960px',
  xl: '1200px',
});

export const fonts = {
  ...theme.fonts,
  brand: {
    serif1: 'Georgia, serif',
    serif2: 'Raleway, sans-serif',
    mono1: 'Menlo, monospace',
    cursive1: 'Dancing Script, cursive',
  },
};

export const setColorTheme = () => {
  return {
    brand:
      localStorage.getItem('chakra-ui-color-mode') === 'light'
        ? {
            green1: '#43a747',
            green2: '#47db00',
            black1: '#ffffff',
            black2: '#eeeeee',
            white1: '#020b16',
            white2: '#000000',
            red1: '#dd2211',
            gold1: '#2a109e',
            gray1: '#404040	',
            alwaysWhite: '#ffffff',
          }
        : {
            green1: '#43a747',
            green2: '#47db00',
            black1: '#020b16',
            black2: '#020b16',
            white1: '#ffffff',
            white2: '#E2E8F0',
            red1: '#dd2211',
            gold1: '#FFD232',
            gray1: '#404040	',
            alwaysWhite: '#ffffff',
          },
  };
};

export const getColorTheme = () => localStorage.getItem('chakra-ui-color-mode');

export const colors = setColorTheme();

export const config = {
  useSystemColorMode: false,
  initialColorMode: 'dark',
};
